<?php
/**
 * This file is part of the TelegramBot package.
 *
 * (c) Avtandil Kikabidze aka LONGMAN <akalongman@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Longman\TelegramBot\Commands\UserCommands;

use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Entities\InlineKeyboard;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Raids;
use Longman\TelegramBot\RaidsDB;

/**
 * User "/survey" command
 */
class GymexCommand extends UserCommand
{
 /**
     * @var string
     */
    protected $name = 'gymex';

    /**
     * @var string
     */
    protected $description = 'Permette di selezione le palestre attive da segnalare nel canale condiviso';

    /**
     * @var string
     */
    protected $usage = '/gymex';

    /**
     * @var string
     */
    protected $version = '0.1.0';

    /**
     * @var bool
     */
    protected $need_mysql = true;


	/**
     * @var bool
     */
    protected $show_in_help = false;
		
		
	protected $private_only = true;
	
    /**
     * Command execute method
     *
     * @return \Longman\TelegramBot\Entities\ServerResponse
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function execute(): ServerResponse
    {
        $message = $this->getMessage();

        $chat    = $message->getChat();
        $type    = $chat->getType();
        $user    = $message->getFrom();
		$command = $message->getCommand();
        $subcommand   = trim($message->getText(true));
        $text   = trim($message->getText(true));
        $chat_id = $chat->getId();
        $user_id = $user->getId();
		$username = $user->getUsername();

		if($user_id != 14303576 && $user_id != 144667398)
			return Request::emptyResponse();
		
		//Preparing Response
        $data =  Raids::gymEx($user_id);
		
		
        return Request::sendMessage($data);
    }
}		