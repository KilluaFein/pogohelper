<?php
/**
 * This file is part of the TelegramBot package.
 *
 * (c) Avtandil Kikabidze aka LONGMAN <akalongman@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Longman\TelegramBot\Commands\UserCommands;

use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Entities\KeyboardButton;
use Longman\TelegramBot\Entities\InlineKeyboard;
use Longman\TelegramBot\Entities\PhotoSize;
use Longman\TelegramBot\Conversation;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Raids;
use Longman\TelegramBot\RaidsDB;

/**
 * User "/survey" command
 */
class SegnalapokemonCommand extends UserCommand
{
 /**
     * @var string
     */
    protected $name = 'segnalapokemon';

    /**
     * @var string
     */
    protected $description = 'Permette agli utenti del team istinto di segnalare pokemon nel canale del team';

    /**
     * @var string
     */
    protected $usage = '/segnalapokemon';

    /**
     * @var string
     */
    protected $version = '0.1.0';

    /**
     * @var bool
     */
    protected $need_mysql = true;

    /**
     * Conversation Object
     *
     * @var \Longman\TelegramBot\Conversation
     */
    protected $conversation;

	
	protected $private_only = true;
	
    /**
     * Command execute method
     *
     * @return \Longman\TelegramBot\Entities\ServerResponse
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function execute(): ServerResponse
    {
        $message = $this->getMessage();

        $chat    = $message->getChat();
        $type    = $chat->getType();
        $user    = $message->getFrom();
        $text    = trim($message->getText(true));
        $chat_id = $chat->getId();
        $user_id = $user->getId();
		$username = $user->getUsername();
		
		
		if(RaidsDB::isEnabledUser($chat_id) != 1 || $type != "private")
			return Request::emptyResponse();
		
		if( RaidsDB::isInstinctPlayer($chat_id) != "Giallo")
			return $this->replyToUser("Comando disponibile per i giocatori del team Istinto");
				
		
        //Preparing Response
        $data = [
            'chat_id' => $chat_id,
             
        ];

		$publishPokemon = [
			'chat_id' => -1001063890554,//14303576,
			'user_id' => $user_id,
			//'disable_notification' => true,
			
		];
	
        //Conversation start
        $this->conversation = new Conversation($user_id, $chat_id, $this->getName());

        $notes = &$this->conversation->notes;
        !is_array($notes) && $notes = [];

        //cache data from the tracking session if any
        $state = 0;
        if (isset($notes['state'])) {
            $state = $notes['state'];
        }

        $result = Request::emptyResponse();

        //State machine
        //Entrypoint of the machine state if given by the track
        //Every time a step is achieved the track is updated
        switch ($state) {
			case 0:
                if ($message->getPhoto() === null) {
                    $notes['state'] = 0;
                    $this->conversation->update();

                    $data['text'] = "Invia lo screenshot della segnalazione; effettua lo screen in maniera che si possa vedere il *tempo residuo del pokemon* e la *zona in cui si trova*:\nUsa il comando /cancel per cancellare questo invio";
					$data['parse_mode'] = "markdown";
					
                    $result = Request::sendMessage($data);
                    break;
                }

                /** @var PhotoSize $photo */
                $photo             = end(($message->getPhoto()));
                $notes['photo_id'] = $photo->getFileId();
				// no break
            case 1:
                if ($text === '') {
                    $notes['state'] = 1;
                    $this->conversation->update();

                    $data['text'] = 'Inserisci la zona generica di dove si trova il pokemon che stai segnalando:';
                    

                    $result = Request::sendMessage($data);
                    break;
                }

                $notes['zona'] = $text;
                
                $text         = '';

            // no break
            case 2:
                $this->conversation->update();
                
                unset($notes['state']);

				$publishPokemon['photo'] 	= $notes['photo_id'];
				$publishPokemon['caption'] = "🙎‍♂️ Segnalato da: @$username" . PHP_EOL;
				$publishPokemon['caption'] .= "⛰: ". $notes['zona']. PHP_EOL;
				
                $data['text']      = "Pokemon segnalato con successo!";
                $data['reply_markup'] = Keyboard::remove(['selective' => true]);

				Request::sendPhoto($publishPokemon);
				
                $result = Request::sendMessage($data);
				
				$this->conversation->stop();
                break;
        }

        return $result;
    }
}
