<?php

namespace Longman\TelegramBot\Commands\UserCommands;

use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Raids;
use Longman\TelegramBot\RaidsDB;
use Longman\TelegramBot\Entities\Chat;
use Spatie\Emoji\Emoji;



class UtentiCommand extends UserCommand
{
	protected $name = 'utenti';                      // Your command's name
    protected $description = 'Visualizza la lista degli utenti che hanno compilato il proprio profilo'; // Your command description
    protected $usage = '/utenti';                    // Usage of your command
    protected $version = '0.1.0';                  // Version of your command
	protected $private_only = true;
	
    public function execute(): ServerResponse
    {

        $message = $this->getMessage();		     // Get Message object
		$chat = $message->getChat();
        $user = $message->getFrom();
		$type    = $chat->getType();	
		$value   = trim($message->getText(true));		
		$command = $message->getCommand();
        $user_id = $user->getId(); // Get the current Chat ID
		$chat_id = $chat->getId();
		
		$data = [];                        	        // Set up the new message data
        $data['chat_id'] = $user_id;         	    // Set Chat ID to send the message to
		$data['parse_mode'] = 'HTML';
		
		if(RaidsDB::isEnabledUser($chat_id) != 1 || $type != "private")
			return Request::emptyResponse();
		
		
		
		$users = RaidsDB::getUsers();
		
		$text = Emoji::fire() . "<b>Lista Codici Amico</b>" . Emoji::fire() . PHP_EOL . PHP_EOL;
		$leng = 0;
		foreach($users as $user){
			$text .= sprintf('<code>%s</code> <a href="tg://user?id=%d">%s</a> L%d' . PHP_EOL,
									$user['idunivoco'],$user['id'],$user['ingamename'],$user['livello']);
			if(strlen($text) > 3500){
				$data['text'] = $text;
				Request::sendMessage($data);
				$text = "";
			}
		}
		$data['text'] = $text;
		
		return Request::sendMessage($data);
	}
}