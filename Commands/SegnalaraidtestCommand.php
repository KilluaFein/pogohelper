<?php
/**
 * This file is part of the TelegramBot package.
 *
 * (c) Avtandil Kikabidze aka LONGMAN <akalongman@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Longman\TelegramBot\Commands\AdminCommands;

use Longman\TelegramBot\Commands\AdminCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Entities\KeyboardButton;
use Longman\TelegramBot\Entities\InlineKeyboard;
use Longman\TelegramBot\Entities\PhotoSize;
use Longman\TelegramBot\Conversation;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Raids;
use Longman\TelegramBot\RaidsDB;

/**
 * User "/survey" command
 */
class SegnalaraidtestCommand extends AdminCommand
{
 /**
     * @var string
     */
    protected $name = 'segnalaraidtest';

    /**
     * @var string
     */
    protected $description = 'Permette ad ogni utente di segnalare i raid. I Raid L4 vengono postati nel canale';

    /**
     * @var string
     */
    protected $usage = '/segnalaraidtest';

    /**
     * @var string
     */
    protected $version = '0.1.0';

    /**
     * @var bool
     */
    protected $need_mysql = true;

    /**
     * Conversation Object
     *
     * @var \Longman\TelegramBot\Conversation
     */
    protected $conversation;

	
	protected $private_only = true;
	
    /**
     * Command execute method
     *
     * @return \Longman\TelegramBot\Entities\ServerResponse
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function execute(): ServerResponse
    {
        $message = $this->getMessage();

        $chat    = $message->getChat();
        $user    = $message->getFrom();
        $text    = trim($message->getText(true));
        $chat_id = $chat->getId();
        $user_id = $user->getId();
		$username = $user->getUsername();
		
		
		if(RaidsDB::isEnabledUser($chat_id) != 1)
			return false;
		
		
        //Preparing Response
        $data = [
            'chat_id' => $chat_id,
			'parse_mode' => 'html',
             
        ];
		$publishRaid = [
			'chat_id' => 14303576,//-1001134006412,
			'user_id' => $user_id,
			'parse_mode' => 'html',
			
		];
		
		
		if((date('H') < 7 or date('H') > 22) and $chat_id <> 14303576 ){
			$data['text'] = "Non mi pare che a quest'ora ci siano raid, stai cercando di fare lo scemino?";
			return Request::sendMessage($data);
		}
			

        //Conversation start
        $this->conversation = new Conversation($user_id, $chat_id, $this->getName());

        $notes = &$this->conversation->notes;
        !is_array($notes) && $notes = [];

        //cache data from the tracking session if any
        $state = 0;
        if (isset($notes['state'])) {
            $state = $notes['state'];
        }

        $result = Request::emptyResponse();

        //State machine
        //Entrypoint of the machine state if given by the track
        //Every time a step is achieved the track is updated
        switch ($state) {
		case 0:
			if ($text === '' || !is_numeric($text) || $text < 1 || $text > 5) {
				$notes['state'] = 0;
				$this->conversation->update();

				$data['text']         = "Inserisci il livello del Raid:\nUsa il comando /cancel per cancellare questo invio";
				//
				$data['reply_markup'] = (new Keyboard(['4','5']))
					->setResizeKeyboard(true)
					->setOneTimeKeyboard(true)
					->setSelective(true);
				
				if ($text !== '') {
					$data['text'] = 'In formato numerico, tonto. O non mettere numeri stravanati tanto per fare lo scemino.';
				}

				$result = Request::sendMessage($data);
				break;
			}

			$notes['raidlevel'] = $text;
			
			$text          = '';

            // no break
		case 1:
			if ($text === '' || strlen($text) < 3) {
				$notes['state'] = 1;
				$this->conversation->update();

				$data['text'] = "Inserisci il nome della palestra che vuoi segnalare (minimo 3 lettere):\nUsa il comando /cancel per cancellare questo invio";
				$data['reply_markup'] = Keyboard::remove(['selective' => true]);
				
				$result = Request::sendMessage($data);
				break;
			}

			$notes['ricerca'] = $text;
			
			$data['text'] = "Risultato ricerca per <i>". $notes['ricerca'] ."</i>:" . PHP_EOL . PHP_EOL;
				
			$gyms = RaidsDB::getGymByName($text);
			
			foreach($gyms as $gym){
				$data['text'] .= "<code>".$gym['gym_id']."</code>: ".$gym['gym_name']."" . PHP_EOL;
			}
			
			$data['text'] .= "<code>0</code>: Effettua una nuova ricerca" . PHP_EOL . PHP_EOL;					
			$data['text'] .= "<i>Fai Tap sul codice della palestra corretta per copiarlo negli appunti, e incollalo come nuovo messaggio.</i>";
			
			$result = Request::sendMessage($data);
			
			
			$text          = '';

            // no break
		case 2:
			if ($text === '') {
				$notes['state'] = 2;
				$this->conversation->update();
				
				
				break;
			}			
			if($text > 0 )	{
				$notes['gym_id'] = $text;
				$text          = '';
			}
			else{
				$notes['state'] = 1;
				$this->conversation->update();
				$data['text'] = "Inserisci il nome della palestra che vuoi segnalare (minimo 3 lettere):\nUsa il comando /cancel per cancellare questo invio";
				$data['reply_markup'] = Keyboard::remove(['selective' => true]);
				
				$result = Request::sendMessage($data);
				break;
			}
		
            // no break
		case 3:
			if ($text === '' || strlen($text) > 5){// || !preg_match('/^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]/$',$text)) {
				$notes['state'] = 3;
				$this->conversation->update();

				$data['text'] = "A che ora schiude? (se già schiuso, mettere comunque l'ora di schiusura) nel formato HH:MM:\nUsa il comando /cancel per cancellare questo invio";//schiude? (se già schiuso, mettere comunque l\'ora di schiusura) nel formato HH:MM:';
				
				if ($text !== '') {
					$data['text'] = 'HO DETTO FORMATO HH:MM 🖕. E se hai fatto la cazzata di mettere uno screen con raid multipli, fammi la cortesia di premere /cancel e riiniziare da capo. Uno alla volta!';
				}

				$result = Request::sendMessage($data);
				break;
			}
			$text = str_replace(".",":",$text);
			$notes['oraschiusa'] = $text;
			
			$text             = '';

            // no break
		
		case 4:
			$this->conversation->update();
			
			unset($notes['state']);
			$team = ["⚪️", "🔵", "🔴", "💛"];
			$raidlevel  = ['0️⃣','1️⃣','2️⃣','3️⃣','4️⃣','5️⃣','6️⃣','7️⃣','8️⃣','9️⃣','🔟'];
			$photo_id = [1 => "AgADBAADeqwxGzDPgFPEd5KHFoTRfW3XihoABPVKswGu3a8qgWoBAAEC",
					2 => "AgADBAADeqwxGzDPgFPEd5KHFoTRfW3XihoABPVKswGu3a8qgWoBAAEC",
					3 => "AgADBAADeawxGzDPgFMRHSaeZQasZgLEJxoABJ3obj-Ueqr11U0DAAEC",
					4 => "AgADBAADeawxGzDPgFMRHSaeZQasZgLEJxoABJ3obj-Ueqr11U0DAAEC",
					5 => "AgADBAADeKwxGzDPgFOVLSUj2NPfGybZJxoABLjDlAN6Sr7dtVADAAEC",
				];
			
			$gym = RaidsDB::getGymById($notes['gym_id']);
			
			$publishRaid['raidlevel']	= $notes['raidlevel'];
			$publishRaid['photo']       = $photo_id[$notes['raidlevel']];
			$publishRaid['oraschiusa'] 	= date('Y-m-d ').$notes['oraschiusa'].":00";
			$publishRaid['photo_id'] 	= $photo_id[$notes['raidlevel']];
			$publishRaid['parse_mode'] 	= 'html';
			$publishRaid['team']  = 0;
			$publishRaid['zona']		= $gym['gym_name'];
			$publishRaid['message_id']  = 0;
			$publishRaid['lat']  = $gym['lat'];
			$publishRaid['lon']  = $gym['lon'];
			$publishRaid['imageURL']  = "http://maps.googleapis.com/maps/api/staticmap?language=it&center=";
			$publishRaid['imageURL'] .= $publishRaid['lat'].",".$publishRaid['lon']	."&zoom=16&scale=2&size=400x300";
			$publishRaid['imageURL'] .= "&key=AIzaSyC_e__t0UTw6CwjdDvVf4XXcGxLFZscgMs";
			$publishRaid['imageURL'] .= "&markers=anchor:center%7Cicon:https://teambattle.info/pogofiles/upload/raid".$publishRaid['raidlevel'].".png%7C";
			$publishRaid['imageURL'] .= $publishRaid['lat'].",".$publishRaid['lon'];
						
			$publishRaid['text']  = "🔥<b>NEW RAID SEGNALESCION!!</b>🔥". PHP_EOL . PHP_EOL;
			$publishRaid['text'] .= "Livello <a href=\"".$publishRaid['imageURL']."\">‍</a>".$raidlevel[$notes['raidlevel']] . PHP_EOL;
			$publishRaid['text'] .= "🕰: ". $notes['oraschiusa']. PHP_EOL;
			$publishRaid['text'] .= "⛰: <a href=\"http://maps.google.com/maps?q=" . $gym['lat'] . "," . $gym['lon'] . "\">". $gym['gym_name']."</a> " . $team[$publishRaid['team']] . PHP_EOL;
			$publishRaid['text'] .= "🙎‍♂️: @$username". PHP_EOL;
			
			$publishRaid['fixed_text'] = $publishRaid['text'];
				
			$oraestesa = date_create_from_format("Y-m-d H:i:s",$publishRaid['oraschiusa']);
			//date_add($oraestesa, date_interval_create_from_date_string('1 hour'));
			$orario = $oraestesa->format('H:i');
			$publishRaid['t1'] = $oraestesa->format("Y-m-d H:i:s");
			$publishRaid['text'] .= "Partecipano alle $orario: " . PHP_EOL;
			date_add($oraestesa, date_interval_create_from_date_string('15 minutes'));
			$orario = $oraestesa->format('H:i');
			$publishRaid['t2'] = $oraestesa->format("Y-m-d H:i:s");
			$publishRaid['text'] .= "Partecipano alle $orario: " . PHP_EOL;
			date_add($oraestesa, date_interval_create_from_date_string('15 minutes'));
			$orario = $oraestesa->format('H:i');
			$publishRaid['t3'] = $oraestesa->format("Y-m-d H:i:s");
			$publishRaid['text'] .= "Partecipano alle $orario: " . PHP_EOL;
			
			$publishRaid['reply_markup'] = Raids::raidButtonOrario($publishRaid['raidlevel'],$publishRaid['oraschiusa']);
				// $publishRaid['reply_markup'] = Raids::raidButton($publishRaid['raidlevel']);
				
			
			$publishRaid['comando'] = "segnalaraidtest";				
			
			$publishRaid['reply_markup'] = Raids::raidButtonOrarioBis($publishRaid);
			
			$data['text']      = "Raid segnalato con successo!";
			$data['reply_markup'] = Keyboard::remove(['selective' => true]);

			if ($publishRaid['raidlevel'] >= 4){ //pubblico nel canale solo se è un 4+
				$result = Request::sendMessage($publishRaid);
				$publishRaid['message_id'] = $result->result->message_id;
			}
			
			//Request::sendMessage($data); //reply to user
			
			$data['text']      = print_r($publishRaid,true);
			Request::sendMessage($data);
			
			//RaidsDB::addSegnalazioni($publishRaid['user_id']); 	//aumento contatore segnalazioni utente
			//RaidsDB::insertRaid($publishRaid);					//inserisco la segnalazione nella tabella Raids
			
			$this->conversation->stop();
			break;
        }
        return $result;
    }
}
