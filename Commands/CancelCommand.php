<?php
/**
 * This file is part of the TelegramBot package.
 *
 * (c) Avtandil Kikabidze aka LONGMAN <akalongman@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Longman\TelegramBot\Commands\UserCommands;

use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Conversation;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\RaidsDB;

/**
 * User "/cancel" command
 *
 * This command cancels the currently active conversation and
 * returns a message to let the user know which conversation it was.
 * If no conversation is active, the returned message says so.
 */
class CancelCommand extends UserCommand
{
    /**
     * @var string
     */
    protected $name = 'cancel';

    /**
     * @var string
     */
    protected $description = 'Cancella la segnalazione corrente';

    /**
     * @var string
     */
    protected $usage = '/cancel';

    /**
     * @var string
     */
    protected $version = '0.2.0';

    /**
     * @var bool
     */
    protected $need_mysql = true;

	
	protected $private_only = true;
	
    /**
     * Command execute method
     *
     * @return \Longman\TelegramBot\Entities\ServerResponse
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function execute(): ServerResponse
    {
		$message     = $this->getMessage();
        $chat_id     = $message->getFrom()->getId();
		$command 	 = $message->getCommand();
		
		if(RaidsDB::isEnabledUser($chat_id) != 1 && $command =="cancel")
			return false;
		
		if($command != "cancel"){
			$channel_id = str_replace('cancel','-',$command);
			RaidsDB::deleteChannel($chat_id,$channel_id);
			$text ="Canale cancellato con successo.";
		}
		else{
		
			$text = 'No active conversation!';

			//Cancel current conversation if any
			$conversation = new Conversation(
				$this->getMessage()->getFrom()->getId(),
				$this->getMessage()->getChat()->getId()
			);

			if ($conversation_command = $conversation->getCommand()) {
				$conversation->cancel();
				if ($conversation_command == "segnalaraid" || $conversation_command == "segnalapokemon")
					$text = 'Segnalazione cancellata! ';
				elseif ($conversation_command == "nests")
					$text = 'Aggiunta nido cancellata! ';
				else
					$text = 'Conversazione "' . $conversation_command . '" cancellata!';
			}
		}
        return $this->removeKeyboard($text);
    }

    /**
     * Remove the keyboard and output a text
     *
     * @param string $text
     *
     * @return \Longman\TelegramBot\Entities\ServerResponse
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    private function removeKeyboard($text): ServerResponse
    {
        return Request::sendMessage(
            [
                'reply_markup' => Keyboard::remove(['selective' => true]),
                'chat_id'      => $this->getMessage()->getChat()->getId(),
                'text'         => $text,
            ]
        );
    }

    /**
     * Command execute method if MySQL is required but not available
     *
     * @return \Longman\TelegramBot\Entities\ServerResponse
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function executeNoDb(): ServerResponse
    {
        return $this->removeKeyboard('Nothing to cancel.');
    }
}