<?php
/**
 * This file is part of the TelegramBot package.
 *
 * (c) Avtandil Kikabidze aka LONGMAN <akalongman@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Longman\TelegramBot\Commands\SystemCommands;

use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Entities\InlineKeyboard;
use Longman\TelegramBot\Entities\Chat;
use Longman\TelegramBot\Conversation;
use Longman\TelegramBot\Commands\SystemCommand;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Raids;
use Longman\TelegramBot\RaidsDB;
use Longman\TelegramBot\DB;
use Spatie\Emoji\Emoji;

/**
 * Callback query command
 */
class CallbackqueryCommand extends SystemCommand
{
    /**
     * @var string
     */
    protected $name = 'callbackquery';

    /**
     * @var string
     */
    protected $description = 'Reply to callback query';

    /**
     * @var string
     */
    protected $version = '1.1.0';

	/**
     * Conversation Object
     *
     * @var \Longman\TelegramBot\Conversation
     */
    protected $conversation;
	
    /**
     * Command execute method
     *
     * @return \Longman\TelegramBot\Entities\ServerResponse
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function execute(): ServerResponse
    {
        $update            = $this->getUpdate();
        $callback_query    = $update->getCallbackQuery();
        $callback_query_id = $callback_query->getId();
        $callback_data     = $callback_query->getData();
        $callback_message_id     = $callback_query->getMessage()->getMessageId();
        $callback_entities     = $callback_query->getMessage()->getEntities();
        $callback_user_id     = $callback_query->getFrom()->getId();
        $callback_username     = $callback_query->getFrom()->getUsername();
        $callback_chat_id     = $callback_query->getMessage()->getChat()->getId();
        $callback_message_text     = trim($callback_query->getMessage()->getText(true)); //testo del messaggio associato alla callback
		
		$channel_id['chat_id'] =  -1001134006412; //CrossRaid Channel
		$refeer = RaidsDB::getRefeer($callback_user_id);
		
		// $dataToAdmin = [
					// 'chat_id' => 14303576,
					// 'text'    => json_encode($callback_entities),
				// ];				
		// Request::sendMessage($dataToAdmin);
		
		$emojiPokemon= [ 
			'Tyranitar' 	=> Emoji::dragonFace(),
			'Snorlax' 		=> Emoji::panda(),
			'Lapras' 		=> Emoji::spoutingWhale(),
			'Rhydon'		=> Emoji::middleFinger(),
			'Venusaur' 		=> Emoji::hibiscus(),
			'Charizard'		=> Emoji::fire(),
			'Blastoise' 	=> Emoji::droplet(),
			'Absol' 		=> Emoji::wolf().Emoji::newMoonFace(),
			'Nidoqueen' 	=> Emoji::cow().Emoji::womenSRoom(),
			'Nidoking' 		=> Emoji::rhinoceros().Emoji::menSRoom(),
			'Victribel' 	=> Emoji::bug().Emoji::shamrock(),
			'Golem' 		=> Emoji::bear().Emoji::moai(),
			'Articuno' 		=> Emoji::droplet().Emoji::droplet().Emoji::droplet(),
			'Moltres' 		=> Emoji::fire().Emoji::fire().Emoji::fire(),
			'Zapdos'		=> Emoji::highVoltage().Emoji::highVoltage().Emoji::highVoltage(),
			'Hooh' 			=> Emoji::eagle().Emoji::eagle().Emoji::eagle(),
			'Lugia' 		=> Emoji::dove().Emoji::dove().Emoji::dove(),
			'Mewtwo' 		=> Emoji::smilingFaceWithHorns().Emoji::smilingFaceWithHorns().Emoji::smilingFaceWithHorns(),
			'Raikou' 		=> Emoji::largeOrangeDiamond().Emoji::largeOrangeDiamond().Emoji::largeOrangeDiamond(),
			'Entei' 		=> Emoji::diamondSuit().Emoji::diamondSuit().Emoji::diamondSuit(),
			'Suicune' 		=> Emoji::largeBlueDiamond().Emoji::largeBlueDiamond().Emoji::largeBlueDiamond(),
			'Mew' 			=> Emoji::hamster(),
			'Celebi' 		=> Emoji::alien(),
			'Groudon' 		=> Emoji::lizard().Emoji::lizard().Emoji::lizard(),
			'Kyogre' 		=> Emoji::shark().Emoji::shark().Emoji::shark(),
		];
				
		$parameter = explode(",",$callback_data);
		
		switch($parameter[0]){ //$paramenter[0] mi indica da che comando sto richiamando la Callback
			/************************************ START *************************************/
			case 'start': //se la callback proviene dal comando start
				
				$user_id = $parameter[1];
				$state = $parameter[2];
				$refeer = $parameter[3];
				
				$data = [
					'callback_query_id' => $callback_query_id,
					'text'              => Emoji::thumbsUp(),
					'show_alert'        => false,
					'cache_time'        => 7,
				];
				
				$textUser = array(
					-1 => "Hai fatto da monellino? Sei stato disabilitato!",
					0  => "",
					1  => "Sei stato abilitato da un amministratore, premi /help per vedere i comandi disponibili.\nTi ricordo che puoi usare il comando /profilo per modificare la tua scheda profilo e partecipare alle classifiche di zona.\nBuona permanenza!",//*MI RACCOMANDO*\nè vietato inoltrare i messaggi dal canale in chat non del tuo team. *Pena il ban dal canale e dal bot*.",
					2  => "Il grande capo ti ha reso admin ",
				);
				
				$dataToUser = [
					'chat_id' => $user_id,
					'parse_mode' => 'MARKDOWN',
					'text' => $textUser[$state],
				];
				
				
				if($state == 1){
					// $channels = RaidsDB::getChannels($refeer);
					// $dataToUser['reply_markup'] = Raids::printChannelsButton($channels);
					
					// $dataToAdmin = [
					// 'chat_id' => 14303576,
					// 'text'    => ".".json_encode($channels),//Raids::printChannelsButton($channels),
						// ];				
					// Request::sendMessage($dataToAdmin);
				}
				
				Request::sendMessage($dataToUser);
				
				RaidsDB::setEnabled($user_id,$state);
				
				$textWithEntities = Raids::parseEntitiesString($callback_message_text,$callback_entities);
				
				$editedAdminMessage = [
					'chat_id' => $callback_chat_id,
					'message_id' => $callback_message_id,
					'text' => str_replace("_","\_",$textWithEntities). PHP_EOL . PHP_EOL . ($state ==1?"*Abilitato*":"*Disabilitato*")." in data ". date('Y-m-d H:i:s'),
					'reply_markup' => Raids::enableDisableButton($user_id,$refeer),
					'parse_mode' => 'MARKDOWN',
				];
				Request::editMessageText($editedAdminMessage);
			break;
			/************************************ MAKEMEADMIN *************************************/
			case 'makemeadmin': //se la callback proviene dal comando makemeadmin
				
				$utente = $parameter[1];
				$user_id = $parameter[2];
				$step = $parameter[3];

				if ($utente == "user"){
					
					if($step <= 3){
					
						$editedUserMessage = Raids::makeMeAdmin($callback_data);
						$editedUserMessage['message_id'] = $callback_message_id;
						
						if($step == 3){
							$editedUserMessage['reply_markup'] = null;
							//$editedUserMessage['reply_markup'] = Keyboard::remove(['selective' => true]);
							
							$dataToAdmin = Raids::makeMeAdmin("makemeadmin,admin,$user_id,0");
							$dataToAdmin['chat_id'] = 14303576;
							
							Request::sendMessage($dataToAdmin);
						}
						
						Request::editMessageText($editedUserMessage);
					}
				}
				elseif ($utente == "admin"){
					if($step <= 3){
						if($step == 2){
							$new_quantity = $parameter[4];
							if ($new_quantity != 0){
								$current_channel_quantity = RaidsDB::getChannelQuantity($user_id);
								RaidsDB::setChannelQuantity($user_id, $current_channel_quantity+$new_quantity);
								RaidsDB::setNotificationStatus('Refeer', $user_id, $user_id, RaidsDB::getUsername($user_id));
								RaidsDB::setEnabled($user_id,1);
							}	
						}
							
						$editedAdminMessage = Raids::makeMeAdmin($callback_data);
						$editedAdminMessage['message_id'] = $callback_message_id;
						
						if($step == 3){
							$editedAdminMessage['reply_markup'] = null;
							//inviare notifica utente
							$dataToUser = Raids::makeMeAdmin("makemeadmin,user,$user_id,4");
							$dataToUser['reply_markup'] = null;
							Request::sendMessage($dataToUser);
						}
						Request::editMessageText($editedAdminMessage);
					}
				}
				
				$data = [
					'callback_query_id' => $callback_query_id,
					'text'              => "",
					'show_alert'        => false,
					'cache_time'        => 7,
				];	
				
			break;
			/************************************ profilo *************************************/
			
			case 'profilo':  //"profilo,$user_id,edit,team"
				$user_id = $parameter[1];
				$subcommand = $parameter[2]; //edit,edited,add
				$campo = $parameter[3]; //team,
				
				switch($campo){
					
					case 'team':
						if($subcommand == "edit"){
							
							$editedAdminMessage['chat_id'] = $user_id;
							$editedAdminMessage['message_id'] = $callback_message_id;
							$editedAdminMessage['parse_mode'] = 'html';
							
							$text = "<b>Modifica Team</b>" .PHP_EOL;
							$text .= "<b>Attenzione!</b> La modifica del team è permessa UNA sola volta! Stai attento a non sbagliare!";
							
							$editedAdminMessage['text'] = $text;
							
							$editedAdminMessage['reply_markup'] = new InlineKeyboard([
								['text' => "Blu", 'callback_data' => "profilo,$user_id,edited,team,1"],
								['text' => "Giallo", 'callback_data' => "profilo,$user_id,edited,team,3"],
								['text' => "Rosso", 'callback_data' => "profilo,$user_id,edited,team,2"],
							]);
							
							Request::editMessageText($editedAdminMessage);
						}
						elseif($subcommand == "edited")
						{				
							$editedAdminMessage = Raids::profile($user_id);
							$editedAdminMessage['message_id'] = $callback_message_id;
								
							RaidsDB::setUserDetail($user_id,RaidsDB::getUsername($user_id), $campo, $parameter[4]);
							
							Request::editMessageText($editedAdminMessage);
							
							$callback_feedback = "Team cambiato con successo!";
						}
					break;
					
					case 'canali':
					break;
					
					case 'idunivocoprivacy':
					case 'raid':
					
						RaidsDB::setUserDetail($user_id,RaidsDB::getUsername($user_id), $campo, $parameter[4]);
						
						$editedAdminMessage = Raids::profile($user_id,"full");
						$editedAdminMessage['message_id'] = $callback_message_id;
						
						Request::editMessageText($editedAdminMessage);
						
					break;
					
					
					case 'profilo':
						
							$editedAdminMessage = Raids::profile($user_id,"edit");
							$editedAdminMessage['message_id'] = $callback_message_id;
							
							Request::editMessageText($editedAdminMessage);
						
					break;
					case 'termina':
					
							$editedAdminMessage = Raids::profile($user_id,"full");
							$editedAdminMessage['message_id'] = $callback_message_id;
							
							Request::editMessageText($editedAdminMessage);
						
					
					break;
					default:
						if($subcommand == "edit"){
							
							$editedAdminMessage['chat_id'] = $user_id;
							$editedAdminMessage['message_id'] = $callback_message_id;
							$editedAdminMessage['parse_mode'] = 'html';
							
							if($campo == "ingamename"){
								$text = "<b>Modifica Nome Giocatore</b>" .PHP_EOL;
								$text .= "Inserisci il tuo nickname su Pokemon Go";	
							}
							elseif($campo == "idunivoco"){
								$text = "<b>Inserisci il tuo Codice Amico </b>" .PHP_EOL;
								
							}
							else
								$text = "<b>Modifica Valore:</b> <i>$campo</i>";
								if($campo == "datainizio")
									$text .= PHP_EOL . "Formato: gg/mm/aaaa";
							
							$editedAdminMessage['text'] = $text;
							
							
							Request::editMessageText($editedAdminMessage);
							
							$this->conversation = new Conversation($user_id, $user_id, 'profilo');
							
							$notes = &$this->conversation->notes;
							!is_array($notes) && $notes = [];

							
							$notes['command'] = $campo;
							$notes['state'] = 0;
							$this->conversation->update();
						}
						
				}
				
				
				$data = [
					'callback_query_id' => $callback_query_id,
					'text'              => (isset($callback_feedback)?$callback_feedback:""),
					'show_alert'        => false,
					'cache_time'        => 7,
				];	
			break;
			/************************************ raidEx *************************************/
			case 'raidex': //se la callback proviene dal comando raidEx
				
				$raid_id = $parameter[1];
				$pass = $parameter[2]; // 0/1 , non/ho il pass
				$user_id = $callback_user_id;
				$text = "";
				$dataToUser = [
						'chat_id' => $user_id,
					];	
					
				$editedUserMessage = [
						'chat_id' => $user_id,
						'message_id' => $callback_message_id,
				];
				
				if($pass == -1){
					$status = RaidsDB::insertRaidExUser($raid_id, $user_id, 0 ,0);
					if($status)
						$text = "Per gestire i pass usa il comando /passex";
					else
						$text = "C'è stato un problema con l'inserimento nel database. Contatta l'amministratore @killuafein.";
					$pass = -1;
				}elseif($pass == 0){
					
					$status = RaidsDB::insertRaidExUser($raid_id, $user_id, 0 ,1);
					if($status)
						$text = "Sei stato inserito nella lista degli utenti in attesa di ricevere un pass da un amico. Per gestire i pass usa il comando /passex";
					else
						$text = "C'è stato un problema con l'inserimento nel database. Contatta l'amministratore @killuafein.";
					$pass = -1;
				}elseif($pass == 1){
				
				$editedUserMessage['reply_markup'] = new InlineKeyboard([
						['text'  => "Voglio mettere il pass a disposizione di un amico", 'callback_data' => "raidex,$raid_id,2"],
						],[	
						['text'  => "L'ho già promesso a qualcuno che non usa il bot", 'callback_data' => "raidex,$raid_id,3"],	
					]);
					
				}elseif($pass == 2){
					
					$status = RaidsDB::insertRaidExUser($raid_id, $user_id, 1 ,1);
					if($status)
						$text = "Sei stato inserito nella lista degli utenti che possono dare il pass. Per gestire i pass usa il comando /passex";
					else
						$text = "C'è stato un problema con l'inserimento nel database. Contatta l'amministratore @killuafein.";
					$pass = -1;
				}elseif($pass == 3){
					
					$status = RaidsDB::insertRaidExUser($raid_id, $user_id, 1 ,0);
					$pass = -1;
					$text = "";
				}
				if($pass == -1){
				
					// $editedUserMessage['reply_markup'] = Keyboard::remove(['selective' => true]);
				
				}
				
				$dataToUser['text'] = $text;
				
				Request::sendMessage($dataToUser);
				
			
				Request::editMessageReplyMarkup($editedUserMessage);
				
				$data = [
					'callback_query_id' => $callback_query_id,
					'text'              => (isset($callback_feedback)?$callback_feedback:""),
					'show_alert'        => false,
					'cache_time'        => 7,
				];		
				
			break;
			/************************************ passex *************************************/
			case 'passex': //se la callback proviene dal comando passex
				$raid_id = $parameter[1];
				$status = $parameter[2]; // -1/0/1/2/3: aggiorna/mettimi in lista/rendi disponibile/richiedi pass ad utente/assegna pass a richiedente
				$user_id = $callback_user_id;
				$text = "";
				
				$dataToUser = [
						'chat_id' => $user_id,
						'parse_mode' => 'html',
						'disable_web_page_preview' => 'true',
					];	
					
				$editedUserMessage = [
						'chat_id' => $user_id,
						'message_id' => $callback_message_id,
						'parse_mode' => 'html',
						'disable_web_page_preview' => 'true',
				];
				
				
				switch($status){
					
					case '-1':
						$editedUserMessage['text'] = Raids::printActiveRaid($raid_id, $user_id);
						$editedUserMessage['reply_markup'] = Raids::printActiveRaidButtons($raid_id, $user_id);
						
						Request::editMessageText($editedUserMessage);
					break;
					case '0':
						RaidsDB::insertRaidExUser($raid_id, $user_id, 0 ,1);
						
						$editedUserMessage['text'] = Raids::printActiveRaid($raid_id, $user_id);
						$editedUserMessage['reply_markup'] = Raids::printActiveRaidButtons($raid_id, $user_id);
						
						Request::editMessageText($editedUserMessage);
						
					break;
						
					case '1':
						RaidsDB::insertRaidExUser($raid_id, $user_id, 1 ,0);
						
						$editedUserMessage['text'] = Raids::printActiveRaid($raid_id, $user_id);
						$editedUserMessage['reply_markup'] = Raids::printActiveRaidButtons($raid_id, $user_id);
						
						Request::editMessageText($editedUserMessage);
					break;
					case '2':
					case '3':
					
						$duplicate = RaidsDB::getRaidExUsersDuplicate($raid_id, $user_id);
						if(empty($duplicate)){
							
							$editedUserMessage['text'] = Raids::managePassEx($raid_id, $user_id,$status,"text");
							$editedUserMessage['reply_markup'] = Raids::managePassEx($raid_id, $user_id,$status,"reply_markup");
						}else{
							
							$editedUserMessage['text'] = Raids::printActiveRaid($raid_id, $user_id);
							$editedUserMessage['reply_markup'] = Raids::printActiveRaidButtons($raid_id, $user_id);
						}
						Request::editMessageText($editedUserMessage);
						
					break;
					
					case '6':
						$user_friend_id = $parameter[3];
						$user = RaidsDB::getRaidExUsersFilter($raid_id, null,null, $user_friend_id);
						
						if(empty($user)){
							RaidsDB::insertRaidExUser($raid_id, $user_id, 0 ,1, $user_friend_id);
							RaidsDB::insertRaidExUser($raid_id, $user_friend_id, 1 ,1, $user_id);
							
							$editedUserMessage['text'] = "Hai chiesto a <i>".RaidsDB::getUsername($user_friend_id)."</i> di poter ricevere il pass Ex.";
							Request::editMessageText($editedUserMessage);
							
							$dataToUser['text'] = Raids::printActiveRaid($raid_id, $user_id);
						
							$dataToUser['reply_markup'] =  Raids::printActiveRaidButtons($raid_id, $user_id);
							
							Request::sendMessage($dataToUser);
							
							Raids::askPassEx($raid_id,$user_id,$user_friend_id,"domanda");
							
						}else{
							$editedUserMessage['text'] = "Qualcuno ti ha ninjato l'utente!";
							$editedUserMessage['reply_markup'] = new InlineKeyboardButton(['text' => 'Indietro', 'callback_data' => "passex,$raid_id,-1"]);
							Request::editMessageText($editedUserMessage);
						}
					break;
					
					case '7':
					case '8':
						$user_friend_id = $parameter[3];
						if($status == 7){
							RaidsDB::insertRaidExUser($raid_id, $user_id, 1 ,0, $user_friend_id);
							RaidsDB::insertRaidExUser($raid_id, $user_friend_id, 0 ,0, $user_id);
							$editedUserMessage['text'] = sprintf("Hai correttamente invitato <i>%s</i> al raid Ex.",RaidsDB::getUsername($user_friend_id));
						}else{
							RaidsDB::insertRaidExUser($raid_id, $user_id, 1 ,1, null);
							RaidsDB::insertRaidExUser($raid_id, $user_friend_id, 0 ,1, null);
							$editedUserMessage['text'] = sprintf("Hai rifiutato l'invitato a <i>%s</i>.",RaidsDB::getUsername($user_friend_id));
						}

						Request::editMessageText($editedUserMessage);
						
						$dataToUser['text'] = Raids::printActiveRaid($raid_id, $user_id);
						
						$dataToUser['reply_markup'] =  Raids::printActiveRaidButtons($raid_id, $user_id);
						
						Request::sendMessage($dataToUser);
						
						$dataToUser['text'] = Raids::printActiveRaid($raid_id, $user_friend_id);
						
						$dataToUser['reply_markup'] =  Raids::printActiveRaidButtons($raid_id, $user_friend_id);
						
						$dataToUser['chat_id'] = $user_friend_id;
						
						Request::sendMessage($dataToUser);
						
					break;
					
					case '9':
						$user_friend_id = $parameter[3];
						$user = RaidsDB::getRaidExUsersFilter($raid_id, null,null, $user_friend_id);
						
						if(empty($user)){
							RaidsDB::insertRaidExUser($raid_id, $user_id, 1 ,1, $user_friend_id);
							RaidsDB::insertRaidExUser($raid_id, $user_friend_id, 0 ,1, $user_id);
							
							$editedUserMessage['text'] = "Hai chiesto a <i>".RaidsDB::getUsername($user_friend_id)."</i> di poter ricevere il pass Ex.";
							Request::editMessageText($editedUserMessage);
							
							$dataToUser['text'] = Raids::printActiveRaid($raid_id, $user_id);
						
							$dataToUser['reply_markup'] =  Raids::printActiveRaidButtons($raid_id, $user_id);
							
							Request::sendMessage($dataToUser);
							
							Raids::askPassEx($raid_id,$user_id,$user_friend_id,"richiesta");
							
						}else{
							$editedUserMessage['text'] = "Qualcuno ti ha ninjato l'utente!";
							$editedUserMessage['reply_markup'] = new InlineKeyboardButton(['text' => 'Indietro', 'callback_data' => "passex,$raid_id,-1"]);
							Request::editMessageText($editedUserMessage);
						}
					break;
					
					case '10':
					case '11':
						$user_friend_id = $parameter[3];
						if($status == 10){
							RaidsDB::insertRaidExUser($raid_id, $user_id, 0 ,0, $user_friend_id);
							RaidsDB::insertRaidExUser($raid_id, $user_friend_id, 1 ,0, $user_id);
							$editedUserMessage['text'] = sprintf("Hai correttamente ricevuto il pass per il raid Ex da <i>%s</i>.",RaidsDB::getUsername($user_friend_id));
						}else{
							RaidsDB::insertRaidExUser($raid_id, $user_id, 0 ,1, null);
							RaidsDB::insertRaidExUser($raid_id, $user_friend_id, 1 ,1, null);
							$editedUserMessage['text'] = sprintf("Hai rifiutato l'invitato da <i>%s</i>.",RaidsDB::getUsername($user_friend_id));
						}

						Request::editMessageText($editedUserMessage);
						
						$dataToUser['text'] = Raids::printActiveRaid($raid_id, $user_id);
						
						$dataToUser['reply_markup'] =  Raids::printActiveRaidButtons($raid_id, $user_id);
						
						Request::sendMessage($dataToUser);
						
						$dataToUser['text'] = Raids::printActiveRaid($raid_id, $user_friend_id);
						
						$dataToUser['reply_markup'] =  Raids::printActiveRaidButtons($raid_id, $user_friend_id);
						
						$dataToUser['chat_id'] = $user_friend_id;
						
						Request::sendMessage($dataToUser);
						
					break;
					case '12':
						RaidsDB::insertRaidExUser($raid_id, $user_id, 1 ,1);
						
						$editedUserMessage['text'] = Raids::printActiveRaid($raid_id, $user_id);
						$editedUserMessage['reply_markup'] = Raids::printActiveRaidButtons($raid_id, $user_id);
						
						Request::editMessageText($editedUserMessage);
					break;
				}
				$data = [
					'callback_query_id' => $callback_query_id,
					'text'              => (isset($callback_feedback)?$callback_feedback:""),
					'show_alert'        => false,
					'cache_time'        => 7,
				];
			break;
			/************************************ statistiche *************************************/
			case 'statistiche': //se la callback proviene dal comando statistiche
				
				$user_id = $parameter[1];
				$statType= $parameter[2]; //local,global
				$field = $parameter[3];
				
				$editedAdminMessage = Raids::showStats($user_id,$field);
				$editedAdminMessage['message_id'] = $callback_message_id;
							
				Request::editMessageText($editedAdminMessage);
				

			$data = [
					'callback_query_id' => $callback_query_id,
					'text'              => (isset($callback_feedback)?$callback_feedback:""),
					'show_alert'        => false,
					'cache_time'        => 7,
				];

			break;
			/************************************ nests *************************************/
			case 'nests': //se la callback proviene dal comando statistiche
				
				$chat_id = $parameter[1];
				$status = $parameter[2]; //0 = gestione nidi
				$nest_id = $parameter[3]; // 0 = nessun nido
				
				// $dataToAdmin = [
					// 'chat_id' => 14303576,
					// 'text'    => $callback_data,//Raids::printChannelsButton($channels),
						// ];				
					// Request::sendMessage($dataToAdmin);
				$editedMessage = Raids::showNests($chat_id);
				switch($status){
					
					case '-1':
					case '0':
					
						$editedMessage['reply_markup'] = Raids::nestsButtons($chat_id,$status);
					break;
					case '1':
					case '2':
					
						$nest = RaidsDB::getNest($nest_id);
						if($status ==2){
							RaidsDB::updateNestStatus($nest_id,($nest['enabled']?0:1));
							$nest = RaidsDB::getNest($nest_id);
						}
						$editedMessage['text'] = "<b>Modifica Nido: " . $nest['zona'] . "</b>" . PHP_EOL . PHP_EOL;
						$editedMessage['text'] .= "Status: " . ($nest['enabled']?"Attivo":"Disattivato") . PHP_EOL;
						$editedMessage['text'] .= "Pokemon: " . $nest['pokemon'] . PHP_EOL;
						$editedMessage['text'] .= "Ultimo aggiornamento: " . $nest['date_add'] . PHP_EOL;
						
						$editedMessage['reply_markup'] = Raids::nestsButtons($chat_id,$status,$nest_id);
					break;
					case '3':
						$this->conversation = new Conversation($chat_id, $chat_id, 'nests');

						$notes = &$this->conversation->notes;
						!is_array($notes) && $notes = [];
						
						$nest = RaidsDB::getNest($nest_id);
						//cache data from the tracking session if any
						$state = 0;
						$notes['command'] = 'upd';
						$notes['zona'] = $nest['zona'];
						$notes['nest_id'] = $nest_id;
						
						if (isset($notes['state'])) {
							$state = $notes['state'];
						}
						$this->conversation->update();
						
						$editedMessage['text'] = "<b>Modifica Nido: " . $nest['zona'] . "</b>" . PHP_EOL . PHP_EOL;
						$editedMessage['text'] = "Inserisci il nuovo pokemon per questo nido:";
						
					break;
					case '9':
						RaidsDB::disableNestsStatus();
						$editedMessage['reply_markup'] = Raids::nestsButtons($chat_id,0,$nest_id);
					break;
					case '10':
						$callback_feedback = "Premi sull'id corrispondente al nido che vuoi modificare per avere le opzioni.";
						$data['show_alert'] = true;
						$editedMessage['reply_markup'] = Raids::nestsButtons($chat_id,0,$nest_id);
					break;
					case '11':
						$editedMessage = Raids::showNests($chat_id,true);
						$editedMessage['reply_markup'] = Raids::nestsButtons($chat_id,-1);
						
				}
				
				$editedMessage['message_id'] = $callback_message_id;
							
				Request::editMessageText($editedMessage);
				

			$data = [
					'callback_query_id' => $callback_query_id,
					'text'              => (isset($callback_feedback)?$callback_feedback:""),
					'show_alert'        => false,
					'cache_time'        => 0,
				];

			break;
			/************************************ gymex *************************************/
			case 'gymEx': //se la callback proviene dal comando gymex
				$gym_id = $parameter[1];
				$active = $parameter[2];
				
				RaidsDB::setGymEx($gym_id,$active);
				
				$editedAdminMessage = Raids::gymEx($callback_user_id);
				$editedAdminMessage['message_id'] = $callback_message_id;
							
				
				$data = [
					'callback_query_id' => $callback_query_id,
					'text'              => Emoji::thumbsUp(),
					'show_alert'        => false,
					'cache_time'        => 7,
				];
				
				Request::editMessageText($editedAdminMessage);
			break;
			
			
			case 'gymExPubblica': 
				
				$palestre = RaidsDB::getGymEx(1);
				
				$text = "<b>Palestre Ex selezionate da oggi " . date('d/m/Y').":</b>" . PHP_EOL . PHP_EOL;
		
				foreach($palestre as $palestra)
					$text.= Emoji::checkMarkButton() .' <a href="https://maps.google.com/maps?q=' . $palestra['lat'] . ',' . $palestra['lon']. '">' . $palestra['gym_name'] . '</a>' . PHP_EOL;
				
				$data_to_channel = [
					'chat_id' => -1001259440680,
					'parse_mode' => "html",
					'disable_web_page_preview' => true,
					'text'    => $text,
				];				
				$result = Request::sendMessage($data_to_channel);
				
				Request::pinChatMessage(['message_id' => $result->result->message_id, 'chat_id' => -1001259440680]);
				
				$data = [
					'callback_query_id' => $callback_query_id,
					'text'              => Emoji::thumbsUp(),
					'show_alert'        => false,
					'cache_time'        => 7,
				];
			break;
			/************************************ segnalaraid *************************************/
			case 'segnalaraid': //se la callback proviene dal comando segnalaRaid
				// if(strpos($username,"\\") === false)
				$username = str_replace("_","\_",$callback_username);
				$comando = $parameter[1];
				$raidlevel  = [Emoji::CHARACTER_KEYCAP_4,Emoji::CHARACTER_KEYCAP_5];
				
				if ($username =='') return Request::emptyResponse();
				
				Raids::logChannel("@$callback_username (<code>$callback_user_id</code>,<code>$refeer</code>) preme <b>$comando</b>");
				
				$raid = RaidsDB::getRaid($callback_message_id,$callback_chat_id);
				
				$editText = $raid["fixed_text"];
				$username = " @" . $username;
				switch($comando){
					case 'Partecipa':
						if(isset($parameter[3])){
							$dataToAdmin = [
									'chat_id' => 14303576,
									'text'    => $callback_data,
							];				
						Request::sendMessage($dataToAdmin);
						}
							
						
						$orario = $parameter[2]; //voglio partecipare a quest'ora
						
						
						$raidlevel  = [Emoji::CHARACTER_KEYCAP_4,Emoji::CHARACTER_KEYCAP_5];
						$lines = explode(PHP_EOL, $callback_message_text);
						foreach($lines as $line){
							if(strpos($line,"Partecipano") !== false){
								if (strpos($line,$orario) !== false){
									if (strpos($line,$username) !== false){
										$partecipo = true;
										
										// $line = str_replace($username,'',$line);
										$posnick = strpos($line,$username) + strlen($username);
										$plus = substr($line, $posnick, 2); //recupero quanti plus ci sono, output "+x"
										if (strpos($plus,"+") !== false){	//se ho già + nella stringa, aggiungo 1 al numero
											$totplus = intval($plus);
											if ($totplus == 9)
												$totplus = 9;
											else 
												$totplus += 1;
											
											$line = str_replace($username.$plus,$username . "+".$totplus,$line);
											// $line .= $username . "+".$totplus;
										}
										else{								//altrimenti inserisco +1
											$line = str_replace($username,'',$line);
											$line .= $username . "+1"; 	
										}
										
										
									}
									else{
										$partecipo = true;
										
										$line .= $username;
									}
								}
								$editText .= $line . PHP_EOL;
							}
							
							
							if(strpos($line,"🕰") !== false){
								$oraschiusa = explode(" ",$line);
							}	
							// if(strpos($line,$raidlevel[0]) !== false || strpos($line,$raidlevel[1]) !== false){
								// if(strpos($line,$raidlevel[0]) !== false)
									// $raidlevelKeyboard = 4;
								// else
									$raidlevelKeyboard = 5;
							// }	
								
							
						}
						
						$data = [
							'callback_query_id' => $callback_query_id,
							'text'              => ($partecipo)?Emoji::thumbsUp():Emoji::thumbsDown(),
							'show_alert'        => false,
							'cache_time'        => 7,
						];
						// $textWithEntities = Raids::parseEntitiesString($editText,$callback_entities);
						$textWithEntities = $editText;
						$editedAdminMessage = [
							'chat_id' => $callback_chat_id,
							'message_id' => $callback_message_id,
							'text' => $textWithEntities,
							'reply_markup' => Raids::raidButtonOrario($raidlevelKeyboard,date('Y-m-d ').$oraschiusa[1].":00"),
							'parse_mode' => 'html',
							
						];
						Request::editMessageText($editedAdminMessage);
						
						
					break; 
					
					case 'NonPartecipa':
						
						$partecipo = false;
						$raidlevel  = [Emoji::CHARACTER_KEYCAP_4,Emoji::CHARACTER_KEYCAP_5];
						$lines = explode(PHP_EOL, $callback_message_text);
						foreach($lines as $line){
							if(strpos($line,"Partecipano") !== false)
							{
								if (strpos($line,$username) !== false){
									
									
									$posnick = strpos($line,$username);
									$posnick += strlen($username);
									$plus = substr($line, $posnick, 3);
									if (strpos($plus,"+") !== false)
										$line = str_replace($username.$plus,'',$line);
									else
										$line = str_replace($username,'',$line);
								}
								$editText .= $line . PHP_EOL;
							}
								
							
							if(strpos($line,"🕰") !== false){
								$oraschiusa = explode(" ",$line);
							}	
							// if(strpos($line,$raidlevel[0]) !== false || strpos($line,$raidlevel[1]) !== false){
								// if(strpos($line,$raidlevel[0]) !== false)
									// $raidlevelKeyboard = 4;
								// else
									$raidlevelKeyboard = 5;
							// }	
								
							
						}
						
						$data = [
							'callback_query_id' => $callback_query_id,
							'text'              => ($partecipo)?Emoji::thumbsUp():Emoji::thumbsDown(),
							'show_alert'        => false,
							'cache_time'        => 7,
						];
						// $textWithEntities = Raids::parseEntitiesString($editText,$callback_entities);
						$textWithEntities = $editText;
						
						$editedAdminMessage = [
							'chat_id' => $callback_chat_id,
							'message_id' => $callback_message_id,
							'text' => $textWithEntities,
							'reply_markup' => Raids::raidButtonOrario($raidlevelKeyboard,date('Y-m-d ').$oraschiusa[1].":00"),
							'parse_mode' => 'html',
							
						];
						Request::editMessageText($editedAdminMessage);
						
						
					break;
						
					default:
						
						$segnalato = 0;
						$cancellazioneEffettuata = 0;
						$invioNotifica = 0;
						$raidlevel  = [Emoji::CHARACTER_KEYCAP_4,Emoji::CHARACTER_KEYCAP_5];
						$posted = $callback_query->getMessage()->getDate();
						$segnalatoalle = Time();
						$orariotrenta = "";
						$dove = '';
						$lines = explode(PHP_EOL, $callback_message_text);
						foreach($lines as $line){
							$cancellazione = 0;
							
							if(strpos($line,"🕰") !== false){
								$oraschiusa = explode(" ",$line);
								$oraintera = date('Y-m-d ').$oraschiusa[1].":00";
								$oraestesa = date_create_from_format("Y-m-d H:i:s",$oraintera);
								//date_add($oraestesa, date_interval_create_from_date_string('30 minutes'));
								$orariotrenta = $oraestesa->format('H:i');
								
							}
							
							if(strpos($line,"🏔") !== false){
								$dove = $line;
							}
							
							if(strpos($line,"Pokemon Segnalato:") !== false){ //controllo se è già stata effettuta la segnalazione
								$segnalato = 1;
								if((strpos($line,$username) !== false || $username == "KilluaFein") && strpos($line,strtoupper($comando)) !== false)
								{$cancellazione = 1;$cancellazioneEffettuata = 1;}
							}
							if(strpos($line,"Partecipano alle " . $orariotrenta ) !== false)
								if(!$segnalato){
									$line = "Pokemon Segnalato: " .
									$emojiPokemon[$comando] . strtoupper($comando) . $emojiPokemon[$comando] .
									" da$username alle ". Date('H:i:s') . PHP_EOL . $line;
									// $line = $line;
									
									$invioNotifica = 1;
								}
							if(!$cancellazione)	
								$editText .= $line . PHP_EOL;
							
							
							
							// if(strpos($line,$raidlevel[0]) !== false || strpos($line,$raidlevel[1]) !== false){
								// if(strpos($line,$raidlevel[0]) !== false)
									// $raidlevelKeyboard = 4;
								
								// else
									$raidlevelKeyboard = 5;
							
							// }
						}						
						
						// $textWithEntities = Raids::parseEntitiesString($editText,$callback_entities);
						$textWithEntities = $editText;
						$partecipo = false;					
						$editedAdminMessage = [
							'chat_id' => $callback_chat_id,
							'message_id' => $callback_message_id,
							'text' => $textWithEntities,
							'reply_markup' => Raids::raidButtonOrario($raidlevelKeyboard,date('Y-m-d ').$oraschiusa[1].":00"),
							'parse_mode' => 'html',
						];
						Request::editMessageText($editedAdminMessage);
						
						if ($interval = ($segnalatoalle - $posted) / 60 < 120 && $invioNotifica)
							//Raids::sendNotification($comando,$username,$dove);
						
						$data = [
							'callback_query_id' => $callback_query_id,
							'text'              => ($segnalato)?($cancellazioneEffettuata)?"Segnalazione cancellata!":"Pokemon già segnalato!":ucfirst($comando)." segnalato!",
							'show_alert'        => false,
							'cache_time'        => 7,
						];		
				}
			break;
			
			/************************************ segnalaraidtest *************************************/
			case 'segnalaraidtest': //se la callback proviene dal comando segnalaRaidTest
				
				$username = $callback_username;
				$comando = $parameter[1];
				$raidlevel  = [Emoji::CHARACTER_KEYCAP_4,Emoji::CHARACTER_KEYCAP_5];
				
				if ($username =='') return Request::emptyResponse();
				
				Raids::logChannel("@$callback_username (<code>$callback_user_id</code>,<code>$refeer</code>) preme <b>$comando</b>");
				switch($comando){
					case 'Partecipa':

						$editText = "";
						$orario = $parameter[2]; //voglio partecipare a quest'ora
						$username = " @" . $username;
						$raidlevel  = [Emoji::CHARACTER_KEYCAP_4,Emoji::CHARACTER_KEYCAP_5];
						$lines = explode(PHP_EOL, $callback_message_text);
						foreach($lines as $line){
							if(strpos($line,"Partecipano") !== false){
								if (strpos($line,$orario) !== false){
									if (strpos($line,$username) !== false){
										$partecipo = true;
										
										// $line = str_replace($username,'',$line);
										$posnick = strpos($line,$username) + strlen($username);
										$plus = substr($line, $posnick, 2); //recupero quanti plus ci sono, output "+x"
										if (strpos($plus,"+") !== false){	//se ho già + nella stringa, aggiungo 1 al numero
											$totplus = intval($plus);
											if ($totplus == 9)
												$totplus = 9;
											else 
												$totplus += 1;
											
											$line = str_replace($username.$plus,$username . "+".$totplus,$line);
											// $line .= $username . "+".$totplus;
										}
										else{								//altrimenti inserisco +1
											$line = str_replace($username,'',$line);
											$line .= $username . "+1"; 	
										}
										
										
									}
									else{
										$partecipo = true;
										
										$line .= $username;
									}
								}
							}
							$editText .= $line . PHP_EOL;
							
							if(strpos($line,"🕰") !== false){
								$oraschiusa = explode(" ",$line);
							}	
							// if(strpos($line,$raidlevel[0]) !== false || strpos($line,$raidlevel[1]) !== false){
								// if(strpos($line,$raidlevel[0]) !== false)
									// $raidlevelKeyboard = 4;
								// else
									$raidlevelKeyboard = 5;
							// }	
								
							
						}
						
						$data = [
							'callback_query_id' => $callback_query_id,
							'text'              => ($partecipo)?Emoji::thumbsUp():Emoji::thumbsDown(),
							'show_alert'        => false,
							'cache_time'        => 7,
						];
						$textWithEntities = Raids::parseEntitiesString($editText,$callback_entities);
						$editedAdminMessage = [
							'chat_id' => $callback_chat_id,
							'message_id' => $callback_message_id,
							'text' => $textWithEntities,
							'reply_markup' => Raids::raidButtonOrario($raidlevelKeyboard,date('Y-m-d ').$oraschiusa[1].":00"),
							'parse_mode' => 'Markdown',
							
						];
						Request::editMessageText($editedAdminMessage);
						
						
					break; 
					
					case 'NonPartecipa':
						$editText = "";
						$username = " @" . $username;
						$partecipo = false;
						$raidlevel  = [Emoji::CHARACTER_KEYCAP_4,Emoji::CHARACTER_KEYCAP_5];
						$lines = explode(PHP_EOL, $callback_message_text);
						foreach($lines as $line){
							if(strpos($line,"Partecipano") !== false)
									if (strpos($line,$username) !== false){
										
										$posnick = strpos($line,$username);
										$posnick += strlen($username);
										$plus = substr($line, $posnick, 3);
										if (strpos($plus,"+") !== false)
											$line = str_replace($username.$plus,'',$line);
										else
											$line = str_replace($username,'',$line);
									}
						$editText .= $line . PHP_EOL;
							
							if(strpos($line,"🕰") !== false){
								$oraschiusa = explode(" ",$line);
							}	
							// if(strpos($line,$raidlevel[0]) !== false || strpos($line,$raidlevel[1]) !== false){
								// if(strpos($line,$raidlevel[0]) !== false)
									// $raidlevelKeyboard = 4;
								// else
									$raidlevelKeyboard = 5;
							// }	
								
							
						}
						
						$data = [
							'callback_query_id' => $callback_query_id,
							'text'              => ($partecipo)?Emoji::thumbsUp():Emoji::thumbsDown(),
							'show_alert'        => false,
							'cache_time'        => 7,
						];
						$textWithEntities = Raids::parseEntitiesString($editText,$callback_entities);
						$editedAdminMessage = [
							'chat_id' => $callback_chat_id,
							'message_id' => $callback_message_id,
							'text' => $textWithEntities,
							'reply_markup' => Raids::raidButtonOrario($raidlevelKeyboard,date('Y-m-d ').$oraschiusa[1].":00"),
							'parse_mode' => 'Markdown',
							
						];
						Request::editMessageText($editedAdminMessage);
					break;
						
					default:
						$editText = "";
						$segnalato = 0;
						$cancellazioneEffettuata = 0;
						$invioNotifica = 0;
						$raidlevel  = [Emoji::CHARACTER_KEYCAP_4,Emoji::CHARACTER_KEYCAP_5];
						$posted = $callback_query->getMessage()->getDate();
						$segnalatoalle = Time();
						$orariotrenta = "";
						$dove = '';
						$lines = explode(PHP_EOL, $callback_message_text);
						foreach($lines as $line){
							$cancellazione = 0;
							
							if(strpos($line,"🕰") !== false){
								$oraschiusa = explode(" ",$line);
								$oraintera = date('Y-m-d ').$oraschiusa[1].":00";
								$oraestesa = date_create_from_format("Y-m-d H:i:s",$oraintera);
								//date_add($oraestesa, date_interval_create_from_date_string('30 minutes'));
								$orariotrenta = $oraestesa->format('H:i');
								
							}
							
							if(strpos($line,"🏔") !== false){
								$dove = $line;
							}
							
							if(strpos($line,"Pokemon Segnalato:") !== false){ //controllo se è già stata effettuta la segnalazione
								$segnalato = 1;
								if((strpos($line,$username) !== false || $username == "KilluaFein") && strpos($line,strtoupper($comando)) !== false)
								{$cancellazione = 1;$cancellazioneEffettuata = 1;}
							}
							if(strpos($line,"Partecipano alle " . $orariotrenta ) !== false)
								if(!$segnalato){
									$line = "Pokemon Segnalato: " .
									$emojiPokemon[$comando] . strtoupper($comando) . $emojiPokemon[$comando] .
									" da @$username alle ". Date('H:i:s') . PHP_EOL . $line;
									// $line = $line;
									
									$invioNotifica = 1;
								}
							if(!$cancellazione)	
								$editText .= $line . PHP_EOL;
							
							
							
							// if(strpos($line,$raidlevel[0]) !== false || strpos($line,$raidlevel[1]) !== false){
								// if(strpos($line,$raidlevel[0]) !== false)
									// $raidlevelKeyboard = 4;
								
								// else
									$raidlevelKeyboard = 5;
							
							// }
						}						
						
						$textWithEntities = Raids::parseEntitiesString($editText,$callback_entities);
						$partecipo = false;					
						$editedAdminMessage = [
							'chat_id' => $callback_chat_id,
							'message_id' => $callback_message_id,
							'text' => $textWithEntities,
							'reply_markup' => Raids::raidButtonOrario($raidlevelKeyboard,date('Y-m-d ').$oraschiusa[1].":00"),
							'parse_mode' => 'Markdown',
						];
						Request::editMessageText($editedAdminMessage);
						
						if ($interval = ($segnalatoalle - $posted) / 60 < 120 && $invioNotifica)
							Raids::sendNotification($comando,$username,$dove);
						
						$data = [
							'callback_query_id' => $callback_query_id,
							'text'              => ($segnalato)?($cancellazioneEffettuata)?"Segnalazione cancellata!":"Pokemon già segnalato!":ucfirst($comando)." segnalato!",
							'show_alert'        => false,
							'cache_time'        => 7,
						];		
				}
			break;
			
			
			/************************************ notifiche *************************************/
			case 'notifiche': //se la callback proviene dal comando notifiche
				
				$username = $callback_username;
				$status = $parameter[1]; //se ho premuto attiva, disattiva, icona pokemon
				$field = $parameter[2]; //identifico il campo da modificare
				$tier = $parameter[3]; //identifico quale parte della tastiera mostrare

				$data = [
							'callback_query_id' => $callback_query_id,
							'show_alert'        => false,
							'cache_time'        => 7,
						];
				
				switch($status){
					case 0:
					case 1:
						RaidsDB::setNotificationStatus($field, $status, $callback_chat_id, $username);
						$data['text'] = Emoji::thumbsUp();
						
						break;
						
					case -1:
						$data['text'] = "Permette di ricevere o meno la notifica per ". ucfirst($field);
						$data['show_alert'] = true;
						break;
					default:
				}
				
				$editedPrivateMessage = [
						'chat_id' => $callback_chat_id,
						'message_id' => $callback_message_id,
						'text' => Raids::notification($callback_chat_id), 
						'reply_markup' => Raids::notificationButton($tier),
						'parse_mode' => 'Markdown',
					];
				Request::editMessageText($editedPrivateMessage);
			break;			
		}
		
		
       return Request::answerCallbackQuery($data);
    }
}