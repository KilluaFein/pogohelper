<?php
/**
 * This file is part of the TelegramBot package.
 *
 * (c) Avtandil Kikabidze aka LONGMAN <akalongman@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Longman\TelegramBot\Commands\SystemCommands;

use Longman\TelegramBot\Commands\SystemCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Raids;
use Longman\TelegramBot\Request;
use Spatie\Emoji\Emoji;

/**
 * Channel post command
 *
 * Gets executed when a new post is created in a channel.
 */
class ChannelpostCommand extends SystemCommand
{
    /**
     * @var string
     */
    protected $name = 'channelpost';

    /**
     * @var string
     */
    protected $description = 'Handle channel post';

    /**
     * @var string
     */
    protected $version = '1.0.0';

    /**
     * Command execute method
     *
     * @return \Longman\TelegramBot\Entities\ServerResponse
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function execute(): ServerResponse
    {
        $channel_post = $this->getUpdate()->getChannelPost();
        $channel_id = $channel_post->getChat()->getId();
        $channel_text = trim($channel_post->getText(true));
        $channel_to_PK = -1001063890554; //14303576 ;// Team Istinto Avvistamenti
        $channel_to_GE = -1001259440680; //14303576 ;// CrossRaid EX Channel
        $channel_to_GM = -1001134006412; //14303576 ;// CrossRaid Channel // 96762970; Davide //
        $channel_from_PK = -1001077335110; // Team Istinto Avvistamenti CITM
        $channel_from_GM = -1001259567022; // Team Istinto Raid CITM

        $pokemote = [
				Emoji::dogFace(),Emoji::catFace(),Emoji::mouseFace(),Emoji::hamster(),
				Emoji::rabbitFace(),Emoji::bear(),Emoji::panda(),Emoji::koala(),
				Emoji::tigerFace(),Emoji::lionFace(),Emoji::cowFace(),Emoji::pigFace(),
				Emoji::pigNose(),Emoji::frogFace(),Emoji::octopus(),Emoji::monkeyFace(),
				Emoji::monkey(),Emoji::chicken(),Emoji::penguin(),Emoji::bird(),
				Emoji::babyChick(),Emoji::hatchingChick(),Emoji::wolf(),Emoji::boar(),
				Emoji::horseFace(),Emoji::unicornFace(),Emoji::honeybee(),Emoji::bug(),
				Emoji::snail(),Emoji::ladyBeetle(),Emoji::ant(),Emoji::spider(),
				Emoji::scorpion(),Emoji::crab(),Emoji::snake(),Emoji::turtle(),
				Emoji::tropicalFish(),Emoji::fish(),Emoji::blowfish(),Emoji::dolphin(),
				Emoji::spoutingWhale(),Emoji::whale(),Emoji::crocodile(),Emoji::leopard(),
				Emoji::tiger(),Emoji::waterBuffalo(),Emoji::ox(),Emoji::cow(),
				Emoji::camel(),Emoji::twoHumpCamel(),Emoji::elephant(),Emoji::goat(),
				Emoji::ram(),Emoji::ewe(),Emoji::horse(),Emoji::pig(),
				Emoji::rat(),Emoji::mouse(),Emoji::rooster(),Emoji::turkey(),
				Emoji::dove(),Emoji::dog(),Emoji::poodle(),Emoji::cat(),
				Emoji::rabbit(),Emoji::chipmunk(),Emoji::dragonFace(),Emoji::dragon(),
				Emoji::snowmanWithoutSnow(),
		];

        $frase = [
            " *%s* sbiccato!",
            " Millo, un *%s*!",
            " Là unu cazz'e *%s*!",
            " Ceee, un *%s*!",
            " E pitticcu su *%s*!",
            " C'è cosa spawnata! Un *%s*!",
            " Minzega, là un *%s*!",
            " Ho appena svisato un *%s*!",
            " Laghe ne è uscito *%s*, movirindi!",
            " Non hai capito che ho svisato un *%s*!",

        ];

        if ($channel_id == $channel_from_PK && (strpos($channel_text, "poketrack") !== false || strpos($channel_text, "goo.gl") !== false)) {

            $splitted = explode(PHP_EOL, $channel_text);

            $pokemonName = $splitted[0];

            if (strpos($splitted[1], "IV") !== false) {
                $iv = $splitted[1];
                $cp = $splitted[2];
                $m1 = str_replace("Move 1: ", "", str_replace(" fast", "", $splitted[3]));
                $m2 = str_replace("Move 2: ", "", $splitted[4]);
                $timeleft = $splitted[5];
                $spottedtime = explode(": ", $splitted[6]);
                $spottedat = $spottedtime[1];
                if (strpos($splitted[7], "map.poketrack.xyz") !== false) {
                    $headers['Location'] = $splitted[7];
                } else {
                    $headers = get_headers($splitted[7], 1);
                }

            } else {
                $timeleft = $splitted[1];
                $spottedtime = explode(": ", $splitted[2]);
                $spottedat = $spottedtime[1];
                if (strpos($splitted[3], "map.poketrack.xyz") !== false) {
                    $headers['Location'] = $splitted[3];
                } else {
                    $headers = get_headers($splitted[3], 1);
                }

            }

            $text = $pokemote[array_rand($pokemote, 1)] . sprintf($frase[array_rand($frase, 1)], $pokemonName) . PHP_EOL;

            $dataToAdmin = [
                'chat_id' => 14303576,
                'text' => $pokemote[array_rand($pokemote, 1)] . sprintf($frase[array_rand($frase, 1)], $pokemonName) . PHP_EOL,
            ];
            //Request::sendMessage($dataToAdmin);
            if (strpos($splitted[1], "IV") !== false) {
                $text .= $iv . PHP_EOL;
                $text .= $cp . PHP_EOL;
                $text .= "M1: " . ucwords($m1) . PHP_EOL;
                $text .= "M2: " . ucwords($m2) . PHP_EOL;
            }
            $text .= Emoji::alarmClock() . " " . $timeleft . PHP_EOL;
            $text .= Emoji::hourglassNotDone(). " " . $spottedat . PHP_EOL;
            $spottedplace = preg_split("/([?&=])/", $headers['Location']);

            $full_address = Raids::getFullAddress($spottedplace[2], $spottedplace[4]);

            $text .= Emoji::snowCappedMountain()." [$full_address](http://maps.google.com/maps?q=" . $spottedplace[2] . "," . $spottedplace[4] . ")" . PHP_EOL;

            $data_to_channel = [
                'chat_id' => $channel_to_PK,
                'parse_mode' => 'markdown',
                'text' => $text,
            ];

            Request::sendMessage($data_to_channel);
        }
        
        return parent::execute();
    }
}
