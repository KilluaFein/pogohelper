<?php
/**
 * This file is part of the TelegramBot package.
 *
 * (c) Avtandil Kikabidze aka LONGMAN <akalongman@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Longman\TelegramBot\Commands\SystemCommands;

use Longman\TelegramBot\Commands\SystemCommand;
use Longman\TelegramBot\Entities\InlineKeyboard;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Raids;
use Longman\TelegramBot\RaidsDB;

/**
 * Start command
 */
class StartCommand extends SystemCommand
{
    /**
     * @var string
     */
    protected $name = 'start';

    /**
     * @var string
     */
    protected $description = 'Avvia il bot';

    /**
     * @var string
     */
    protected $usage = '/start';

    /**
     * @var string
     */
    protected $version = '1.1.0';

	
	protected $private_only = true;
    /**
     * Command execute method
     *
     * @return mixed
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function execute(): ServerResponse
    {
        $message = $this->getMessage();
        $chat    = $message->getChat();
        $type    = $chat->getType();			
		$refeer   = trim($message->getText(true));
        $chat_id = $message->getChat()->getId();
        $user_id = $message->getFrom()->getId();
		$user_firstname = $message->getFrom()->getFirstName();
		$user_username = $message->getFrom()->getUsername();
		
		
		if($type != "private"){
			return Request::emptyResponse();
		}
		
		$data = [];                        	        // Set up the new message data
        $data['chat_id'] = $chat_id;				// Set Chat ID to send the message to
        $data['parse_mode'] = "markdown";				// Set Chat ID to send the message to
		
		
		
		$isEnabled = RaidsDB::isEnabledUser($chat_id);
		if($isEnabled == 0){
			if($refeer == null){
				if(RaidsDB::getRefeer($user_id) == null){
					if($user_username == null){
						$data['text'] = "Per usufruire di questo bot, devi impostare un username su telegram. [Ecco come puoi fare](https://t.me/SardinianPoGoBot?start=username). Dopo che hai fatto, ripremi [qui](https://t.me/SardinianPoGoBot?start=$refeer)";
					}
					else{
					
						$data['text'] = "Benvenuto @$user_username! Sembra che tu abbiamo avviato il bot senza passare dal link del tuo referente. Controlla di avere il link giusto e riprova!" .PHP_EOL;
						$data['text'].= "Se invece vuoi candidarti come admin per un nuovo canale, premi qui /makemeadmin$user_id";
					}
				}
				elseif(RaidsDB::getRefeer($user_id) == $user_id)
				$data['text'] = "Se leggi questo testo, probabilmente hai appena effettuato una donazione ed è in attesa di verifica!";
				else
					$data['text'] = "Un amministratore ha già preso in carico la tua richiesta, premere Start più volte non lo farà andare più veloce";
			}
			elseif(RaidsDB::getRefeer($user_id) != null){
				$data['text'] = "Un amministratore ha già preso in carico la tua richiesta, premere Start più volte non lo farà andare più veloce";
			}	
			
			elseif($refeer == "username")
				$data['text'] = "COME IMPOSTARE UN USERNAME?". PHP_EOL . PHP_EOL ."Per impostare un username vai nelle IMPOSTAZIONI di Telegram, quindi premi su USERNAME e scegline uno. Poi salva.\nSemplice, no?";
			else{
				$refeers = RaidsDB::getRefeers();
				if (in_array($refeer,$refeers)){
					
				
					RaidsDB::setNotificationStatus('Refeer', $refeer, $user_id, $user_username);
			
					$data['text'] = "Benvenuto @$user_username! Devi attendere che un amministratore ti abiliti all'uso di questo bot. Il tuo referente è @".RaidsDB::getUsername($refeer);
				
					$datatoadmin= [
						'chat_id' => $refeer,
						'text'	=> "@$user_username ($chat_id) richiede di essere abilitato all'uso del bot.",
						'reply_markup' => Raids::enableDisableButton($chat_id,$refeer),
					];
					
					Raids::logChannel("@$user_username (<code>$chat_id</code>,<code>$refeer</code>) richiede di essere abilitato all'uso del bot.");
					Request::sendMessage($datatoadmin);				
				
				}
				else{
					$data['text'] = "Sembra che il referente che hai usato non sia admin di nessun canale! Controlla il link o fatti dare il nuovo dal tuo amministratore di zona e riprova";
				}
				
				
				
			}
		}
		elseif ($isEnabled == -1){
			$data['text'] ="";
			return Request::emptyResponse();
		}
		elseif ($isEnabled == 1)
			$data['text'] = "Sei già abilitato!";
		elseif ($isEnabled == 2)
			$data['text'] = "Sei già abilitato!";
			
        return Request::sendMessage($data);
    }
}
