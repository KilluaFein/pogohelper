<?php
/**
 * This file is part of the TelegramBot package.
 *
 * (c) Avtandil Kikabidze aka LONGMAN <akalongman@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Longman\TelegramBot\Commands\SystemCommands;

use Longman\TelegramBot\Commands\SystemCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Entities\Chat;
use Longman\TelegramBot\Entities\User;
use Longman\TelegramBot\Entities\Update;
use Longman\TelegramBot\Conversation;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Raids;
use Longman\TelegramBot\RaidsDB;

/**
 * Generic message command
 *
 * Gets executed when any type of message is sent.
 */
class GenericmessageCommand extends SystemCommand
{
    /**
     * @var string
     */
    protected $name = 'genericmessage';

    /**
     * @var string
     */
    protected $description = 'Handle generic message';

    /**
     * @var string
     */
    protected $version = '1.1.0';

    /**
     * @var bool
     */
    protected $need_mysql = true;
	
	


    /**
     * Command execute method if MySQL is required but not available
     *
     * @return \Longman\TelegramBot\Entities\ServerResponse
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function executeNoDb(): ServerResponse
    {
        // Do nothing
        return Request::emptyResponse();
    }

    /**
     * Command execute method
     *
     * @return \Longman\TelegramBot\Entities\ServerResponse
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function execute(): ServerResponse
    {
        $message 	 = $this->getMessage();
		$chat    	= $message->getChat();
        $user    	= $message->getFrom();
        $chat_id 	= $chat->getId();
        $user_id 	= $user->getId();
		$portalMapBot_id = 146534872;
		$enabled_user = [14303576,209242427,95080075,86701420,144667398]; //io, alemoi,rodrigo,pod,pirro
		
		//If a conversation is busy, execute the conversation command after handling the message
        $conversation = new Conversation(
            $user_id,
            $chat_id
        );
		

        //Fetch conversation command if it exists and execute it
        if ($conversation->exists() && ($command = $conversation->getCommand())) {
            return $this->telegram->executeCommand($command);
        }
		
		if($message->getForwardFromChat() !== null && trim($message->getText(true)) != null && $message->getForwardFromChat()->getType() ==="channel"){
			Raids::manageNewChannel($message->getForwardFromChat(),$user_id);
		}
		
		$forward_from = $message->getForwardFrom();
        if ($forward_from instanceof User)  
			if ($forward_from->getId() == $portalMapBot_id and in_array($user_id,$enabled_user)){
				
				$splitted = explode(PHP_EOL,trim($message->getText(true)));
				$portal_name = trim(str_replace("(Intel)","",$splitted[0]));
				if(strpos($splitted[4],"Coords:") !== false){
					$latlon = explode(",",str_replace("Coords: ","",$splitted[4]));
					$portal_original_id = $splitted[6];
				}else{
					$latlon = explode(",",str_replace("Coords: ","",$splitted[5]));
					$portal_original_id = $splitted[7];
				}
				$portal_lat  = preg_replace("%[^0-9.]%","",$latlon[0]);
				$portal_lon  = preg_replace("%[^0-9.]%","",$latlon[1]);
				
				$portal_link = null;
				$entities = $message->getEntities();
				
				foreach($entities as $entity)
					if($entity->getType() == 'text_link' and self::strposa($entity->getUrl(), ['googleusercontent','ggpht']) !== false)
						$portal_link = $entity->getUrl();
				
				if($portal_link === null)
					Request::sendMessage( ['chat_id' => 14303576,'text' => "Palestra $portal_name senza link, da controllare"]);
				
				$gym = RaidsDB::insertNewGym($portal_name,$portal_lat,$portal_lon,$portal_original_id,$portal_link);
				
				$this->replyToUser("Modificata/Inserita la nuova palestra $portal_name con ID ".$gym['gym_id']);
				// $this->replyToUser($splitted[4]." ".print_r($latlon,true));
			}
			
        return Request::emptyResponse();
    }
	
	public function strposa($haystack, $needle, $offset=0) {
		if(!is_array($needle)) $needle = array($needle);
		foreach($needle as $query) {
			if(strpos($haystack, $query, $offset) !== false) return true; // stop on first true result
		}
		return false;
	}
}