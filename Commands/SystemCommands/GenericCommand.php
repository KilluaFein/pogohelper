<?php
/**
 * This file is part of the TelegramBot package.
 *
 * (c) Avtandil Kikabidze aka LONGMAN <akalongman@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Longman\TelegramBot\Commands\SystemCommands;

use Longman\TelegramBot\Commands\SystemCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Request;

/**
 * Generic command
 */
class GenericCommand extends SystemCommand
{
    /**
     * @var string
     */
    protected $name = 'Generic';

    /**
     * @var string
     */
    protected $description = 'Handles generic commands or is executed by default when a command is not found';

    /**
     * @var string
     */
    protected $version = '1.0.0';

    /**
     * Command execute method
     *
     * @return mixed
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function execute(): ServerResponse
    {
        $message = $this->getMessage();
        $chat_id = $message->getChat()->getId();
        $user_id = $message->getFrom()->getId();
        $command = $message->getCommand();
        $text = trim($message->getText(true));
		
		//If the user is and admin and the command is in the format "/whoisXYZ", call the /whois command
        if (stripos($command, 'whois') === 0 && $this->telegram->isAdmin($user_id)) {
            return $this->telegram->executeCommand('whois');
        }
		if (in_array($user_id, $this->telegram->getAdminList()) && strtolower(substr($command, 0, 13)) == 'abilitautente') {
            return $this->telegram->executeCommand('abilitautente', $this->update);
        }
        if (in_array($user_id, $this->telegram->getAdminList()) && strtolower(substr($command, 0, 16)) == 'disabilitautente') {
            return $this->telegram->executeCommand('disabilitautente', $this->update);
        }
        if (in_array($user_id, $this->telegram->getAdminList()) && strtolower(substr($command, 0, 4)) == 'nest') {
            return $this->telegram->executeCommand('nest', $this->update);
        }
        if (strtolower(substr($command, 0, 11)) == 'makemeadmin') {
            return $this->telegram->executeCommand('makemeadmin', $this->update);
        }
        if (strtolower(substr($command, 0, 6)) == 'cancel') {
            return $this->telegram->executeCommand('cancel', $this->update);
        }
        return parent::execute();
    }
}
