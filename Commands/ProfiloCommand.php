<?php

namespace Longman\TelegramBot\Commands\UserCommands;

use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Entities\Chat;
use Longman\TelegramBot\Conversation;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Raids;
use Longman\TelegramBot\RaidsDB;


class ProfiloCommand extends UserCommand
{
	protected $name = 'profilo';                      
    protected $description = 'Visualizza il tuo profilo e ti permette di modificare varie impostazioni'; 
    protected $usage = '/profilo';                    // Usage of your command
    protected $version = '0.2.0';                  
	protected $private_only = true;
    
	/**
     * @var bool
     */
    protected $need_mysql = true;

    /**
     * Conversation Object
     *
     * @var \Longman\TelegramBot\Conversation
     */
    protected $conversation;

	
    public function execute(): ServerResponse
    {

        $message = $this->getMessage();		     // Get Message object
		$chat = $message->getChat();
        $user = $message->getFrom();
		$type    = $chat->getType();	
		$value   = trim($message->getText(true));		
		$command = $message->getCommand();
        $user_id = $user->getId(); // Get the current Chat ID
		$chat_id = $chat->getId();
		
		if(RaidsDB::isEnabledUser($user_id) != 1 )
			return Request::emptyResponse();
		
		$conversation = new Conversation($user_id, $chat_id);
		
		if ($conversation->exists() && ($command = $conversation->getCommand())) {
			
			$this->conversation = new Conversation($user_id, $chat_id, $this->getName());
			$notes = &$this->conversation->notes;
					           
			$field = $notes['command'];
			
			if($field == "datainizio")
				
				$value = date_format(date_create_from_format("d/m/Y",$value),'Y-m-d');
			
			$value = str_replace(".","", $value );
			$notes['value'] = $value;
			
			$this->conversation->update();
			
							
			RaidsDB::setUserDetail($user_id,RaidsDB::getUsername($user_id), $field, $value);
		    
			unset($notes['state']);
			$this->conversation->stop();
        }

		$data = Raids::profile($user_id,($conversation->exists()?"edit":"full"));

		return Request::sendMessage($data);	
	}
}