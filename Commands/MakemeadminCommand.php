<?php

namespace Longman\TelegramBot\Commands\UserCommands;

use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Entities\Chat;
use Longman\TelegramBot\Entities\InlineKeyboard;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Raids;
use Longman\TelegramBot\RaidsDB;


class MakemeadminCommand extends UserCommand
{
 /**
     * @var string
     */
    protected $name = 'makemeadmin';

    /**
     * @var string
     */
    protected $description = 'Promuove un utente ad admin di un canale';

    /**
     * @var string
     */
    protected $usage = '/makemeadmin';

    /**
     * @var string
     */
    protected $version = '0.1.0';
	
    /**
     * @var bool
     */
    protected $show_in_help = false;
	
	
	protected $private_only = true;
	
    public function execute(): ServerResponse
    {
		
		$message = $this->getMessage();		     // Get Message object
        $chat    = $message->getChat();
        $user    = $message->getFrom();
        $chat_id = $chat->getId();
        $user_id = $user->getId();
		$type    = $chat->getType();		
		$username = $user->getUsername();
		$command = $message->getCommand();
	
		if($type != "private")
			return Request::emptyResponse();
	
	
		$dataToUser = [							//preparo la risposta
					'chat_id' => $user_id,
					'parse_mode' => 'html',
					'disable_web_page_preview' => 'true',
					'text' => 'Niente di utile qui.',
				];	
	
		if($command != "makemeadmin"){
			
			$new_admin = str_replace('makemeadmin','',$command);
			if ($new_admin != $user_id)
				$dataToUser['text'] = "Non puoi far richieste per altre persone.";
			else{
				$dataToUser = Raids::makeMeAdmin("makemeadmin,user,$user_id,0");
				Raids::logChannel("@$username (<code>$user_id</code>) #request #newadmin");
			
			}
		}

		return Request::sendMessage($dataToUser);
	}
}	