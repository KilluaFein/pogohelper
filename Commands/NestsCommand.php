<?php
/**
 * This file is part of the TelegramBot package.
 *
 * (c) Avtandil Kikabidze aka LONGMAN <akalongman@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Longman\TelegramBot\Commands\UserCommands;

use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Entities\KeyboardButton;
use Longman\TelegramBot\Conversation;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Raids;
use Longman\TelegramBot\RaidsDB;

/**
 * User "/survey" command
 */
class NestsCommand extends UserCommand
{
 /**
     * @var string
     */
    protected $name = 'nests';

    /**
     * @var string
     */
    protected $description = 'Mostra i nidi scoperti dagli utenti che hanno la possibilità di scansionare. Comando disponibile solo per il Team Istinto Cagliari';

    /**
     * @var string
     */
    protected $usage = '/nests';

    /**
     * @var string
     */
    protected $version = '0.1.0';

    /**
     * @var bool
     */
    protected $need_mysql = true;

    /**
     * Conversation Object
     *
     * @var \Longman\TelegramBot\Conversation
     */
    protected $conversation;

	
	protected $private_only = false;
	
    /**
     * Command execute method
     *
     * @return \Longman\TelegramBot\Entities\ServerResponse
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function execute(): ServerResponse
    {
        $message = $this->getMessage();

        $chat    = $message->getChat();
        $type    = $chat->getType();
        $user    = $message->getFrom();
		$command = $message->getCommand();
        $subcommand   = trim($message->getText(true));
        $text   = trim($message->getText(true));
        $chat_id = $chat->getId();
        $user_id = $user->getId();
		$username = $user->getUsername();

		if(RaidsDB::isEnabledUser($user_id) != 1 )
			return Request::emptyResponse();
		
		if(RaidsDB::isInstinctPlayer($user_id) != "Giallo" || 
		   RaidsDB::getRefeer($user_id) != 14303576 )
			
			return $this->replyToUser("Devi impostare il team dal /profilo");
			
		$conversation = new Conversation($user_id, $chat_id);
		
		if ($conversation->exists() && ($command = $conversation->getCommand())) {
			
			$this->conversation = new Conversation($user_id, $chat_id, $this->getName());
			$notes = &$this->conversation->notes;
					           
			$subcommand = $notes['command'];
		    
        }
		
		if($subcommand == "add" || $subcommand == "upd" || strpos($subcommand,"del") !== false ){
			if($user_id == 14303576 || $user_id == 209242427){
				//Preparing Response
				$data = [
					'chat_id' => $chat_id, 
					'markdown' => 'html', 
				];
				
				if($subcommand == "add"){
					//Conversation start
					$this->conversation = new Conversation($user_id, $chat_id, $this->getName());

					$notes = &$this->conversation->notes;
					!is_array($notes) && $notes = [];

					//cache data from the tracking session if any
					$state = 0;
					$notes['command'] = 'add';
					
					if (isset($notes['state'])) {
						$state = $notes['state'];
					}

					$result = Request::emptyResponse();

					//State machine
					//Entrypoint of the machine state if given by the track
					//Every time a step is achieved the track is updated
					switch ($state) {
						case 0:
							if ($text === '' || $text === 'add' ) {
								$notes['state'] = 0;
								
								$this->conversation->update();

								$data['text'] = "Inserisci la zona del nuovo nido da segnalare: (es. parco della musica, monteclaro, etc.)";
								$data['parse_mode'] = "markdown";
								
								$result = Request::sendMessage($data);
								break;
							}

							$notes['zona'] = $text;
							// no break
						case 1:
							if ($text === ''|| $text === $notes['zona'] ) {
								$notes['state'] = 1;
								$this->conversation->update();

								$data['text'] = 'Inserisci il pokemon che stai segnalando:';
								

								$result = Request::sendMessage($data);
								break;
							}

							$notes['pokemon'] = $text;
							
							$text         = '';

						// no break
						case 2:
							if ($message->getLocation() === null) {
								$notes['state'] = 2;
								$this->conversation->update();
								$data['text'] = 'Invia la posizione del nido (posizione GPS): ';
								$result = Request::sendMessage($data);
								break;
							}
							$notes['longitude'] = $message->getLocation()->getLongitude();
							$notes['latitude']  = $message->getLocation()->getLatitude();
						// no break
						case 3:
						
							$this->conversation->update();
						
							unset($notes['state']);
							
							$zona = $notes['zona'];
							$pokemon = $notes['pokemon'];
							$lat = $notes['latitude'];
							$lon = $notes['longitude'];
							
							
							$full_address=Raids::getFullAddress($lat,$lon);
							
							RaidsDB::addNest($zona, $pokemon,$lat,$lon,$full_address);
							
							$data['text']      = "Nido segnalato con successo!";
							$data['reply_markup'] = Keyboard::remove(['selective' => true]);
							$result = Request::sendMessage($data);
							
							$this->conversation->stop();
							break;
					}
					return $result;
				}
				elseif($subcommand == "del"){
					return Raids::delNests($chat_id);
				}
				elseif(strpos($subcommand,"del") !== false){
					$param = explode(" ",$subcommand);
					
					if(RaidsDB::getNest($param[1]) != null){
						$data['text']      = "Nido cancellato con successo.";
						RaidsDB::delNest($param[1]);
					
					}
					else
						$data['text'] = "Nido non trovato.";
					
					return Request::sendMessage($data);
				}
				elseif($subcommand == "upd"){
					
					
					$this->conversation = new Conversation($user_id, $chat_id, $this->getName());

					$notes = &$this->conversation->notes;
					!is_array($notes) && $notes = [];

					//cache data from the tracking session if any
										
						
					$notes['pokemon'] = $text;
						
					$this->conversation->update();
				
					unset($notes['state']);
					
					$nest_id = $notes['nest_id'];
					$pokemon = $notes['pokemon'];
												
					RaidsDB::updNestPokemon($nest_id, $pokemon);
					
					$data['text']      = "Nido aggiornato con successo.";
					$data['reply_markup'] = Keyboard::remove(['selective' => true]);
					Request::sendMessage($data);
					
					
					$data = Raids::showNests($chat_id);
					$data['reply_markup']	= Raids::nestsButtons($chat_id,0);
					$result = Request::sendMessage($data);
					$this->conversation->stop();

					
					return $result;
				}
			}
			else
				
				$dataToUser = Raids::showNests($chat_id);
				$dataToUser['reply_markup']	= Raids::nestsButtons($chat_id,-1);
				
				return Request::sendMessage($dataToUser);
		}
		else{
				$dataToUser = Raids::showNests($chat_id);
				$dataToUser['reply_markup']	= Raids::nestsButtons($chat_id,-1);
		
			return Request::sendMessage($dataToUser);
		}
	}
}
		