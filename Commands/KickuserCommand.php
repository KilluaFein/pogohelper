<?php

namespace Longman\TelegramBot\Commands\UserCommands;

use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Raids;
use Longman\TelegramBot\RaidsDB;
use Longman\TelegramBot\Entities\Chat;
use Spatie\Emoji\Emoji;



class KickuserCommand extends UserCommand
{
	protected $name = 'kickuser';                      // Your command's name
    protected $description = 'Effettua un controllo sull\'utente, per vedere in quali chat è presente'; // Your command description
    protected $usage = '/kickuser nomeutente-id';                    // Usage of your command
    protected $version = '0.1.0';                  // Version of your command
	protected $private_only = true;
	protected $show_in_help = false;
	
    public function execute(): ServerResponse
    {

        $message = $this->getMessage();		     // Get Message object
		$chat = $message->getChat();
        $user = $message->getFrom();
		$type    = $chat->getType();	
		$value   = trim($message->getText(true));		
		$command = $message->getCommand();
        $user_id = $user->getId(); // Get the current Chat ID
		$chat_id = $chat->getId();
		
		$chat_list = [
				['chat_id' => -1001089924240, 'title' => "Team Istinto Annunci"],
				['chat_id' => -1001063890554, 'title' => "Team Istinto Avvistamenti"],
				['chat_id' => -1001134006412, 'title' => "CrossRaid Channel"],
				['chat_id' => -1001259440680, 'title' => "CrossRaid EX Channel"],
				['chat_id' => -1001058702168, 'title' => "Team Istinto Sardegna"],
				['chat_id' => -1001078416629, 'title' => "RAID Instinct - Quartu"],
				['chat_id' => -1001094198639, 'title' => "RAID Instinct - Monserrato"],
		];
		
		$data = [];                        	        // Set up the new message data
        $data['chat_id'] = $user_id;         	    // Set Chat ID to send the message to
        $data['action'] = "typing";         	    // Set Chat ID to send the message to
		$data['parse_mode'] = 'HTML';
		
		
		if(($user_id <> 14303576 && $user_id <> 144667398) || $type != "private")
			return Request::emptyResponse();
		
		Request::sendChatAction($data);
		if($value === NULL)
			$value = $user_id;
		if(!is_numeric($value)){
			$username = str_replace("@","",$value);
			$value = RaidsDB::getUserByUsername(str_replace("@","",$value));
		}
		else
			$username = RaidsDB::getUsername($value);
		
		//$check_id = RaidsDB::getUsers();
		
		$text = Emoji::fire() . "<b>Kick utente:</b> @$username " . Emoji::fire() . PHP_EOL . PHP_EOL;
		
		$status = [
			"left" 			=> Emoji::crossMark(),
			"kicked" 		=> Emoji::crossMark(),
			"member"		=> Emoji::checkMarkButton(),
			"administrator" => Emoji::checkMarkButton(),
			"bot" 			=> Emoji::checkMarkButton(),
			"creator" 		=> Emoji::checkMarkButton(),
		];
			
		
		$leng = 0;
		foreach($chat_list as $single_chat){
			
			
			$response = Request::kickChatMember(['chat_id' => $single_chat['chat_id'],'user_id' =>$value]);
			
			if($response->ok)
				$text .= ($response?Emoji::checkMarkButton():Emoji::crossMark()) . " Rimosso dalla chat " . $single_chat['title'] . PHP_EOL ;
			else{
				$text .= "Utente Sconosciuto." . PHP_EOL;
				break;
			}
			
			
			
				// $data['text'] = $single_chat['title'].$response->ok.json_encode($response);
				// Request::sendMessage($data);

		}
		RaidsDB::setEnabled($value,-1);
			
		$text .= Emoji::checkMarkButton()." Rimossi i permessi dal bot.";
		
		
		$data['text'] = $text;
		
		$dataToChat = ['chat_id' => $user_id,
						'parse_mode'=> 'HTML',
						'text' => 'Utente @' . $username . ' <i>rimosso</i> da tutte le chat.',
					];
		Request::sendMessage($dataToChat);
		
		
		Return Request::sendMessage($data);
	}
}