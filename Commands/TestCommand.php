<?php

namespace Longman\TelegramBot\Commands\AdminCommands;

use Longman\TelegramBot\Commands\AdminCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Entities\Chat;
use Longman\TelegramBot\Entities\InlineKeyboard;
use Longman\TelegramBot\Entities\InlineKeyboardButton;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Raids;
use Longman\TelegramBot\RaidsDB;

class TestCommand extends AdminCommand
{
	protected $name = 'test';                      // Your command's name
    protected $description = 'Comando di test per vedere se le cose funzionano'; // Your command description
    protected $usage = '/test';                    // Usage of your command
    protected $version = '0.1.0';                  // Version of your command
	protected $private_only = true;
	
    public function execute(): ServerResponse
    {

        $message = $this->getMessage();		     // Get Message object
		$chat = $message->getChat();
        $user = $message->getFrom();
		$command = $message->getCommand();
        $admin_id = $message->getChat()->getId(); // Get the current Chat ID

		
		$datatoadmin= [
				'chat_id' => $admin_id,
				'parse_mode' => 'html',
				// 'reply_markup' => $inlineKeyboard,
				'text'	=>".",
			];
			
		
		return Request::sendMessage($datatoadmin);
		// Return Raids::notifyRaidEx(5);
	}
}