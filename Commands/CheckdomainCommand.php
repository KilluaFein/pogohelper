<?php

namespace Longman\TelegramBot\Commands\AdminCommands;

use Longman\TelegramBot\Commands\AdminCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Request;

class CheckdomainCommand extends AdminCommand
{
	protected $name = 'checkdomain';                      // Your command's name
    protected $description = 'Comando di test per vedere se le cose funzionano'; // Your command description
    protected $usage = '/checkdomain';                    // Usage of your command
    protected $version = '0.1.0';                  // Version of your command
	protected $private_only = true;
	
    public function execute(): ServerResponse
    {
        $message = $this->getMessage();		     // Get Message object
		// $chat = $message->getChat();
        // $user = $message->getFrom();
		// $command = $message->getCommand();
        // $chat_id = $message->getChat()->getId(); // Get the current Chat ID
		
        // $result = fsockopen("sardinianpogobot.it",80,$error_code);
        $URL = 'https://www.riversospa.it';

        if(self::isSiteAvailible($URL)){
            $result = 'The website is available.';      
        }else{
            $result = 'Woops, the site is not found.'; 
        }

		$dataToAdmin = [
				'chat_id' => 14303576, 
				'text' => $result."-", 
				'parse_mode' => 'html',
			];

		return Request::sendMessage($dataToAdmin);	
	}

    public static function isSiteAvailible($url){
        // Check, if a valid url is provided
        if(!filter_var($url, FILTER_VALIDATE_URL)){
            return false;
        }
    
        // Initialize cURL
        $curlInit = curl_init($url);
        
        // Set options
        curl_setopt($curlInit,CURLOPT_CONNECTTIMEOUT,10);
        curl_setopt($curlInit,CURLOPT_HEADER,true);
        curl_setopt($curlInit,CURLOPT_NOBODY,true);
        curl_setopt($curlInit,CURLOPT_RETURNTRANSFER,true);
    
        // Get response
        $response = curl_exec($curlInit);
        
        // Close a cURL session
        curl_close($curlInit);
    
        return $response?true:false;
    }

}