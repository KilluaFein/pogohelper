<?php

namespace Longman\TelegramBot\Commands\UserCommands;

use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Entities\Chat;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Entities\KeyboardButton;
use Longman\TelegramBot\Entities\InlineKeyboard;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Conversation;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Raids;
use Longman\TelegramBot\RaidsDB;


class PassexCommand extends UserCommand
{
	protected $name = 'passex';                      // Your command's name
    protected $description = 'Gestisci gli inviti ai raidex'; // Your command description
    protected $usage = '/passex';                    // Usage of your command
    protected $version = '0.1.0';                  // Version of your command
	protected $private_only = true;
	
    protected $need_mysql = true;

    /**
     * Conversation Object
     *
     * @var \Longman\TelegramBot\Conversation
     */
    protected $conversation;	
	
    public function execute(): ServerResponse
    {

        $message = $this->getMessage();		     // Get Message object
		$chat = $message->getChat();
        $user = $message->getFrom();
		$command = $message->getCommand();
		$text    = trim($message->getText(true));
		$chat_id = $chat->getId();
		$user_id = $user->getId();
        $admin_id = $message->getChat()->getId(); // Get the current Chat ID
		

		if(RaidsDB::isEnabledUser($user_id) != 1)
			return Request::emptyResponse();
		
		 //Preparing Response
        $data = [
            'chat_id' => $chat_id,
			'parse_mode' => 'html',
			'disable_web_page_preview' => 'true',
             
        ];
		
		$raids = RaidsDB::getActiveRaidEx();
		if(empty($raids)){
			$data['text'] = "Non c'è nessun raid Ex in corso.";
			Request::sendMessage($data);
		}
		else
			foreach($raids as $raid){
				$data['text'] = Raids::printActiveRaid($raid['id'], $user_id);
				$data['reply_markup'] = Raids::printActiveRaidButtons($raid['id'], $user_id);				
				Request::sendMessage($data);
			}
			
		return Request::emptyResponse();
	}
}