<?php

namespace Longman\TelegramBot\Commands\AdminCommands;

use Longman\TelegramBot\Commands\AdminCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Entities\Chat;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Entities\KeyboardButton;
use Longman\TelegramBot\Entities\InlineKeyboard;
use Longman\TelegramBot\Conversation;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Raids;
use Longman\TelegramBot\RaidsDB;


class RaidexCommand extends AdminCommand
{
	protected $name = 'raidex';                      // Your command's name
    protected $description = 'Aggiungi un nuovo raidex e gestisci gli inviti'; // Your command description
    protected $usage = '/raidex';                    // Usage of your command
    protected $version = '0.1.0';                  // Version of your command
	protected $private_only = true;
	
    protected $need_mysql = true;

    /**
     * Conversation Object
     *
     * @var \Longman\TelegramBot\Conversation
     */
    protected $conversation;	
	
    public function execute(): ServerResponse
    {

        $message = $this->getMessage();		     // Get Message object
		$chat = $message->getChat();
        $user = $message->getFrom();
		$command = $message->getCommand();
		$text    = trim($message->getText(true));
		$chat_id = $chat->getId();
		$user_id = $user->getId();
        $admin_id = $message->getChat()->getId(); // Get the current Chat ID
		$publishRaid = [];

		
		 //Preparing Response
        $data = [
            'chat_id' => $chat_id,
			'parse_mode' => 'html',
             
        ];
		
		 //Conversation start
        $this->conversation = new Conversation($user_id, $chat_id, $this->getName());

        $notes = &$this->conversation->notes;
        !is_array($notes) && $notes = [];

        //cache data from the tracking session if any
        $state = 0;
        if (isset($notes['state'])) {
            $state = $notes['state'];
        }

        $result = Request::emptyResponse();
			
		//State machine
        //Entrypoint of the machine state if given by the track
        //Every time a step is achieved the track is updated
        switch ($state) {
		case 0:
			if ($text === '' || !is_numeric($text)) {
				$notes['state'] = 0;
				$this->conversation->update();

				$data['text'] = "Seleziona la palestra dove si svolgerà il raid EX";
				$data['text'] = " inserendo l'ID della palestra" . PHP_EOL . PHP_EOL;
				
				$palestre = RaidsDB::getGymEx(); 
				
				foreach($palestre as $palestra)
					$data['text'] .= $palestra['gym_id'] . " " . ucfirst($palestra['gym_name']) . PHP_EOL;
				
				$result = Request::sendMessage($data);
				break;
			}

			$notes['gym_id'] = $text;
			
			$text          = '';

            // no break
		case 1:
			if ($text === '' || strlen($text) > 17){// || !preg_match('/^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]/$',$text)) {
				$notes['state'] = 1;
				$this->conversation->update();

				$data['text'] = "Che giorno e a che ora schiude? (nel formato GG-MM-AAAA HH:MM \nUsa il comando /cancel per cancellare questo invio";
				
				if ($text !== '') {
					$data['text'] = 'Hai sbagliato qualcosa, riprova';
				}

				$result = Request::sendMessage($data);
				break;
			}
			$text = str_replace(".",":",$text);
			$notes['datetime'] = $text;
			
			$text             = '';

            // no break
		case 2:
			if ($text === '' || $text === "Niente link"){
				$notes['state'] = 2;
				$this->conversation->update();

				$data['text'] = "Se hai già creato la chat del gruppo per questo Raid, inviami il link, altrimenti premi Niente Link.";
				
				if ($text !== '') {
					$data['text'] = 'Errore inserimento link. Riprova.';
				}
				
				$data['reply_markup'] = (new Keyboard(['Niente Link']))
					->setResizeKeyboard(true)
					->setOneTimeKeyboard(true)
					->setSelective(true);
				
				$result = Request::sendMessage($data);
				break;
			}
			
			$notes['chat_link'] = $text;
			
			$text             = '';

            // no break
		case 3:
			$this->conversation->update();
			
			
						//inserisco la segnalazione nella tabella Raids
			$text = "Dati inseriti: " . PHP_EOL;
			
			$gym = RaidsDB::getGymById($notes['gym_id']);
			
			//$publishRaid['id'] = id da creare;
			$publishRaid['gym_id'] = $notes['gym_id'];
			$publishRaid['gym_name'] = $gym['gym_name'];
			$publishRaid['lat'] = $gym['lat'];
			$publishRaid['lon'] = $gym['lon'];
			$oraestesa = date_create_from_format("d-m-Y H:i:s",$notes['datetime'].':00');
			$publishRaid['datetime'] = $oraestesa->format("Y-m-d H:i:s");
			$publishRaid['chat_link'] = ($notes['chat_link']=="Niente Link"?NULL:$notes['chat_link']);
			
			$text .= "ID palestra: ".$notes['gym_id'] . PHP_EOL;
			$text .= "Palestra: ".$gym['gym_name'] . PHP_EOL;
			$text .= "Posizione: ".$gym['lat'] .",". $gym['lon'] . PHP_EOL;
			$text .= "Orario: ".$publishRaid['datetime'] . PHP_EOL;
			$text .= "LinkChat: ".$notes['chat_link'] . PHP_EOL;
			
			$data['text'] = $text;
			$publishRaid['text'] = $text;
			$data['reply_markup'] = Keyboard::remove(['selective' => true]);
			
			$result = Request::sendMessage($data); //reply to user
			
			$lastId = RaidsDB::insertRaidEx($publishRaid);
			
			// $dataToAdmin = [
					// 'chat_id' => 14303576,
					// 'text'    => "Lastid: " . $lastId,
				// ];				
			// Request::sendMessage($dataToAdmin);
			
			Raids::notifyRaidEx($lastId);
			
			unset($notes['state']);
			$this->conversation->stop();
			break;
        }

        return $result;
	}
}