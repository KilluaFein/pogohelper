<?php
/**
 * This file is part of the TelegramBot package.
 *
 * (c) Avtandil Kikabidze aka LONGMAN <akalongman@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Longman\TelegramBot\Commands\UserCommands;

use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Entities\InlineKeyboard;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Raids;
use Longman\TelegramBot\RaidsDB;

/**
 * User "/survey" command
 */
class NotificheCommand extends UserCommand
{
 /**
     * @var string
     */
    protected $name = 'notifiche';

    /**
     * @var string
     */
    protected $description = 'Permette di impostare le notifiche per i tre R4 principali';

    /**
     * @var string
     */
    protected $usage = '/notiche';

    /**
     * @var string
     */
    protected $version = '0.1.0';
	
    /**
     * @var bool
     */
    protected $show_in_help = true;
	
	protected $private_only = true;

    /**
     * Command execute method
     *
     * @return \Longman\TelegramBot\Entities\ServerResponse
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function execute(): ServerResponse
    {
        $message = $this->getMessage();

        $chat    = $message->getChat();
        $user    = $message->getFrom();
		$type    = $chat->getType();
        $text    = trim($message->getText(true));
        $chat_id = $chat->getId();
        $user_id = $user->getId();
		$username = $user->getUsername();
		
		
		if(RaidsDB::isEnabledUser($chat_id) != 1 || $type != "private")
			return Request::emptyResponse();
		
        //Preparing Response
        $data = [
            'chat_id' => $chat_id,
            'parse_mode' => 'Markdown',
			'text' => Raids::notification($user_id),
			'reply_markup' => Raids::notificationButton("R5G2"),
        ];
		
		
        return Request::sendMessage($data);
    }
}		
		
		