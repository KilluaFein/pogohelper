
<?php

/**
 * README
 * This configuration file is intended to run the bot with the webhook method.
 * Uncommented parameters must be filled
 *
 * Please note that if you open this file with your browser you'll get the "Input is empty!" Exception.
 * This is a normal behaviour because this address has to be reached only by the Telegram servers.
 */

// Load composer
require_once __DIR__ . '/vendor/autoload.php';

// Load custom classes
require_once __DIR__ . '/Classes/Raids.php';
require_once __DIR__ . '/Classes/RaidsDB.php';

// Add you bot's API key and name
$bot_api_key = '397967790:AAEwGeNK6ux7RKaBVO8tW2nGMkkPLPbR_WI';
$bot_username = 'SardinianPoGoBot';

// Define all IDs of admin users in this array (leave as empty array if not used)
$admin_users = [
    14303576,
];



// Enter your MySQL database credentials
$mysql_credentials = [
    'host' => 'localhost',
    'user' => 'pogohelper',
    'password' => 'Stakanovista12',
    'database' => 'pogohelper',
];

try {
    // Create Telegram API object
    $telegram = new Longman\TelegramBot\Telegram($bot_api_key, $bot_username);

    // Enable admin users
    $telegram->enableAdmins($admin_users);

    // Enable MySQL
    $telegram->enableMySql($mysql_credentials);

    // Enable Custom Table
    Longman\TelegramBot\RaidsDB::initializeRaidsDb();

    // Logging (Error, Debug and Raw Updates)
    Longman\TelegramBot\TelegramLog::initErrorLog(__DIR__ . "/{$bot_username}_error.log");
    // Longman\TelegramBot\TelegramLog::initDebugLog(__DIR__ . "/{$bot_username}_debug.log");
    // Longman\TelegramBot\TelegramLog::initUpdateLog(__DIR__ . "/{$bot_username}_update.log");

    // If you are using a custom Monolog instance for logging, use this instead of the above
    //Longman\TelegramBot\TelegramLog::initialize($your_external_monolog_instance);

    // Set custom Upload and Download paths
    $telegram->setDownloadPath('/var/www/html/pogofiles/download');
    $telegram->setUploadPath('/var/www/html/pogofiles/upload');

    // Here you can set some command specific parameters
    // e.g. Google geocode/timezone api key for /date command
    //$telegram->setCommandConfig('date', ['google_api_key' => 'your_google_api_key_here']);

    // Botan.io integration
    $telegram->enableBotan('51c973ec-60fe-4c78-926d-e2a74ba4f3e6');

    // Longman\TelegramBot\Request::sendMessage(['chat_id'=> 14303576, 'text' => "test a me"]);

    // Requests Limiter (tries to prevent reaching Telegram API limits)
    $telegram->enableLimiter();

    //BigBoss
    $boss_id = 14303576;

    // Handle telegram webhook request

} catch (Longman\TelegramBot\Exception\TelegramException $e) {
    // Silence is golden!
    echo $e;
    // Log telegram errors
    Longman\TelegramBot\TelegramLog::error($e);
} catch (Longman\TelegramBot\Exception\TelegramLogException $e) {
    // Silence is golden!
    // Uncomment this to catch log initialisation errors
    echo $e;
}

use Longman\TelegramBot\Raids;
use Longman\TelegramBot\RaidsDB;
use Longman\TelegramBot\Request;

$channel_to_PK = -1001063890554; //14303576 ;// Team Istinto Avvistamenti
$channel_to_GE = -1001259440680; //14303576 ;// CrossRaid EX Channel
$channel_to_GM = -1001134006412; //14303576 ;// CrossRaid Channel // 96762970; Davide //
$channel_from_PK = -1001077335110; // Team Istinto Avvistamenti CITM
$channel_from_GM = -1001259567022; // Team Istinto Raid CITM

$channel_text = json_decode(file_get_contents('php://input'), true);

RaidsDB::insertGym($channel_text);
if (!RaidsDB::getGymActive($channel_text['internal_id'])) {
    return Request::emptyResponse();
}

if (isset($channel_text['pokemon_id']) and $channel_text['pokemon_id'] != 0 ){ //aggiornamento informazioni
	
	$raid = RaidsDB::getRaidByGym($channel_text['internal_id']);
	
	$pokemon_name = RaidsDB::getPokemonName($channel_text['pokemon_id']);
	
	// $pattern = "#https[:/a-z.]+[1-5].jpg#";
	$pattern = "#http[:/a-z.?=&0-9%A-Z-]+dmainas[:/a-z.?=&0-9%A-Z-]+#";
	
	$raid['fixed_text'] = preg_replace($pattern,$channel_text['imageURL'],$raid['fixed_text']);
	
	$raid['fixed_text'] .= "Pokemon Segnalato: " . strtoupper($pokemon_name) . " da @SardinianPoGoHelper alle " . Date('H:i:s') . PHP_EOL;
	
	$raid['chat_id'] = $raid['channel_id'];
	
	$raid['pokemon'] = $pokemon_name;
	
	RaidsDB::insertRaid($raid);
	
	//Request::editMessageText($raid);
	
	return Request::emptyResponse();
}


$dataToAdmin = [
    'chat_id' => 14303576,
    'parse_mode' => 'markdown',
];

$photo_id = [1 => "AgADBAADeqwxGzDPgFPEd5KHFoTRfW3XihoABPVKswGu3a8qgWoBAAEC",
    2 => "AgADBAADeqwxGzDPgFPEd5KHFoTRfW3XihoABPVKswGu3a8qgWoBAAEC",
    3 => "AgADBAADeawxGzDPgFMRHSaeZQasZgLEJxoABJ3obj-Ueqr11U0DAAEC",
    4 => "AgADBAADeawxGzDPgFMRHSaeZQasZgLEJxoABJ3obj-Ueqr11U0DAAEC",
    5 => "AgADBAADeKwxGzDPgFOVLSUj2NPfGybZJxoABLjDlAN6Sr7dtVADAAEC",
];

$team = ["⚪️", "🔵", "🔴", "💛"];
$raidlevel = ['0️⃣', '1️⃣', '2️⃣', '3️⃣', '4️⃣', '5️⃣', '6️⃣', '7️⃣', '8️⃣', '9️⃣', '🔟'];

$data_to_channel = [
    'parse_mode' => 'markdown',
    'user_id' => 310427086,
    'chat_id' => $channel_to_GM, //da spostare dopo il controllo
    'disable_notification' => false,
    'photo_id' => $photo_id[$channel_text['raid_level']],
    'message_id' => 0,
    'comando' => "segnalaraid",
    'gym_id' => $channel_text['internal_id'],
    'raidlevel' => $channel_text['raid_level'],
    'zona' => $channel_text['name'],
    'lat' => $channel_text['lat'],
    'lon' => $channel_text['lon'],
    'photo' => $photo_id[$channel_text['raid_level']],
    'oraschiusa' => gmdate("Y-m-d H:i:s", $channel_text['start_ts'] + 3600),
    'team' => $channel_text['team'],
];

$text = "🔥*NEW RAID SEGNALESCION!!*🔥" . PHP_EOL . PHP_EOL;
$text .= "Livello " . $raidlevel[$data_to_channel['raidlevel']] . " [‍](". $channel_text['imageURL'] .")" . PHP_EOL;
$text .= "🕰: " . gmdate("H:i", $channel_text['start_ts'] + 3600) . PHP_EOL;
$text .= "⛰: " . "[" . $data_to_channel['zona'] . "](http://maps.google.com/maps?q=" . $data_to_channel['lat'] . "," . $data_to_channel['lon'] . ") " . $team[$data_to_channel['team']] . PHP_EOL;
$text .= "🙎‍♂️: @SardinianPoGoHelper" . PHP_EOL;

$data_to_channel['fixed_text'] = $text;
$data_to_channel['text'] = $data_to_channel['fixed_text'];

$oraestesa = date_create_from_format("Y-m-d H:i:s", $data_to_channel['oraschiusa']);
$orario = $oraestesa->format('H:i');
$data_to_channel['t1'] = $oraestesa->format("Y-m-d H:i:s");
$data_to_channel['text'] .= "Partecipano alle $orario: " . PHP_EOL;
date_add($oraestesa, date_interval_create_from_date_string('15 minutes'));
$orario = $oraestesa->format('H:i');
$data_to_channel['t2'] = $oraestesa->format("Y-m-d H:i:s");
$data_to_channel['text'] .= "Partecipano alle $orario: " . PHP_EOL;
date_add($oraestesa, date_interval_create_from_date_string('15 minutes'));
$orario = $oraestesa->format('H:i');
$data_to_channel['t3'] = $oraestesa->format("Y-m-d H:i:s");
$data_to_channel['text'] .= "Partecipano alle $orario: " . PHP_EOL;
if($data_to_channel['raidlevel'] >4)
	$data_to_channel['reply_markup'] = Raids::raidButtonOrario($data_to_channel['raidlevel'], $data_to_channel['oraschiusa']);
else
	$data_to_channel['reply_markup'] = Raids::raidButtonOrarioTris($data_to_channel['oraschiusa']);

$post_to_channel = 1;

$gymsEx = RaidsDB::getGymEx();
foreach ($gymsEx as $gymEx) {
    if ($data_to_channel['gym_id'] == $gymEx['gym_id'] and $gymEx['gym_ex_active'] == 1) {
        $data_to_channel['chat_id'] = $channel_to_GE;
    } elseif ($data_to_channel['gym_id'] == $gymEx['gym_id'] and $gymEx['gym_ex_active'] == 0) {
        if ($data_to_channel['raidlevel'] < 5) {
            $data_to_channel['chat_id'] = $channel_from_GM;
        }
	}
}
if($data_to_channel['chat_id'] == $channel_to_GM and $data_to_channel['raidlevel'] < 5 )
	$data_to_channel['chat_id'] = $channel_from_GM;

if ($post_to_channel) {
    $result = Request::sendMessage($data_to_channel);

    $data_to_channel['message_id'] = $result->result->message_id;

    RaidsDB::insertRaid($data_to_channel);
    RaidsDB::addSegnalazioni(310427086);
}

?>