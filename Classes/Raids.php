<?php
/**
 * This file is part of the TelegramBot package.
 *
 * (c) Avtandil Kikabidze aka LONGMAN <akalongman@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Longman\TelegramBot;

use Longman\TelegramBot\Request;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Entities\Update;
use Longman\TelegramBot\Exception\TelegramException;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Entities\KeyboardButton;
use Longman\TelegramBot\Entities\InlineKeyboard;
use Longman\TelegramBot\Entities\InlineKeyboardButton;
use Longman\TelegramBot\RaidsDB;
use Spatie\Emoji\Emoji;

/**
 * Class Raids
 */
class Raids
{
	public static function raidButtonOrario($livello,$orario): InlineKeyboard
	{
		$estensione = date_create_from_format("Y-m-d H:i:s",$orario);
		
		$counter = 0;
		
		//$inlineKeyboard = [];
		$inlineKeyboard = new InlineKeyboard([]);
		$keyboard_buttons = [];
		//date_add($estensione, date_interval_create_from_date_string('30 minutes'));
		do{
			
			$orario = $estensione->format('H:i');
			$keyboard_buttons[] = new InlineKeyboardButton([
				'callback_data' => "segnalaraid,Partecipa,$orario",
				'text'          => $orario,
			]); 
			date_add($estensione, date_interval_create_from_date_string('15 minutes'));
			$counter ++;
		}	while($counter <3);	
		
		call_user_func_array([$inlineKeyboard, 'addRow'], $keyboard_buttons);
		$keyboard_buttons = [];
		
		$keyboard_buttons[] = new InlineKeyboardButton(['text' => 'Leva Partecipazione', 'callback_data' => "segnalaraid,NonPartecipa"]);
		
		call_user_func_array([$inlineKeyboard, 'addRow'], $keyboard_buttons);
		$keyboard_buttons = [];
		return $inlineKeyboard;
	}	
	public static function raidButtonOrarioTris($oraSchiusa): InlineKeyboard
	{
		$estensione = date_create_from_format("Y-m-d H:i:s",$oraSchiusa);
		
		$counter = 0;
		
		
		$inlineKeyboard = new InlineKeyboard([]);
		$keyboard_buttons = [];
		
		do{
			
			$orario = $estensione->format('H:i');
			$keyboard_buttons[] = new InlineKeyboardButton([
				'callback_data' => "segnalaraid,Partecipa,$orario," . ++$counter,
				'text'          => $orario,
			]); 
			date_add($estensione, date_interval_create_from_date_string('15 minutes'));
		}	while($counter <3);	
		
		call_user_func_array([$inlineKeyboard, 'addRow'], $keyboard_buttons);
		$keyboard_buttons = [];
		
		$keyboard_buttons[] = new InlineKeyboardButton(['text' => 'Leva Partecipazione', 'callback_data' => "segnalaraid,NonPartecipa"]);
		
		call_user_func_array([$inlineKeyboard, 'addRow'], $keyboard_buttons);
		
		return $inlineKeyboard;
	}
	
	public static function raidButtonOrarioBis($raid): InlineKeyboard
	{
		
		$counter = 1;
		
		$inlineKeyboard = new InlineKeyboard([]);
		$keyboard_buttons = [];
		
		while($counter <=3){
			if(array_key_exists("t$counter",$raid) && $raid["t$counter"] !== null){
						
				//$orario = date_create_from_format("Y-m-d H:i:s",$raid["t$counter"]);
				$orario = date("H:i",strtotime($raid["t$counter"]));
				
				$keyboard_buttons[] = new InlineKeyboardButton([
					'callback_data' => $raid["comando"].",Partecipa,$orario",
					'text'          => $orario,
				]); 
			}
			
			$counter ++;
		}		

		call_user_func_array([$inlineKeyboard, 'addRow'], $keyboard_buttons);
		$keyboard_buttons = [];
		
		$keyboard_buttons[] = new InlineKeyboardButton(['text' => 'Leva Partecipazione', 'callback_data' => $raid["comando"].",NonPartecipa"]);
		
		call_user_func_array([$inlineKeyboard, 'addRow'], $keyboard_buttons);
		$keyboard_buttons = [];

		return $inlineKeyboard;
	}
	
	public static function enableDisableButton($user_id,$refeer): InlineKeyboard
	{
		return new InlineKeyboard([
					['text' => 'Abilita', 'callback_data' => "start,$user_id,1,$refeer"],
					['text' => 'Disabilta', 'callback_data' => "start,$user_id,-1,$refeer"],	
				]);
	}
	
	public static function printChannelsButton($channels): InlineKeyboard
	{
		
		$inlineKeyboard = new InlineKeyboard([]);
		$keyboard_buttons = [];
		
		foreach($channels as $channel){
			
			$linkToChannel = Request::exportChatInviteLink(['chat_id' =>$channel['channel_id']]);
			$linkToChannel = $linkToChannel -> getResult();
			
			$keyboard_buttons[] = new InlineKeyboardButton([
										'url' 			=> $linkToChannel,
										'text'			=> $channel['title'],
				]); 
			
			
			call_user_func_array([$inlineKeyboard, 'addRow'], $keyboard_buttons);
			$keyboard_buttons = [];
			
		}	
		return $inlineKeyboard;
	}
	
	function mb_substr_replace($string, $replacement, $start, $length=NULL): string
	{
		if (is_array($string)) {
			$num = count($string);
			// $replacement
			$replacement = is_array($replacement) ? array_slice($replacement, 0, $num) : array_pad(array($replacement), $num, $replacement);
			// $start
			if (is_array($start)) {
				$start = array_slice($start, 0, $num);
				foreach ($start as $key => $value)
					$start[$key] = is_int($value) ? $value : 0;
			}
			else {
				$start = array_pad(array($start), $num, $start);
			}
			// $length
			if (!isset($length)) {
				$length = array_fill(0, $num, 0);
			}
			elseif (is_array($length)) {
				$length = array_slice($length, 0, $num);
				foreach ($length as $key => $value)
					$length[$key] = isset($value) ? (is_int($value) ? $value : $num) : 0;
			}
			else {
				$length = array_pad(array($length), $num, $length);
			}
			// Recursive call
			return array_map(__FUNCTION__, $string, $replacement, $start, $length);
		}
		preg_match_all('/./us', (string)$string, $smatches);
		preg_match_all('/./us', (string)$replacement, $rmatches);
		if ($length === NULL) $length = mb_strlen($string);
		array_splice($smatches[0], $start, $length, $rmatches[0]);
		return join($smatches[0]);
	}
	
	public function parseEntitiesString($text, $entities): string
    {
        $global_incr = 0;
		
        foreach ($entities as $entity) {
            if ($entity->getType() == 'italic') {
                $start = $global_incr + $entity->getOffset();
                $end = 1 + $start + $entity->getLength();

                $text = Self::mb_substr_replace($text, '_', $start, 0);
                $text = Self::mb_substr_replace($text, '_', $end, 0);

                $global_incr = $global_incr + 2;
            } elseif ($entity->getType() == 'bold') {
                $start = mb_strlen(mb_substr($text,$global_incr,$global_incr + $entity->getOffset(),'UTF-16'),'UTF-8');
				$first = false;
                $end = 1 + $start + $entity->getLength();
			
                $text = Self::mb_substr_replace($text, '*', $start, 0);
                $text = Self::mb_substr_replace($text, '*', $end, 0);

                $global_incr = $global_incr + 2;
            } elseif ($entity->getType() == 'code') {
                $start = $global_incr + $entity->getOffset();
                $end = 1 + $start + $entity->getLength();

                $text = Self::mb_substr_replace($text, '`', $start, 0);
                $text = Self::mb_substr_replace($text, '`', $end, 0);

                $global_incr = $global_incr + 2;
            } elseif ($entity->getType() == 'pre') {
                $start = $global_incr + $entity->getOffset();
                $end = 3 + $start + $entity->getLength();

                $text = Self::mb_substr_replace($text, '```', $start, 0);
                $text = Self::mb_substr_replace($text, '```', $end, 0);

                $global_incr = $global_incr + 6;
            } elseif ($entity->getType() == 'text_link') {
                $start = $entity->getOffset() ;
                $end = 1 + $start + $entity->getLength();
                $url = '(' . $entity->getUrl() . ')';
				
                $text = Self::mb_substr_replace($text, '[', $start, 0);
                $text = Self::mb_substr_replace($text, ']' . $url, $end, 0);

                $global_incr = $global_incr + 2 + mb_strlen($url);
            } elseif ($entity->getType() == 'code') {
                $start = $global_incr + $entity->getOffset();

                $text = mb_substr($text, 0, $start);
            }
        }

        return $text;
    }
	
		public static function logChannel($text): ServerResponse
		{
			
			$logChannelId =  -1001122892871;
			
			$text = str_ireplace('@KilluaFein','KilluaFein',$text);
			
			$dataToLogChannel = [
				'chat_id' => $logChannelId,
				'text'    => $text,
				'parse_mode'    => "html",
			];	
			
			return Request::sendMessage($dataToLogChannel);
		}
		
	public static function notificationButton($tier): InlineKeyboard
	{
		$notificationEmoji = [
					'tyranitar' 	=> Emoji::dragonFace(),
					'snorlax' 		=> Emoji::panda(),
					'lapras' 		=> Emoji::spoutingWhale(),
					'absol' 		=> Emoji::wolf(),
					'zapdos' 		=> Emoji::highVoltage(),
					'articuno' 		=> Emoji::droplet(),
					'moltres' 		=> Emoji::fire(),
					'lugia' 		=> Emoji::dove(),
					'hooh' 			=> Emoji::eagle(),
					'mewtwo' 		=> Emoji::smilingFaceWithHorns(),
					'raikou' 		=> Emoji::largeOrangeDiamond(),
					'entei' 		=> Emoji::diamondSuit(),
					'suicune'		=> Emoji::largeBlueDiamond(),
					'mew' 			=> Emoji::hamster(),
					'celebi' 		=> Emoji::alien(),
					'groudon' 		=> Emoji::lizard(),
					'kyogre' 		=> Emoji::shark(),
		];
		
		
		if($tier == "R4")
			return new InlineKeyboard([
					['text' => $notificationEmoji['tyranitar'], 'callback_data' => "notifiche,-1,tyranitar,R4"],
					['text' => Emoji::checkMarkButton().' On', 'callback_data' => "notifiche,1,tyranitar,R4"],
					['text' => Emoji::crossMark().     ' Off', 'callback_data' => "notifiche,0,tyranitar,R4"],
				], [
					['text' => $notificationEmoji['snorlax'], 'callback_data' => "notifiche,-1,snorlax,R4"],
					['text' => Emoji::checkMarkButton().' On', 'callback_data' => "notifiche,1,snorlax,R4"],
					['text' => Emoji::crossMark().     ' Off', 'callback_data' => "notifiche,0,snorlax,R4"],
				], [
					['text' => $notificationEmoji['lapras'], 'callback_data' => "notifiche,-1,lapras,R4"],
					['text' => Emoji::checkMarkButton().' On', 'callback_data' => "notifiche,1,lapras,R4"],
					['text' => Emoji::crossMark().     ' Off', 'callback_data' => "notifiche,0,lapras,R4"],
				], [
					['text' => $notificationEmoji['absol'], 'callback_data' => "notifiche,-1,absol,R4"],
					['text' => Emoji::checkMarkButton().' On', 'callback_data' => "notifiche,1,absol,R4"],
					['text' => Emoji::crossMark().     ' Off', 'callback_data' => "notifiche,0,absol,R4"],
				], [
					['text' => 'R4', 'callback_data' => "notifiche,-2,tier,R4"],
					['text' => 'R5 G1', 'callback_data' => "notifiche,-2,tier,R5G1"],
					['text' => 'R5 G2', 'callback_data' => "notifiche,-2,tier,R5G2"],
					['text' => 'R5 G3', 'callback_data' => "notifiche,-2,tier,R5G3"],				
				]);
		elseif($tier == "R5G1")
			return new InlineKeyboard([
					['text' => $notificationEmoji['zapdos'], 'callback_data' => "notifiche,-1,zapdos,R5G1"],
					['text' => Emoji::checkMarkButton().' On', 'callback_data' => "notifiche,1,zapdos,R5G1"],
					['text' => Emoji::crossMark().     ' Off', 'callback_data' => "notifiche,0,zapdos,R5G1"],
				], [
					['text' => $notificationEmoji['articuno'], 'callback_data' => "notifiche,-1,articuno,R5G1"],
					['text' => Emoji::checkMarkButton().' On', 'callback_data' => "notifiche,1,articuno,R5G1"],
					['text' => Emoji::crossMark().     ' Off', 'callback_data' => "notifiche,0,articuno,R5G1"],
				], [
					['text' => $notificationEmoji['moltres'], 'callback_data' => "notifiche,-1,moltres,R5G1"],
					['text' => Emoji::checkMarkButton().' On', 'callback_data' => "notifiche,1,moltres,R5G1"],
					['text' => Emoji::crossMark().     ' Off', 'callback_data' => "notifiche,0,moltres,R5G1"],
				], [
					['text' => $notificationEmoji['mew'], 'callback_data' => "notifiche,-1,mew,R5G1"],
					['text' => Emoji::checkMarkButton().' On', 'callback_data' => "notifiche,1,mew,R5G1"],
					['text' => Emoji::crossMark().     ' Off', 'callback_data' => "notifiche,0,mew,R5G1"],
				], [
					['text' => $notificationEmoji['mewtwo'], 'callback_data' => "notifiche,-1,mewtwo,R5G1"],
					['text' => Emoji::checkMarkButton().' On', 'callback_data' => "notifiche,1,mewtwo,R5G1"],
					['text' => Emoji::crossMark().     ' Off', 'callback_data' => "notifiche,0,mewtwo,R5G1"],
				], [
					['text' => 'R4', 'callback_data' => "notifiche,-2,tier,R4"],
					['text' => 'R5 G1', 'callback_data' => "notifiche,-2,tier,R5G1"],
					['text' => 'R5 G2', 'callback_data' => "notifiche,-2,tier,R5G2"],
					['text' => 'R5 G3', 'callback_data' => "notifiche,-2,tier,R5G3"],				
				]);
		elseif($tier == "R5G2")
			return new InlineKeyboard([
					['text' => $notificationEmoji['lugia'], 'callback_data' => "notifiche,-1,lugia,R5G2"],
					['text' => Emoji::checkMarkButton().' On', 'callback_data' => "notifiche,1,lugia,R5G2"],
					['text' => Emoji::crossMark().     ' Off', 'callback_data' => "notifiche,0,lugia,R5G2"],
				], [
					['text' => $notificationEmoji['hooh'], 'callback_data' => "notifiche,-1,hooh,R5G2"],
					['text' => Emoji::checkMarkButton().' On', 'callback_data' => "notifiche,1,hooh,R5G2"],
					['text' => Emoji::crossMark().     ' Off', 'callback_data' => "notifiche,0,hooh,R5G2"],
				], [
					['text' => $notificationEmoji['raikou'], 'callback_data' => "notifiche,-1,raikou,R5G2"],
					['text' => Emoji::checkMarkButton().' On', 'callback_data' => "notifiche,1,raikou,R5G2"],
					['text' => Emoji::crossMark().     ' Off', 'callback_data' => "notifiche,0,raikou,R5G2"],
				], [
					['text' => $notificationEmoji['entei'], 'callback_data' => "notifiche,-1,entei,R5G2"],
					['text' => Emoji::checkMarkButton().' On', 'callback_data' => "notifiche,1,entei,R5G2"],
					['text' => Emoji::crossMark().     ' Off', 'callback_data' => "notifiche,0,entei,R5G2"],
				], [
					['text' => $notificationEmoji['suicune'], 'callback_data' => "notifiche,-1,suicune,R5G2"],
					['text' => Emoji::checkMarkButton().' On', 'callback_data' => "notifiche,1,suicune,R5G2"],
					['text' => Emoji::crossMark().     ' Off', 'callback_data' => "notifiche,0,suicune,R5G2"],
				], [
					['text' => $notificationEmoji['celebi'], 'callback_data' => "notifiche,-1,celebi,R5G2"],
					['text' => Emoji::checkMarkButton().' On', 'callback_data' => "notifiche,1,celebi,R5G2"],
					['text' => Emoji::crossMark().     ' Off', 'callback_data' => "notifiche,0,celebi,R5G2"],
				], [
					['text' => 'R4', 'callback_data' => "notifiche,-2,tier,R4"],
					['text' => 'R5 G1', 'callback_data' => "notifiche,-2,tier,R5G1"],
					['text' => 'R5 G2', 'callback_data' => "notifiche,-2,tier,R5G2"],
					['text' => 'R5 G3', 'callback_data' => "notifiche,-2,tier,R5G3"],				
				
				]);
		elseif($tier == "R5G3")
			return new InlineKeyboard([

					['text' => $notificationEmoji['groudon'], 'callback_data' => "notifiche,-1,groudon,R5G3"],
					['text' => Emoji::checkMarkButton().' On', 'callback_data' => "notifiche,1,groudon,R5G3"],
					['text' => Emoji::crossMark().     ' Off', 'callback_data' => "notifiche,0,groudon,R5G3"],
				], [					
					['text' => $notificationEmoji['kyogre'], 'callback_data' => "notifiche,-1,kyogre,R5G3"],
					['text' => Emoji::checkMarkButton().' On', 'callback_data' => "notifiche,1,kyogre,R5G3"],
					['text' => Emoji::crossMark().     ' Off', 'callback_data' => "notifiche,0,kyogre,R5G3"],
				], [
					['text' => 'R4', 'callback_data' => "notifiche,-2,tier,R4"],
					['text' => 'R5 G1', 'callback_data' => "notifiche,-2,tier,R5G1"],
					['text' => 'R5 G2', 'callback_data' => "notifiche,-2,tier,R5G2"],
					['text' => 'R5 G3', 'callback_data' => "notifiche,-2,tier,R5G3"],				
				]);
	}
	
	public static function notification($user_id)
	{
		//controllo lo stato attuale delle notifiche
		$status = [Emoji::crossMark(),Emoji::checkMarkButton()];
		
		$notificationEmoji = [
					'tyranitar' => Emoji::dragonFace(),
					'snorlax' => Emoji::pandaFace(),
					'lapras' => Emoji::spoutingWhale(),
					'absol' => Emoji::wolf(),
					'zapdos' => Emoji::highVoltage(),
					'articuno' => Emoji::droplet(),
					'moltres' => Emoji::fire(),
					'lugia' => Emoji::dove(),
					'hooh' => Emoji::eagle(),
					'mewtwo' => Emoji::smilingFaceWithHorns(),
					'raikou' => Emoji::largeOrangeDiamond(),
					'entei' => Emoji::diamondSuit(),
					'suicune' => Emoji::largeBlueDiamond(),
					'mew' => Emoji::hamster(),
					'celebi' => Emoji::alien(),
					'groudon' => Emoji::lizard(),
					'kyogre' => Emoji::shark(),
		];
		
		$notifiche = RaidsDB::getNotificationStatus($user_id);
		
		$text = "*Gestisci Notifiche*" . PHP_EOL . PHP_EOL;
		
		foreach($notifiche as $campo => $notifica)
			$text.= $notificationEmoji[$campo] ." " . ucfirst($campo) . ": " . $status[$notifica] . PHP_EOL;  
		
		return $text;
	}
	
	public static function sendNotification($pokemon,$username,$dove)
	{
		
		$userList = RaidsDB::getNotificationToUser($pokemon);
		
		$dataToUser = [
			'parse_mode' => 'Markdown',
			'text' => "@$username ha segnalato che è uscito *$pokemon* da un'apparizione recente nella palestra identificata da _".$dove."_!" .PHP_EOL .
						"(premi /notifiche se vuoi modificare le impostazioni di notifica)",
		];
	
		foreach($userList as $user_id){
			
			$dataToUser['chat_id'] = $user_id['id'];
			
			Request::sendMessage($dataToUser);
		}		
	}
	
	public static function showNests($chat_id,$view = false): array
	{
		$pokemote = [
				Emoji::dogFace(),Emoji::catFace(),Emoji::mouseFace(),Emoji::hamster(),
				Emoji::rabbitFace(),Emoji::bear(),Emoji::panda(),Emoji::koala(),
				Emoji::tigerFace(),Emoji::lionFace(),Emoji::cowFace(),Emoji::pigFace(),
				Emoji::pigNose(),Emoji::frogFace(),Emoji::octopus(),Emoji::monkeyFace(),
				Emoji::monkey(),Emoji::chicken(),Emoji::penguin(),Emoji::bird(),
				Emoji::babyChick(),Emoji::hatchingChick(),Emoji::wolf(),Emoji::boar(),
				Emoji::horseFace(),Emoji::unicornFace(),Emoji::honeybee(),Emoji::bug(),
				Emoji::snail(),Emoji::ladyBeetle(),Emoji::ant(),Emoji::spider(),
				Emoji::scorpion(),Emoji::crab(),Emoji::snake(),Emoji::turtle(),
				Emoji::tropicalFish(),Emoji::fish(),Emoji::blowfish(),Emoji::dolphin(),
				Emoji::spoutingWhale(),Emoji::whale(),Emoji::crocodile(),Emoji::leopard(),
				Emoji::tiger(),Emoji::waterBuffalo(),Emoji::ox(),Emoji::cow(),
				Emoji::camel(),Emoji::twoHumpCamel(),Emoji::elephant(),Emoji::goat(),
				Emoji::ram(),Emoji::ewe(),Emoji::horse(),Emoji::pig(),
				Emoji::rat(),Emoji::mouse(),Emoji::rooster(),Emoji::turkey(),
				Emoji::dove(),Emoji::dog(),Emoji::poodle(),Emoji::cat(),
				Emoji::rabbit(),Emoji::chipmunk(),Emoji::dragonFace(),Emoji::dragon(),
				Emoji::snowmanWithoutSnow(),
		];
		
		$nests = RaidsDB::getNests();
		
		
		$dataToUser = [
			'chat_id'					=> $chat_id,
			'parse_mode' 				=> 'html',
			'disable_web_page_preview' 	=> true,
		];
		
		if ($nests != NULL){
			$dataToUser['text'] = "<b>Nidi aggiornati al " . RaidsDB::getNestLastUpdate() ."</b>" . PHP_EOL . PHP_EOL ;
		}
		else{
			$dataToUser['text'] = "Non ci sono nidi conosciuti al momento!";
			return Request::sendMessage($dataToUser);
		}
			
		if(!in_array($chat_id,[14303576,209242427]) || $view){
			foreach($nests as $nest){						
				if($nest['enabled']){						
					$dataToUser['text'] .= Emoji::roundPushpin() . sprintf('<a href="https://maps.google.com/maps?q=%s,%s">%s</a>: ',
																				$nest['lat'],$nest['lon'],$nest['zona']);
					$dataToUser['text'] .= $pokemote[array_rand($pokemote, 1)]." <b>".$nest['pokemon']."</b>". PHP_EOL;
				}
			}
		}else{
			foreach($nests as $nest){	
				$dataToUser['text'] .= ($nest['nest_id']<10?"0".$nest['nest_id']:$nest['nest_id'])." ";
				$dataToUser['text'] .= ($nest['enabled']?Emoji::roundPushpin():Emoji::crossMark());
				$dataToUser['text'] .= sprintf('<a href="https://maps.google.com/maps?q=%s,%s">%s</a>: ',$nest['lat'],$nest['lon'],$nest['zona']);
				$dataToUser['text'] .= $pokemote[array_rand($pokemote, 1)]." <b>".$nest['pokemon']."</b>". PHP_EOL;
			}
		}
		$dataToUser['text'] .= PHP_EOL . "(Clicca sulla zona per aprire maps)";
	
	
		return $dataToUser;
		
	}
	
	public static function nestsButtons($chat_id, $status, $nest_id = 0): InlineKeyboard
	{
		$nests = RaidsDB::getNests();
		
		$inlineKeyboard = new InlineKeyboard([]);
		
		
		$keyboard_buttons = [];
		
		if($status == -1)
			if(in_array($chat_id,[14303576,209242427])) {
				$keyboard_buttons[] = new InlineKeyboardButton([
											'callback_data'	=> "nests,$chat_id,0,0",
											'text'			=> "Gestione Nidi",
					]);
					
				call_user_func_array([$inlineKeyboard, 'addRow'], $keyboard_buttons);
				$keyboard_buttons = [];
				$keyboard_buttons[] = new InlineKeyboardButton([
											'callback_data'	=> "nests,$chat_id,11,0",
											'text'			=> "Vedi come i plebei",
					]);
			}
			else return null;
		elseif($status == 0){			
			foreach($nests as $nest){

				$keyboard_buttons[] = new InlineKeyboardButton([
											'callback_data'	=> "nests,$chat_id,1,".$nest['nest_id'],
											'text'			=> $nest['nest_id'],
					]); 
				
			}
			
			$keyboard_rows = array_chunk($keyboard_buttons, 5);
			$keyboard_rows[] = new InlineKeyboardButton([
											'callback_data'	=> "nests,$chat_id,10,0",
											'text'			=> "Scegli il nido da modificare o",
					]);
			
			$keyboard_rows[] = new InlineKeyboardButton([
												'callback_data'	=> "nests,$chat_id,5,0",
												'text'			=> "Aggiungi un Nido",
												
						]);
			$keyboard_rows[] = new InlineKeyboardButton([
												'callback_data'	=> "nests,$chat_id,9,0",
												'text'			=> "o DISABILITA TUTTI",
												
						]);
			$keyboard_rows[] = new InlineKeyboardButton([
												'callback_data'	=> "nests,$chat_id,-1,0",
												'text'			=> "o Termina",
												
						]);
			return new InlineKeyboard(...$keyboard_rows);
		}elseif($status >= 1 and $status <=2){

			$nest = RaidsDB::getNest($nest_id);
			$keyboard_buttons[] = new InlineKeyboardButton([
											'callback_data'	=> "nests,$chat_id,2,".$nest['nest_id'],
											'text'			=> ($nest['enabled']?"Disabilita":"Abilita"),
					]); 
		
			$keyboard_buttons[] = new InlineKeyboardButton([
											'callback_data'	=> "nests,$chat_id,3,".$nest['nest_id'],
											'text'			=> "Cambia Pokemon",
					]); 
				
			$keyboard_buttons[] = new InlineKeyboardButton([
										'callback_data'	=> "nests,$chat_id,4,".$nest['nest_id'],
										'text'			=> "Cancella Nido",
				]); 
			
			call_user_func_array([$inlineKeyboard, 'addRow'], $keyboard_buttons);
			$keyboard_buttons = [];			
		
			$keyboard_buttons[] = new InlineKeyboardButton([
									'callback_data'	=> "nests,$chat_id,0,0",
									'text'			=> "Indietro",
			]); 	
				
		}
		call_user_func_array([$inlineKeyboard, 'addRow'], $keyboard_buttons);
		$keyboard_buttons = [];
		
		return $inlineKeyboard;
		
	}
	
	public static function delNests($chat_id): ServerResponse
	{
		
		$nests = RaidsDB::getNests();
		$dataToUser = [
			'parse_mode' => 'Markdown',
			'chat_id' => $chat_id,
			
		];
		if ($nests != NULL){
			$dataToUser['text'] = "*Seleziona nido da Cancellare*" . PHP_EOL . PHP_EOL ;
		}
		else{
			$dataToUser['text'] = "Non ci sono nidi al momento.";
			return Request::sendMessage($dataToUser);
		}
			// $dataToUser['text'] .= var_dump($nests). PHP_EOL;
		foreach($nests as $nest){
			$dataToUser['text'] .= "ID: ".$nest['nest_id']. " - *".$nest['zona']."*". PHP_EOL;
		}
		
		$dataToUser['text'] .= PHP_EOL . "(per eliminare una zona, /nests del ID)";
		
		return Request::sendMessage($dataToUser);
		
	}
	
	public static function getFullAddress($lat,$lon): string
	{
		$reverseurl = "https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$lon&key=AIzaSyB40BFXEi0s6ZRVi4rQucAdG89xr71g3Ns";

		$address=file_get_contents($reverseurl);
				
		$json_data=json_decode($address);
		
		return $json_data->results[0]->formatted_address;
	}
	
	public static function getChannelParameters($user_id): array
	{
		
		$refeer = RaidsDB::getRefeer($user_id);
		$result = RaidsDB::getChannels($refeer);
		if ($result == null)
			return null;
		else {
			//$result = json_decode($result); 
			return $result;
			
		}
	}
	
	public static function makeMeAdmin($parameters): array
	//parametri nel formato [0]makemeadmin,[1]user,[2]$user_id,[3](step),[4]channel_quantity
	{
			$parameter = explode(",",$parameters);
			
			$user_id = $parameter[2];
			$step = $parameter[3];
			
			if ($parameter[1] == "user"){
				
				$dataToUser = [							//preparo la risposta
					'chat_id' => $user_id,
					'parse_mode' => 'html',
					'disable_web_page_preview' => 'true',
				];
				
				if ($step >= 0){
					
					$textToUser  = "Stai facendo richiesta per diventare admin di un nuovo canale per le segnalazioni raid." . PHP_EOL;
					$textToUser .= "Per qualsiasi informazione e aiuto puoi usare la nostra assistenza scrivendo a @PogoHelpCenterBot" . PHP_EOL . PHP_EOL;
					$textToUser .= "Completa lo step prima di passare al successivo:" . PHP_EOL;
					
					$dataToUser['reply_markup'] = new InlineKeyboard([
						['text' 		 => "Vai al passo successivo", 
						 'callback_data' => "makemeadmin,user,$user_id,1"],	
					]);
					
				}
				if ($step >= 1){
					
					$textToUser .= ($step <2?Emoji::redCircle():Emoji::checkMarkButton()) . " Effettua una donazione su <a href='https://www.paypal.me/SardinianPogoBot/2'>paypal</a> di 2€ per ogni canale che vuoi attivare, specificando il tuo username di telegram nel messaggio." . PHP_EOL;
					
					$dataToUser['reply_markup'] = new InlineKeyboard([
						['text' 		 => "Vai al passo successivo", 
						 'callback_data' => "makemeadmin,user,$user_id,2"],	
					]);
				}
				if ($step >= 2){
					
					$textToUser .= PHP_EOL . "Ora che hai terminato, notifica l'amministratore che ti abiliterà il prima possibile." . PHP_EOL;
					if ($step != 3)
					$dataToUser['reply_markup'] = new InlineKeyboard([
						['text' 		 => "Notifica l'amministratore", 
						 'callback_data' => "makemeadmin,user,$user_id,3"],	
					]);
				}
							
				if ($step >= 4){
					$channel_active = count(RaidsDB::getChannels($user_id));//self::getChannelParameters($user_id);
					$channel_quantity = RaidsDB::getChannelQuantity($user_id);
					$channel_left = $channel_quantity-$channel_active;
					
					$textToUser  = "L'amministratore ha aumentato i canali a tua disposizione! Al momento puoi gestire fino a <b>$channel_quantity</b> canal" . ($channel_quantity==1?"e.":"i.") ." Hai ancora <b>$channel_left</b> slot disponibil" . ($channel_left==1?"e.":"i.") . PHP_EOL;
					$textToUser .= "Usa questo link per fai iscrivere i tuoi utenti, e quando segnaleranno un raid, potranno scegliere tra i canali da te impostati. <code>https://t.me/SardinianPogoBot/?start=$user_id</code>" . PHP_EOL;
					$textToUser .= "Per associare un canale devi aggiungere <i>come amministratore</i> @SardinianPogoBot nel canale che vuoi associare, ed inoltrare un messaggio <b>di testo</b> inviato successivamente all'ingresso del bot dal canale in questione a questa chat." . PHP_EOL;	
					$textToUser .= "Per qualsiasi informazione o aiuto puoi scrivere a @PogoHelpCenterBot" . PHP_EOL;	
					
				}
				
				$dataToUser['text'] = $textToUser;
				
				return $dataToUser;
			}
			else{
				
				$dataToAdmin = [							//preparo la risposta
					'chat_id' => 14303576,
					'parse_mode' => 'html',
					'disable_web_page_preview' => 'true',
					
				];
				$username = RaidsDB::getUsername($user_id);
				
				//controllo che sia stato effettuato il pagamento
				//controllo che il bot abbia permessi di admin nel nuovo canale
				//controllo che il canale sia censito nella tabella chat
				if ($step >= 0){
					
					$textToAdmin  = "Ciao Capo, l'utente @$username (<code>$user_id</code>) ha richiesto l'attivazione di un nuovo canale" . PHP_EOL;
					$textToAdmin .= "Vediamo un po' se ha fatto le cose come si deve:" . PHP_EOL;
					$dataToAdmin['reply_markup'] = new InlineKeyboard([
						['text' 		 => "Vai allo step successivo", 
						 'callback_data' => "makemeadmin,admin,$user_id,1"],	
					]);
					
				}
				if ($step >= 1){
					
					$textToAdmin .= ($step <2?Emoji::redCircle():Emoji::checkMarkButton()) . " La donazione sia stata effettuata sull'account <a href='https://www.paypal.com'>paypal</a> specificando l'username di telegram nel messaggio." . PHP_EOL;
					$dataToAdmin['reply_markup'] = new InlineKeyboard([
						['text' 		 => "Ha effettuto il pagamento", 
						 'callback_data' => "makemeadmin,admin,$user_id,2,0"],
					]);
				}
				if ($step >= 2){
					
					$channel_quantity = RaidsDB::getChannelQuantity($user_id);
					
					$textToAdmin .= ($step <3?Emoji::redCircle():Emoji::checkMarkButton()) . " Quanti canali vuoi abilitare? Al momento l'utente ha un massimo di <b>$channel_quantity</b> canali attivi" . PHP_EOL;
					
					$dataToAdmin['reply_markup'] = new InlineKeyboard([
						['text'  => "-1", 'callback_data' => "makemeadmin,admin,$user_id,2,-1"],	
						['text'  => "1", 'callback_data' => "makemeadmin,admin,$user_id,2,1"],	
						['text'  => "2", 'callback_data' => "makemeadmin,admin,$user_id,2,2"],	
						['text'  => "3", 'callback_data' => "makemeadmin,admin,$user_id,2,3"],	
						['text'  => "4", 'callback_data' => "makemeadmin,admin,$user_id,2,4"],	
					],[
						['text'  => "Termina", 'callback_data' => "makemeadmin,admin,$user_id,3"],
					]);
					
				}
				if ($step >= 3){
				
					$textToAdmin .= Emoji::checkMarkButton() . " Hai terminato la procedura e notificato l'utente." . PHP_EOL;

				}
				
				$dataToAdmin['text'] = $textToAdmin;
				
				return $dataToAdmin;
				
			}
			
	}
	
	public static function manageNewChannel($forwardFromChat,$user_id): ServerResponse
	{
		
		$forwardFromChat_id 	= $forwardFromChat->getId();
		$forwardFromChat_title	= $forwardFromChat->getTitle();
		
		$username = RaidsDB::getUsername($user_id);
		$channel_quantity = RaidsDB::getChannelQuantity($user_id);
		
		if ($channel_quantity == 0)
			return Request::emptyResponse();
		else{
			$channel_active = count(RaidsDB::getChannels($user_id));
			$channel_left = $channel_quantity-$channel_active;	
		
			
			$textToAdmin	= "@$username(<code>$user_id</code>) ha inoltrato un messaggio dal canale <b>$forwardFromChat_title</b>(<code>$forwardFromChat_id</code>) ";
			if($channel_left > 0){
				$insertToChannels = RaidsDB::addChannel($user_id,$forwardFromChat_id);
				
				Raids::logChannel("@$username (<code>$user_id</code>) #request #newchannel <b>$forwardFromChat_title</b>(<code>$forwardFromChat_id</code>)");

				$textToUser  = "Il messaggio che hai appena inviato proviene dal canale <b>$forwardFromChat_title</b>, ed è stato registrato correttamente dal sistema." . PHP_EOL;

				$textToUser .= "Se pensi che questo sia un errore, o lo hai inviato per sbaglio, premi /cancel".str_replace('-','',$forwardFromChat_id);
			}
			else
			{
				$textToUser  = "Hai raggiunto il limite massimo di canali utilizzabili! Per aggiungerne di nuovi, puoi usare il comando /makemeadmin$user_id";
			}

			// $dataToAdmin = [
					// 'chat_id' => 14303576,
					// 'parse_mode' => 'html',
					// 'disable_web_page_preview' => 'true',
					// 'text' => $textToAdmin. " var: ".print_r($insertToChannels,true),
				// ];
			// Request::sendMessage($dataToAdmin);
		}
	
		$dataToUser = [							//preparo la risposta
					'chat_id' => $user_id,
					'parse_mode' => 'html',
					'disable_web_page_preview' => 'true',
					'text' => $textToUser,
				];

		return Request::sendMessage($dataToUser);		
	}
	public static function profile($user_id,$modifica = "full"): array
	{
		
		$dbdata = [
			"ingamename" 		=> "InGameName(IGN)",
			"team" 				=> "Team",
			"livello" 			=> "Livello",
			"exptotale" 		=> "Punti Esperienza",
			"pokemonregistrati" => "Pokedex",
			"medagliepalestra" 	=> "Medaglie Palestra",
			"oropalestra"	 	=> "Med. Oro Palestra",
			"kmpercorsi" 		=> "Podista",
			"pokemoncatturati"	=> "Cercapokémon",
			"pokemonevoluti" 	=> "Scienziato",
			"uovaschiuse" 		=> "Allevapokémon",
			"pokestopvisitati" 	=> "Giramondo",
			"turista"			=> "Turista",
			"magikarpgrandi" 	=> "Pescatore",
			"lottevinte"	 	=> "Combat Girl",
			"pokemonallenati" 	=> "Fantallenatore",
			"rattatapiccoli" 	=> "Marmocchio",
			"pikachu"		 	=> "Fan di Pikachu",
			"unown"			 	=> "Unown", 
			"raidnormali"	 	=> "Campione",
			"raidleggendari"	=> "Lotta Leggendaria",
			"bacchepalestra" 	=> "Asso delle Bacche",
			"difesapalestra" 	=> "Capopalestra",
			"missioni"	 		=> "Pokémon Ranger",
			"miglioriamici"	 	=> "Pop Star", 
			"scambipokemon"	 	=> "Gentiluomo",
			"wayfarer"			=> "Wayfarer", 
			"distanzascambi" 	=> "Pilota",
			"legaMega"			=> "Lega Mega",
			"legaUltra"			=> "Lega Ultra",
			"legaMaster"		=> "Lega Master",
			"paparazzo"			=> "Paparazzo",
			"eroe"				=> "Eroe",
			"purificatore"		=> "Purificatore",
			"ultraeroe"			=> "UltraEroe",
			"migliorCompagno"	=> "Miglior Compagno",
			"erede"				=> "Erede",
			"gurumega"			=> "Guru della Meg.",
			"triatleta"			=> "Triatleta",
			"giovanesperanza"	=> "Giovane Speranza",
			"duogiovanisperanze"=> "Duo Giovani Speranze",
			"amantedelpicnic"	=> "Amante del Picnic",
			"artistadiraid"		=> "Artista di Raid",
			"idunivoco"			=> "ID Univoco",
		];
		
		$team = [
			0	=> "Sconosciuto",
			1 	=> "Blu",
			2 	=> "Rosso",
			3 	=> "Giallo",
		];
		
		$data = [
				'chat_id' => $user_id,
				'parse_mode' => 'html',
			];
			
		if ($modifica == "full"){

			$userDetails = RaidsDB::getUser($user_id);
			
			$text  = "<b>Profilo Utente</b>" . PHP_EOL . PHP_EOL;	
			
			$text .= "<code>Segnalazioni:</code> <b>" . $userDetails["segnalazioni"] ."</b>" .PHP_EOL;
			if($userDetails["id"] == $userDetails["refeer"]){
				$text .= "<code>Canali Attivi:</code> <b>" . count(RaidsDB::getChannels($user_id)) ."</b>" .PHP_EOL;
				$text .= "<code>Canali Massimi:</code> <b>" . $userDetails["channel_quantity"] ."</b>" .PHP_EOL;
			}
			else
				$text .= "<code>Referente:</code> @" . RaidsDB::getUsername($userDetails["refeer"]) .PHP_EOL;
			
			$text .= PHP_EOL . PHP_EOL;
			
			if($userDetails["idunivoco"] ==! NULL){
				
				$text .= "<b>Il tuo ID Univoco da condividere con gli amici è:</b>" . PHP_EOL;
				$text .= "<code>" . $userDetails["idunivoco"] . "</code>" . PHP_EOL;
				$text .= "Attualmente la visibilità del tuo codice univoco è impostata su <b>" . (($userDetails["idunivocoprivacy"])?"ON":"OFF") . "</b>";
				$text .= PHP_EOL . PHP_EOL;
			}
			
			$text .= "Le notifiche per i raid Ex sono <b>" . (($userDetails["raid"])?"Attive":"Disattivate") . "</b>";
			$text .= PHP_EOL . PHP_EOL;
			
			foreach ($dbdata as $key => $element ){
				if($key == "team")
					$userDetails[$key] = $team[$userDetails[$key]];
				if(is_numeric($userDetails[$key]))
					$userDetails[$key] = number_format($userDetails[$key], 0, ',',".");
				
				$text .= "<code>$element:</code> <b>" . $userDetails[$key] ."</b>" .PHP_EOL;
			}
				$date = date_create($userDetails['datainizio']);
				$text .= "<code>Data Inizio:</code> <b>" . (isset($userDetails['datainizio'])?date_format($date,'d/m/Y'):$userDetails['datainizio']) ."</b>" .PHP_EOL;
			
			$data["reply_markup"] = self::profileButtons($userDetails);
			$data["text"] = $text;//print_r($userDetails,true);	
		}
		else{
			$text = "<b>Modifica Profilo Utente</b>" . PHP_EOL ;
			$text .= "Seleziona il campo da modificare, Termina per tornare al tuo profilo.";
			
			$data["reply_markup"] = self::profileEditButtons($user_id);
			$data["text"] = $text;
		}
		return $data;
		
	}
	public static function profileButtons($userDetails): InlineKeyboard
	{
		$user_id = $userDetails["id"];
		
		$inlineKeyboard = new InlineKeyboard([]);
		$keyboard_buttons = [];
		
		$keyboard_buttons[] = new InlineKeyboardButton(['text'=> "Modifica Profilo",'callback_data' => "profilo,$user_id,edit,profilo"]); 
				
		if($userDetails["team"] == "Sconosciuto")
			$keyboard_buttons[] = new InlineKeyboardButton(['text'=> "Edit Team",'callback_data' => "profilo,$user_id,edit,team"]); 
		
		call_user_func_array([$inlineKeyboard, 'addRow'], $keyboard_buttons);
		$keyboard_buttons = [];
		
		
		$keyboard_buttons[] = new InlineKeyboardButton(['text'=> "Codice Amico",'callback_data' => "profilo,$user_id,edit,idunivoco"]); 
		
		
		$keyboard_buttons[] = new InlineKeyboardButton([
				'text'=> "Vista ID: ".(($userDetails["idunivocoprivacy"])?"ON":"OFF"),
				'callback_data' => "profilo,$user_id,edit,idunivocoprivacy".(($userDetails["idunivocoprivacy"])?",0":",1")]); 
		
		call_user_func_array([$inlineKeyboard, 'addRow'], $keyboard_buttons);
		$keyboard_buttons = [];
		
		$keyboard_buttons[] = new InlineKeyboardButton([
				'text'=> "Notifiche Raid: ".(($userDetails["raid"])?"ON":"OFF"),
				'callback_data' => "profilo,$user_id,edit,raid".(($userDetails["raid"])?",0":",1")]); 
		
		call_user_func_array([$inlineKeyboard, 'addRow'], $keyboard_buttons);
		$keyboard_buttons = [];
		
		if($userDetails["id"] == $userDetails["refeer"]){

			$keyboard_buttons[] = new InlineKeyboardButton(['text' => 'Edit Canali', 'callback_data' => "profilo,$user_id,edit,canali"]);
			$keyboard_buttons[] = new InlineKeyboardButton(['text' => 'Agg. Canali', 'callback_data' => "profilo,$user_id,add,canali"]);
		
			call_user_func_array([$inlineKeyboard, 'addRow'], $keyboard_buttons);
			$keyboard_buttons = [];
			
			$keyboard_buttons[] = new InlineKeyboardButton(['text' => 'Gestisci Player', 'callback_data' => "profilo,$user_id,edit,player"]);
			
			call_user_func_array([$inlineKeyboard, 'addRow'], $keyboard_buttons);
			$keyboard_buttons = [];
		}
				
		$keyboard_buttons[] = new InlineKeyboardButton(['text' => 'Vedi Statistiche', 'callback_data' => "statistiche,$user_id,local,segnalazioni"]);
			
		call_user_func_array([$inlineKeyboard, 'addRow'], $keyboard_buttons);
		$keyboard_buttons = [];
		return $inlineKeyboard;
	}
	
	public static function profileEditButtons($user_id,$command= "profilo",$buttonType = "edit"): InlineKeyboard
	{
		
		$inlineKeyboard = new InlineKeyboard([]);
		$keyboard_buttons = [];
		
		$dbdata = [
			"ingamename" 		=> "InGameName(IGN)",
			"livello" 			=> "Livello",
			"exptotale" 		=> "Punti Esperienza",
			"pokemonregistrati" => "Pokedex",
			"medagliepalestra" 	=> "Medaglie Palestra",
			"oropalestra"	 	=> "Med. Oro Palestra",
			"kmpercorsi" 		=> "Podista",
			"pokemoncatturati"	=> "Cercapokémon",
			"pokemonevoluti" 	=> "Scienziato",
			"uovaschiuse" 		=> "Allevapokémon",
			"pokestopvisitati" 	=> "Giramondo",
			"turista"			=> "Turista",
			"magikarpgrandi" 	=> "Pescatore",
			"lottevinte"	 	=> "Combat Girl",
			"pokemonallenati" 	=> "Fantallenatore",
			"rattatapiccoli" 	=> "Marmocchio",
			"pikachu"		 	=> "Fan di Pikachu",
			"unown"			 	=> "Unown", 
			"raidnormali"	 	=> "Campione",
			"raidleggendari"	=> "Lotta Leggendaria",
			"bacchepalestra" 	=> "Asso delle Bacche",
			"difesapalestra" 	=> "Capopalestra",
			"missioni"	 		=> "Pokémon Ranger",
			"miglioriamici"	 	=> "Pop Star", 
			"scambipokemon"	 	=> "Gentiluomo",
			"wayfarer"			=> "Wayfarer", 
			"distanzascambi" 	=> "Pilota",
			"legaMega"			=> "Lega Mega",
			"legaUltra"			=> "Lega Ultra",
			"legaMaster"		=> "Lega Master",
			"paparazzo"			=> "Paparazzo",
			"eroe"				=> "Eroe",
			"purificatore"		=> "Purificatore",
			"ultraeroe"			=> "UltraEroe",
			"migliorCompagno"	=> "Miglior Compagno",
			"erede"				=> "Erede",
			"gurumega"			=> "Guru della Meg.",
			"triatleta"			=> "Triatleta",
			"giovanesperanza"	=> "Giovane Speranza",
			"duogiovanisperanze"=> "Duo Giovani Speranze",
			"amantedelpicnic"	=> "Amante del Picnic",
			"artistadiraid"		=> "Artista di Raid",
			"datainizio"	 	=> "Data Inizio",
		];
		$cutline = 0;
		
		foreach ($dbdata as $key => $element ){
			
			$keyboard_buttons[] = new InlineKeyboardButton(['text'=> "$element",'callback_data' => "$command,$user_id,$buttonType,$key"]); 
			$cutline++;
			if($cutline == 3){
				$cutline = 0; 
				call_user_func_array([$inlineKeyboard, 'addRow'], $keyboard_buttons);
				$keyboard_buttons = [];
			}
		}
		call_user_func_array([$inlineKeyboard, 'addRow'], $keyboard_buttons);
		$keyboard_buttons = [];
		
		$keyboard_buttons[] = new InlineKeyboardButton(['text' => 'Termina', 'callback_data' => "profilo,$user_id,$buttonType,termina"]);
			
		call_user_func_array([$inlineKeyboard, 'addRow'], $keyboard_buttons);
		$keyboard_buttons = [];
		
		return $inlineKeyboard;
	}
	
	public static function rankingButtons($user_id,$command= "profilo",$buttonType = "edit"): InlineKeyboard
	{
		
		$inlineKeyboard = new InlineKeyboard([]);
		$keyboard_buttons = [];
		
		$dbdata = [
			"segnalazioni" 		=> "Segnalazioni",
			"livello" 			=> "Livello",
			"exptotale" 		=> "Punti Esperienza",
			"pokemonregistrati" => "Pokedex",
			"medagliepalestra" 	=> "Medaglie Palestra",
			"oropalestra"	 	=> "Med. Oro Palestra",
			"kmpercorsi" 		=> "Podista",
			"pokemoncatturati"	=> "Cercapokémon",
			"pokemonevoluti" 	=> "Scienziato",
			"uovaschiuse" 		=> "Allevapokémon",
			"pokestopvisitati" 	=> "Giramondo",
			"turista"			=> "Turista",
			"magikarpgrandi" 	=> "Pescatore",
			"lottevinte"	 	=> "Combat Girl",
			"pokemonallenati" 	=> "Fantallenatore",
			"rattatapiccoli" 	=> "Marmocchio",
			"pikachu"		 	=> "Fan di Pikachu",
			"unown"			 	=> "Unown", 
			"raidnormali"	 	=> "Campione",
			"raidleggendari"	=> "Lotta Leggendaria",
			"bacchepalestra" 	=> "Asso delle Bacche",
			"difesapalestra" 	=> "Capopalestra",
			"missioni"	 		=> "Pokémon Ranger",
			"miglioriamici"	 	=> "Pop Star", 
			"scambipokemon"	 	=> "Gentiluomo",
			"wayfarer"			=> "Wayfarer", 
			"distanzascambi" 	=> "Pilota",
			"legaMega"			=> "Lega Mega",
			"legaUltra"			=> "Lega Ultra",
			"legaMaster"		=> "Lega Master",
			"paparazzo"			=> "Paparazzo",
			"eroe"				=> "Eroe",
			"purificatore"		=> "Purificatore",
			"ultraeroe"			=> "UltraEroe",
			"migliorCompagno"	=> "Miglior Compagno",
			"erede"				=> "Erede",
			"gurumega"			=> "Guru della Meg.",
			"triatleta"			=> "Triatleta",
			"giovanesperanza"	=> "Giovane Speranza",
			"duogiovanisperanze"=> "Duo Giovani Speranze",
			"amantedelpicnic"	=> "Amante del Picnic",
			"artistadiraid"		=> "Artista di Raid",
		];
		$cutline = 0;
		
		foreach ($dbdata as $key => $element ){
			
			$keyboard_buttons[] = new InlineKeyboardButton(['text'=> "$element",'callback_data' => "$command,$user_id,$buttonType,$key"]); 
			$cutline++;
			if($cutline == 3){
				$cutline = 0; 
				call_user_func_array([$inlineKeyboard, 'addRow'], $keyboard_buttons);
				$keyboard_buttons = [];
			}
		}
		call_user_func_array([$inlineKeyboard, 'addRow'], $keyboard_buttons);
		$keyboard_buttons = [];
		
		$keyboard_buttons[] = new InlineKeyboardButton(['text' => 'Termina', 'callback_data' => "profilo,$user_id,$buttonType,termina"]);
			
		call_user_func_array([$inlineKeyboard, 'addRow'], $keyboard_buttons);
		$keyboard_buttons = [];
		
		return $inlineKeyboard;
	}
	
	public static function showStats($user_id,$field = "segnalazioni", $verso = "DESC",$ambito = false): array
	{
		
		$data = [
				'chat_id' => $user_id,
				'parse_mode' => 'html',
			];
			
			$currentPos = 0;
			$fullStats = RaidsDB::getStats($user_id,$field,$verso,$ambito);
			
			$text  = "<b>Classifica ".($ambito?"Globale":"Locale").":</b> <i>$field</i>" . PHP_EOL . PHP_EOL;	
			
			$text  .= "Ecco la lista dei primi 10 classificati:". PHP_EOL . PHP_EOL;	
			$pos = 01;
			$totUser = count($fullStats);
			foreach($fullStats as $user){
				if($pos <=10)
					$text .=  str_pad($user['rank'], 2, '0', STR_PAD_LEFT). ": <code>". $user['ingamename'] ."</code> - <b>". number_format($user[$field], 0, ',',".") ."</b>" . PHP_EOL;
				
				if($user_id == $user['id'])
					$currentPos = $user['rank'];
				$pos++;
			}
			
			$text .= PHP_EOL . "Attualmente sei <b>".$currentPos."°</b> su $totUser!" . PHP_EOL . PHP_EOL;
			
			if($field == "segnalazioni")
			{
				$text .= "Segnalazioni per Team:" . PHP_EOL;
				$segnalazioniPerTeam = RaidsDB::segnalazioniPerTeam();
					foreach($segnalazioniPerTeam as $team)
						$text .= "<code>". $team['team'] ."</code> - <b>". $team['segnalazioni'] ."</b>" . PHP_EOL;
						
				$text .= PHP_EOL . "Player per Team:" . PHP_EOL;
				$playerPerTeam = RaidsDB::playerPerTeam();
					foreach($playerPerTeam as $team)
						$text .= "<code>". $team['team'] ."</code> - <b>". $team['N. Player'] ."</b>" . PHP_EOL;
			}
			$data["reply_markup"] = self::rankingButtons($user_id,"statistiche");
			
			$data["text"] = $text;
		
		return $data;
		
	}
	
	public static function gymEx($user_id): array
	{
		//controllo lo stato attuale delle notifiche
		$status = [Emoji::crossMark(),Emoji::checkMarkButton()];
		
		$data = [
				'chat_id' => $user_id,
				'parse_mode' => 'html',
			];
		
		$palestre = RaidsDB::getGymEx();
		
		$text = "<b>Gestisci Palestre Ex Attive</b>" . PHP_EOL . PHP_EOL;
		
		foreach($palestre as $palestra)
			$text.= $status[$palestra['gym_ex_active']] . " " . ucfirst($palestra['gym_name']) . PHP_EOL;  
		
		
		$data["reply_markup"] = self::gymExButton();
		
		$data["text"] = $text;
		
		return $data;
	}
	
	public static function gymExButton(): InlineKeyboard
	{

		$palestre = RaidsDB::getGymEx();
		$inlineKeyboard = new InlineKeyboard([]);
		$keyboard_buttons = [];
		
		foreach($palestre as $palestra)
			$keyboard_buttons[] = new InlineKeyboardButton(['text'=> $palestra['gym_name'],
							'callback_data' => "gymEx,".$palestra['gym_id'].($palestra['gym_ex_active']?",0":",1")
							]); 
		$keyboard_buttons[] = new InlineKeyboardButton(['text'=> "Pubblica",
							'callback_data' => "gymExPubblica"
							]); 
		$keyboard_rows = array_chunk($keyboard_buttons, 1);
		return new InlineKeyboard(...$keyboard_rows);
		
	}
	public static function notifyRaidEx($raid_id)
	{
		
		$raidEx = RaidsDB::getRaidEx($raid_id);
		$inlineKeyboard = new InlineKeyboard([]);
		$keyboard_buttons = [];
		$lat = $raidEx['lat'];
		$lon = $raidEx['lon'];
		
		date_default_timezone_set('Europe/Rome');
		setlocale(LC_TIME, 'it_IT.utf8');
		//$datetime = date_create_from_format("Y-m-d H:i:s",$raidEx['datetime']);
		$orario = ucwords(strftime('%A %d %B %H:%M',strtotime($raidEx['datetime'])));
		
		$full_address = self::getFullAddress($lat, $lon);

        $data = [
				'chat_id' => 14303576,
				'parse_mode' => 'html',
				'disable_web_page_preview' => 'true',
			];
		$list = RaidsDB::getNotificationToUser("Raid");
		
		$text  = Emoji::fire() . Emoji::fire() ." <b>Nuovo Raid EX Segnalato</b> " . Emoji::fire() . Emoji::fire() . PHP_EOL . PHP_EOL;
		$text .= Emoji::convenienceStore() . " <b>" . $raidEx['gym_name'] . "</b>" . PHP_EOL;
		$text .= Emoji::roundPushpin() . ' <a href="http://maps.google.com/maps?q='.$lat.','.$lon.'">'. $full_address . '</a>' . PHP_EOL; 
		$text .= Emoji::spiralCalendar() . " $orario" . PHP_EOL;
		$text .= ($raidEx['chat_link']?Emoji::chains().' <a href="'  . $raidEx['chat_link'] . '">Chat Raid EX</a>' . PHP_EOL . PHP_EOL:PHP_EOL);
		$text .= "Per non ricevere più queste notifiche, modifica l'opzione raid dal menù /profilo";
		
		$data['text'] = $text;
	
		$data['reply_markup'] = new InlineKeyboard([
						['text'  => "Ho ricevuto il pass", 'callback_data' => "raidex,$raid_id,1"],
						],[	
						['text'  => "Mettimi in lista per riceverlo da un amico", 'callback_data' => "raidex,$raid_id,0"],
						],[	
						['text'  => "Il giorno non posso/non sono interessato", 'callback_data' => "raidex,$raid_id,-1"],	
						
					]);
		foreach($list as $user_id){
			$data['chat_id'] = $user_id['id'];
			Request::sendMessage($data);
		}
	}
	
	public static function printActiveRaid($raid_id, $user_id): string
	{
		$raidEx = RaidsDB::getRaidEx($raid_id);
		$user = RaidsDB::getRaidExUser($raid_id, $user_id);
	
		$lat = $raidEx['lat'];
		$lon = $raidEx['lon'];
		
		// date_default_timezone_set('Europe/Rome');
		setlocale(LC_TIME, 'it_IT.utf8');
		//$datetime = date_create_from_format("Y-m-d H:i:s",$raidEx['datetime']);
		$orario = ucwords(strftime('%A %d %B %H:%M',strtotime($raidEx['datetime'])));
		
		$full_address = self::getFullAddress($lat, $lon);
		
		$text  = Emoji::convenienceStore() . " <b>" . $raidEx['gym_name'] . "</b>" . PHP_EOL;
		$text .= Emoji::roundPushpin() . ' <a href="http://maps.google.com/maps?q='.$lat.','.$lon.'">'. $full_address . '</a>' . PHP_EOL; 
		$text .= Emoji::spiralCalendar() . " $orario" . PHP_EOL;
		$text .= ($raidEx['chat_link']?Emoji::chains().' <a href="'  . $raidEx['chat_link'] . '">Chat Raid EX</a>' . PHP_EOL . PHP_EOL:PHP_EOL);
		
		if($user['got_pass'] == 0 and $user['available'] == 0 and $user['user_friend_id'] == null) //non ha ricevuto pass e non vuole riceverlo
			$text .= "<i>Non hai dato disponibilità a ricevere il pass per questo raid Ex.</i>" . PHP_EOL . PHP_EOL;
										
		if($user['got_pass'] == 0 and $user['available'] == 0 and $user['user_friend_id'] <> null) //ha ricevuto il pass da un amico
			$text .= sprintf("<i>Hai ricevuto il pass da</i> @%s",RaidsDB::getUsername($user['user_friend_id'])) . PHP_EOL . PHP_EOL;
		
		elseif($user['got_pass'] == 1 and $user['available'] == 0  and $user['user_friend_id'] == null) //ha ricevuto pass e non vuole metterlo a disposizione
			$text .= "<i>Hai ricevuto il pass per questo raid Ex e non hai messo a disposizione l'invito</i>" . PHP_EOL . PHP_EOL;
		
		elseif($user['got_pass'] == 1 and $user['available'] == 0  and $user['user_friend_id'] <> null) //ha ricevuto pass e già dato ad amico
			$text .= sprintf("<i>Hai invitato</i> @%s <i>per questo raid Ex.</i>",RaidsDB::getUsername($user['user_friend_id'])) . PHP_EOL . PHP_EOL;
										
		elseif($user['got_pass'] == 0 and $user['available'] == 1  and $user['user_friend_id'] == null) //non ha ricevuto e vuole riceverlo
			$text .= "<i>Sei inserito nella lista degli utenti disponibili per questo raid Ex.</i>" . PHP_EOL . PHP_EOL;
		
		elseif($user['got_pass'] == 0 and $user['available'] == 1  and $user['user_friend_id'] <> null) //richiesta in attesa di risposta
			$text .= sprintf("<i>Hai richiesto il pass a</i> @%s, richiesta in attesa di risposta.",RaidsDB::getUsername($user['user_friend_id'])) . PHP_EOL . PHP_EOL;
										
		elseif($user['got_pass'] == 1 and $user['available'] == 1 and $user['user_friend_id'] == null ) //ricevuto pass e messo a disposizione
			$text .= "<i>Hai ricevuto il pass per questo raid Ex.</i>" . PHP_EOL . PHP_EOL;
										
		elseif($user['got_pass'] == 1 and $user['available'] == 1 and $user['user_friend_id'] <> null ) //invito in attesa di accettazione
			$text .= sprintf("<i>Hai invitato</i> @%s <i>per questo raid Ex, l'invito è in attesa di accettazione</i>",RaidsDB::getUsername($user['user_friend_id'])) . PHP_EOL . PHP_EOL;;
		
		
		$text .= self::printActiveRaidUsers($raid_id);
		
		$text .= PHP_EOL . PHP_EOL . ($raidEx['chat_link']?Emoji::chains().' <a href="'  . $raidEx['chat_link'] . '">ENTRA NELLA CHAT</a>':"");
		return $text;
	}
	
	public static function printActiveRaidUsers($raid_id): string
	{
		$users 		= RaidsDB::getRaidExUsers($raid_id);
		$stats 		= RaidsDB::getRaidExStat($raid_id);
		$availables = RaidsDB::getRaidExUsersFilter($raid_id,1,1,null);
		$needs 		= RaidsDB::getRaidExUsersFilter($raid_id,0,1,null);
		
		$text  = sprintf("Partecipanti: <b>%d</b> \n", $stats['player']);
		$text .= sprintf("Inviti Disponibili: <b>%d</b> \n", $stats['invitidisponibili']);
		$text .= sprintf("In attesa di invito: <b>%d</b> \n\n", $stats['attesainvito']);
	
		foreach($users as $user){
			$text .= "@" . RaidsDB::getUsername($user['user_id']) . PHP_EOL;
		}
		
		if(!empty($availables)){
			$text .= PHP_EOL . "<b>Inviti disponibili:</b>" . PHP_EOL;
			
			foreach($availables as $available){
				$text .= "<i>" . RaidsDB::getUsername($available['user_id']) . "</i>, ";
			}
			$text = substr($text, 0, -2);
		}
		
		if(!empty($needs)){
			$text .= PHP_EOL . "<b>In attesa:</b>" . PHP_EOL;
			
			foreach($needs as $need){
				$text .= "<i>" . RaidsDB::getUsername($need['user_id']) . "</i>, ";
			}
			$text = substr($text, 0, -2);
		}
		
		return $text;
	}
	
	public static function printActiveRaidButtons($raid_id, $user_id): InlineKeyboard
	{
		$user = RaidsDB::getRaidExUser($raid_id, $user_id);
		
		if($user['got_pass'] == 0 and $user['available'] == 0 and $user['user_friend_id'] == null) //non ha ricevuto pass e non vuole riceverlo
			return new InlineKeyboard([['text'  => "Ho ricevuto il pass", 'callback_data' => "passex,$raid_id,1"]],
									  [['text'  => "Mettimi in lista per riceverlo da un amico", 'callback_data' => "passex,$raid_id,0"]],
									  [['text'  => "Aggiorna", 'callback_data' => "passex,$raid_id,-1"]]);
										
		if($user['got_pass'] == 0 and $user['available'] == 0 and $user['user_friend_id'] <> null) //ha ricevuto il pass da un amico
			return new InlineKeyboard([['text'  => "Aggiorna", 'callback_data' => "passex,$raid_id,-1"]]);
		
		elseif($user['got_pass'] == 1 and $user['available'] == 0  and $user['user_friend_id'] == null) //ha ricevuto pass e non vuole metterlo a disposizione
			return new InlineKeyboard([['text'  => "Rendi disponibile pass", 'callback_data' => "passex,$raid_id,12"]],
										[['text'  => "Aggiorna", 'callback_data' => "passex,$raid_id,-1"]]);
		
		elseif($user['got_pass'] == 1 and $user['available'] == 0  and $user['user_friend_id'] <> null) //ha ricevuto pass e già dato ad amico
			return new InlineKeyboard([['text'  => "Aggiorna", 'callback_data' => "passex,$raid_id,-1"]]);
										
		elseif($user['got_pass'] == 0 and $user['available'] == 1  and $user['user_friend_id'] == null) //non ha ricevuto e vuole riceverlo
			return new InlineKeyboard([['text'  => "Richiedi pass ad un utente", 'callback_data' => "passex,$raid_id,2"]],
										[['text'  => "Aggiorna", 'callback_data' => "passex,$raid_id,-1"]]);
		
		elseif($user['got_pass'] == 0 and $user['available'] == 1  and $user['user_friend_id'] <> null) //non possibile
			return new InlineKeyboard(/*[['text'  => "Annulla richiesta pass", 'callback_data' => "passex,$raid_id,4"]],*/
										[['text'  => "Aggiorna", 'callback_data' => "passex,$raid_id,-1"]]); 
										
		elseif($user['got_pass'] == 1 and $user['available'] == 1 and $user['user_friend_id'] == null ) //ricevuto pass e messo a disposizione
			return new InlineKeyboard([['text'  => "Assegna pass ad un richiedente", 'callback_data' => "passex,$raid_id,3"]],
										[['text'  => "Aggiorna", 'callback_data' => "passex,$raid_id,-1"]]);
										
		elseif($user['got_pass'] == 1 and $user['available'] == 1 and $user['user_friend_id'] <> null ) //non possibile
			return new InlineKeyboard(/*[['text'  => "Annulla invio pass", 'callback_data' => "passex,$raid_id,5"]],*/
										[['text'  => "Aggiorna", 'callback_data' => "passex,$raid_id,-1"]]);
			
	}
	
	public static function managePassEx($raid_id, $user_id, $status, $mode): InlineKeyboard
	{
		
		if($mode == "text"){
			$text = "";
			switch($status){
				case 2:
					$text  = "Stai richiedendo il pass ad un utente che ha messo a disposizione il proprio invito. " . PHP_EOL;
					$text .= "<b>Attenzione:</b> il Bot non tiene conto dei livelli amicizia tra i giocatori, ";
					$text .= "quindi manda la richiesta solo alle persone che possono inviartelo (devi avere 3/4 cuori con quella persona).". PHP_EOL;
					$text .= "<i>L'utente dovrà accettare o rifiutare la tua richiesta prima di poterne effettuare un'altra.</i>" . PHP_EOL . PHP_EOL;
					$text .= "Seleziona un utente dalla lista qui sotto:";
				break;
				case 3:
					$text  = "Stai assegnando il pass ad un utente che ha dato disponibilità per ricevere l'invito. " . PHP_EOL;
					$text .= "<b>Attenzione:</b> il Bot non tiene conto dei livelli amicizia tra i giocatori, ";
					$text .= "quindi manda la richiesta solo alle persone che possono inviartelo (devi avere 3/4 cuori con quella persona)." . PHP_EOL;
					$text .= "<i>L'utente dovrà accettare o rifiutare la tua richiesta prima di poterne effettuare un'altra.</i>" . PHP_EOL . PHP_EOL;
					$text .= "Seleziona un utente dalla lista qui sotto:";
			}
			
			return $text;
		}else{
			$inlineKeyboard = new InlineKeyboard([]);
			$keyboard_buttons = [];
			
			switch($status){
				case 2:
					
					$users = RaidsDB::getRaidExUsersFilter($raid_id, 1, 1, null);
					$nextStep = 6;
				break;
				case 3:	
					$users = RaidsDB::getRaidExUsersFilter($raid_id,0, 1, null);					
					$nextStep = 9;
				break;
			}		
				if(empty($users))
					$keyboard_buttons[] = new InlineKeyboardButton(['text' => 'Nessun utente disponibile, torna indietro', 'callback_data' => "passex,$raid_id,-1"]);
				else{
					foreach($users as $user)
						$keyboard_buttons[] = new InlineKeyboardButton(['text'=> RaidsDB::getUsername($user['user_id']),
									'callback_data' => "passex,$raid_id,$nextStep,".$user['user_id']
									]); 	
					
					$keyboard_buttons[] = new InlineKeyboardButton(['text' => 'Indietro', 'callback_data' => "passex,$raid_id,-1"]);
				}
				$keyboard_rows = array_chunk($keyboard_buttons, 2);
					
				
			
			return new InlineKeyboard(...$keyboard_rows);
			
		}
	}
	
	public static function askPassEx($raid_id,$user_id,$user_friend_id,$mode): ServerResponse
	{
		
		$dataToFriend = [
						'chat_id' => $user_friend_id,
						'parse_mode' => 'html',
						'disable_web_page_preview' => 'true',
					];
		$raidEx = RaidsDB::getRaidEx($raid_id);
		
		if($mode == "domanda"){
			$dataToFriend['text'] = sprintf("@%s chiede se puoi invitarlo al raid Ex press <b>%s</b>, vuoi accettare?",RaidsDB::getUsername($user_id),$raidEx['gym_name']);
			
			$dataToFriend['reply_markup'] =  new InlineKeyboard([['text'=> "Accetta",
																'callback_data' => "passex,$raid_id,7,$user_id"],
																	['text'=> "Rifiuta",
																'callback_data' => "passex,$raid_id,8,$user_id"],
									]);

		}else{
			$dataToFriend['text'] = sprintf("@%s vuole darti il pass per il raid a <b>%s</b>, vuoi accettare?",RaidsDB::getUsername($user_id),$raidEx['gym_name']);
			
			$dataToFriend['reply_markup'] =  new InlineKeyboard([['text'=> "Accetta",
																'callback_data' => "passex,$raid_id,10,$user_id"],
																	['text'=> "Rifiuta",
																'callback_data' => "passex,$raid_id,11,$user_id"],
												]);
		}	
		
		Request::sendMessage($dataToFriend);
	}
	
	function a_number_format($number_in_iso_format, $no_of_decimals=3, $decimals_separator='.', $thousands_separator='', $digits_grouping=3)
	{
		// Check input variables
		if (!is_numeric($number_in_iso_format)){
			error_log("Warning! Wrong parameter type supplied in my_number_format() function. Parameter \$number_in_iso_format is not a number.");
			return false;
		}
		if (!is_numeric($no_of_decimals)){
			error_log("Warning! Wrong parameter type supplied in my_number_format() function. Parameter \$no_of_decimals is not a number.");
			return false;
		}
		if (!is_numeric($digits_grouping)){
			error_log("Warning! Wrong parameter type supplied in my_number_format() function. Parameter \$digits_grouping is not a number.");
			return false;
		}
		
		
		// Prepare variables
		$no_of_decimals = $no_of_decimals * 1;
		
		
		// Explode the string received after DOT sign (this is the ISO separator of decimals)
		$aux = explode(".", $number_in_iso_format);
		// Extract decimal and integer parts
		$integer_part = $aux[0];
		$decimal_part = isset($aux[1]) ? $aux[1] : '';
		
		// Adjust decimal part (increase it, or minimize it)
		if ($no_of_decimals > 0){
			// Check actual size of decimal_part
			// If its length is smaller than number of decimals, add trailing zeros, otherwise round it
			if (strlen($decimal_part) < $no_of_decimals){
				$decimal_part = str_pad($decimal_part, $no_of_decimals, "0");
			} else {
				$decimal_part = substr($decimal_part, 0, $no_of_decimals);
			}
		} else {
			// Completely eliminate the decimals, if there $no_of_decimals is a negative number
			$decimals_separator = '';
			$decimal_part       = '';
		}
		
		// Format the integer part (digits grouping)
		if ($digits_grouping > 0){
			$aux = strrev($integer_part);
			$integer_part = '';
			for ($i=strlen($aux)-1; $i >= 0 ; $i--){
				if ( $i % $digits_grouping == 0 && $i != 0){
					$integer_part .= "{$aux[$i]}{$thousands_separator}";
				} else {
					$integer_part .= $aux[$i];            
				}
			}
		}
		
		$processed_number = "{$integer_part}{$decimals_separator}{$decimal_part}";
		return $processed_number;
	}
}
