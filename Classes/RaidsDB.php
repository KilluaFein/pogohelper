<?php
/**
 * This file is part of the TelegramBot package.
 *
 * (c) Avtandil Kikabidze aka LONGMAN <akalongman@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Longman\TelegramBot;

use Exception;
use Longman\TelegramBot\Exception\TelegramException;
use Longman\TelegramBot\Request;
use PDO;

/**
 * Class RaidsDB
 */
class RaidsDB extends DB
{
    /**
     * Initilize raids table
     */
    public static function initializeRaidsDb()
    {
        if (!defined('TB_RAIDS')) {
            define('TB_RAIDS', self::$table_prefix . 'raids');
            define('TB_USER_DETAILS', self::$table_prefix . 'user_details');
            define('TB_TEAMS', self::$table_prefix . 'teams');
            define('TB_NESTS', self::$table_prefix . 'nests');
            define('TB_CHANNELS', self::$table_prefix . 'channels');
            define('TB_GYM', self::$table_prefix . 'gym');
            define('TB_POKEMON', self::$table_prefix . 'pokemon');
            define('TB_RAIDEX', self::$table_prefix . 'raidEx');
            define('TB_RAIDEX_USER', self::$table_prefix . 'raidEx_user');
            define('V_SEGNALAZIONI_PER_TEAM', 'V_SegnalazioniPerTeam');
            define('V_PLAYER_PER_TEAM', 'V_UtentiPerTeam');
        }
    }


    public static function selectChats($select_chats_params)
    {
        if (!self::isDbConnected()) {
            return false;
        }
        // Set defaults for omitted values.
        $select = array_merge([
            'groups'      => true,
            'supergroups' => true,
            'channels'    => true,
            'users'       => true,
            'date_from'   => null,
            'date_to'     => null,
            'chat_id'     => null,
            'text'        => null,
        ], $select_chats_params);
        if (!$select['groups'] && !$select['users'] && !$select['supergroups']) {
            return false;
        }
        try {
            $query = '
                SELECT * ,
                ' . TB_CHAT . '.`id` AS `chat_id`,
                ' . TB_CHAT . '.`username` AS `chat_username`,
                ' . TB_CHAT . '.`created_at` AS `chat_created_at`,
                ' . TB_CHAT . '.`updated_at` AS `chat_updated_at`
            ';
            if ($select['users']) {
                $query .= '
                    , ' . TB_USER . '.`id` AS `user_id`
                    FROM `' . TB_CHAT . '`
                    LEFT JOIN `' . TB_USER . '`
                    ON ' . TB_CHAT . '.`id`=' . TB_USER . '.`id`
                ';
            } else {
                $query .= 'FROM `' . TB_CHAT . '`';
            }
            //Building parts of query
            $where  = [];
            $tokens = [];
            if (!$select['groups'] || !$select['users'] || !$select['supergroups']) {
                $chat_or_user = [];
                $select['groups'] && $chat_or_user[] = TB_CHAT . '.`type` = "group"';
                $select['supergroups'] && $chat_or_user[] = TB_CHAT . '.`type` = "supergroup"';
                $select['channels'] && $chat_or_user[] = TB_CHAT . '.`type` = "channel"';
                $select['users'] && $chat_or_user[] = TB_CHAT . '.`type` = "private"';
                $where[] = '(' . implode(' OR ', $chat_or_user) . ')';
            }
            if (null !== $select['date_from']) {
                $where[]              = TB_CHAT . '.`updated_at` >= :date_from';
                $tokens[':date_from'] = $select['date_from'];
            }
            if (null !== $select['date_to']) {
                $where[]            = TB_CHAT . '.`updated_at` <= :date_to';
                $tokens[':date_to'] = $select['date_to'];
            }
            if (null !== $select['chat_id']) {
                $where[]            = TB_CHAT . '.`id` = :chat_id';
                $tokens[':chat_id'] = $select['chat_id'];
            }
            if (null !== $select['text']) {
                if ($select['users']) {
                    $where[] = '(
                        LOWER(' . TB_CHAT . '.`title`) LIKE :text
                        OR LOWER(' . TB_USER . '.`first_name`) LIKE :text
                        OR LOWER(' . TB_USER . '.`last_name`) LIKE :text
                        OR LOWER(' . TB_USER . '.`username`) LIKE :text
                    )';
                } else {
                    $where[] = 'LOWER(' . TB_CHAT . '.`title`) LIKE :text';
                }
                $tokens[':text'] = '%' . strtolower($select['text']) . '%';
            }
            if (!empty($where)) {
                $query .= ' WHERE ' . implode(' AND ', $where);
            }
			$query .= ' AND ' . TB_USER_DETAILS . '.`enabled` = 1';
            $query .= ' ORDER BY ' . TB_CHAT . '.`updated_at` ASC';
            $sth = self::$pdo->prepare($query);
            $sth->execute($tokens);
            return $sth->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            throw new TelegramException($e->getMessage());
        }
    }	
/*-------------------- USERS - UTENTI ------------------------*/
	public static function isEnabledUser($user_id)
    {
        if (!self::isDbConnected()) {
            return false;
        }
		try {
            self::initializeRaidsDb();
            $sth = self::$pdo->prepare('SELECT `enabled` FROM '.TB_USER_DETAILS.' WHERE `id` = :userid;');

            
            $sth->bindParam(':userid', $user_id, \PDO::PARAM_STR);

            $sth->execute();
			
            $temp = $sth->fetch();
			return $temp[0];
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}	
	
	public static function getUsername($user_id)
    {
        if (!self::isDbConnected()) {
            return false;
        }
		try {
            self::initializeRaidsDb();
            $sth = self::$pdo->prepare('SELECT `username` FROM '.TB_USER.' WHERE `id` = :userid;');

            
            $sth->bindParam(':userid', $user_id, \PDO::PARAM_INT);

            $sth->execute();
			
            $temp = $sth->fetch();
			return $temp[0];
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}
    
	public static function getUserByUsername($username)
    {
        if (!self::isDbConnected()) {
            return false;
        }
		try {
            self::initializeRaidsDb();
            $sth = self::$pdo->prepare('SELECT `id` FROM '.TB_USER.' WHERE `username` = :username;');

            
            $sth->bindParam(':username', $username, \PDO::PARAM_STR);

            $sth->execute();
			
            $temp = $sth->fetch();
			return $temp[0];
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}
	

	public static function getRefeer($user_id)
    {
        if (!self::isDbConnected()) {
            return false;
        }
		try {
            self::initializeRaidsDb();
            $sth = self::$pdo->prepare('SELECT `refeer` FROM '.TB_USER_DETAILS.' WHERE `id` = :userid;');

            
            $sth->bindParam(':userid', $user_id, \PDO::PARAM_STR);

            $sth->execute();
			
            $temp = $sth->fetch();
			return $temp[0];
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}
	public static function getRefeers()
    {
        if (!self::isDbConnected()) {
            return false;
        }
		try {
            self::initializeRaidsDb();
            $sth = self::$pdo->prepare('SELECT DISTINCT `user_id` FROM '.TB_CHANNELS.';');


            $sth->execute();
			
            return $sth->fetchAll(PDO::FETCH_COLUMN, 0);
			 
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}
	
	public static function isInstinctPlayer($user_id)
    {
        if (!self::isDbConnected()) {
            return false;
        }
		try {
            self::initializeRaidsDb();
            $sth = self::$pdo->prepare('SELECT t.`team` 
								FROM '.TB_TEAMS.' t JOIN '.TB_USER_DETAILS.' ud  on ud.team = t.team_id 
								WHERE `id` = :userid and `refeer` = 14303576;' );

            
            $sth->bindParam(':userid', $user_id, \PDO::PARAM_STR);

            $sth->execute();
			
            $temp = $sth->fetch();
			return $temp[0];
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}
	
	public static function setEnabled($user_id,$state)
	{
		if (!self::isDbConnected()) {
            return false;
        }
		
		try {
            self::initializeRaidsDb();
            $sth = self::$pdo->prepare('UPDATE '.TB_USER_DETAILS.' SET `enabled`=:state where `id` = :userid;');
            
            $sth->bindParam(':state', $state, \PDO::PARAM_STR);
            $sth->bindParam(':userid', $user_id, \PDO::PARAM_STR);

            return $sth->execute();
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}
	
	public static function addSegnalazioni($userid)
	{
		if (!self::isDbConnected()) {
            return false;
        }
		
		try {
            self::initializeRaidsDb();
            $sth = self::$pdo->prepare('UPDATE '.TB_USER_DETAILS.' SET `segnalazioni`=`segnalazioni`+1 where `id` = :userid;');

            $sth->bindParam(':userid', $userid, \PDO::PARAM_STR);

            return $sth->execute();
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}
	
	public static function getChannelQuantity($userid)
	 {
        if (!self::isDbConnected()) {
            return false;
        }
		try {
            self::initializeRaidsDb();
            $sth = self::$pdo->prepare('SELECT ud.`channel_quantity` FROM '.TB_USER_DETAILS.' ud WHERE `id` = :userid;');

            
            $sth->bindParam(':userid', $userid, \PDO::PARAM_STR);

            $sth->execute();
			
            $temp = $sth->fetch();
			if ($temp[0] == null)
				return 0;
			else
				return $temp[0];
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}

	public static function setChannelQuantity($userid,$quantity)	
	{
		if (!self::isDbConnected()) {
            return false;
        }
		
		try {
            self::initializeRaidsDb();
            $sth = self::$pdo->prepare('UPDATE '.TB_USER_DETAILS.' SET `channel_quantity`=:channel_quantity where `id` = :userid;');

            $sth->bindParam(':userid', $userid, \PDO::PARAM_STR);
            $sth->bindParam(':channel_quantity', $quantity, \PDO::PARAM_STR);

            return $sth->execute();
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}
	
	public static function getUser($userid)
	{
		if (!self::isDbConnected()) {
            return false;
        }
		try {
            self::initializeRaidsDb();
            $sth = self::$pdo->prepare('SELECT * FROM '.TB_USER.' u
							JOIN '.TB_USER_DETAILS.' ud ON u.id = ud.id
							WHERE u.`id` = :userid;');

            
            $sth->bindParam(':userid', $userid, \PDO::PARAM_STR);

            $sth->execute();
			
            $temp = $sth->fetch(PDO::FETCH_ASSOC);
			
			return $temp;
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}
	public static function getUsers()
	{
		if (!self::isDbConnected()) {
            return false;
        }
		try {
            self::initializeRaidsDb();
            $sth = self::$pdo->prepare('SELECT * FROM '.TB_USER.' u
							JOIN '.TB_USER_DETAILS.' ud ON u.id = ud.id
							WHERE ud.enabled = 1 and ud.idunivoco IS NOT NULL and ud.idunivocoprivacy = 1
							ORDER BY ud.ingamename; ');// 

            $sth->execute();
			
            $temp = $sth->fetchAll(PDO::FETCH_ASSOC);
			
			return $temp;
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}
	public static function setUserDetail($user_id, $username, $field, $value)
	{
		if (!self::isDbConnected()) {
            return false;
        }
		try {
            self::initializeRaidsDb();
            
            $sth = self::$pdo->prepare('
                INSERT INTO `' . TB_USER_DETAILS . '`
                (`id`,`username`,`' . $field . '`)
				VALUES (:user_id, :username, :value)
                ON DUPLICATE KEY UPDATE
                    `username`       = VALUES(`username`),
                    `' . $field . '`       = VALUES(`' . $field . '`)
            ');
            
            $sth->bindParam(':user_id', $user_id, \PDO::PARAM_STR);
            $sth->bindParam(':username', $username, \PDO::PARAM_STR);
            $sth->bindParam(':value', $value, \PDO::PARAM_STR);

            return $sth->execute();
			
            //$temp = $sth->fetch(PDO::FETCH_ASSOC);
			
			//return $temp;
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}
	
/*-------------------- NOTIFICHE ------------------------*/		
	public static function getNotificationStatus($user_id)
    {
        if (!self::isDbConnected()) {
            return false;
        }
		try {
            self::initializeRaidsDb();
            $sth = self::$pdo->prepare('SELECT `tyranitar`,`snorlax`,`lapras`,`zapdos`,`articuno`,`moltres`,`mewtwo`,`mew`,
												`raikou`,`entei`,`suicune`,`lugia`,`hooh`,`celebi`,`groudon`,`kyogre`,
												`absol`
							FROM '.TB_USER.' u
							JOIN '.TB_USER_DETAILS.' ud ON u.id = ud.id
							WHERE u.`id` = :userid;');

            
            $sth->bindParam(':userid', $user_id, \PDO::PARAM_STR);

            $sth->execute();

			return $sth->fetch(PDO::FETCH_ASSOC);
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}
	public static function setNotificationStatus($field, $status, $user_id, $username) //also user for refeer
    {
        if (!self::isDbConnected()) {
            return false;
        }
		try {
            self::initializeRaidsDb();
            $sth = self::$pdo->prepare('
                INSERT INTO `' . TB_USER_DETAILS . '`
                (`id`,`username`,`' . $field . '`)
				VALUES (:id, :username, :status)
                ON DUPLICATE KEY UPDATE
                    `username`       = VALUES(`username`),
                    `' . $field . '`       = VALUES(`' . $field . '`)
            ');
            
            $sth->bindParam(':id', $user_id, \PDO::PARAM_STR);
            $sth->bindParam(':username', $username, \PDO::PARAM_STR);
            $sth->bindParam(':status', $status, \PDO::PARAM_STR);

			return $sth->execute();
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}
	
	public static function getNotificationToUser($field)
    {
        if (!self::isDbConnected()) {
            return false;
        }
		try {
            self::initializeRaidsDb();
            $sth = self::$pdo->prepare('SELECT ud.`id` 
							FROM '.TB_USER.' u
							JOIN '.TB_USER_DETAILS.' ud ON u.id = ud.id
							WHERE `' . $field . '` AND `enabled` >= 1 ');


            $sth->execute();

			return $sth->fetchAll();
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}
	
/*-------------------- RAIDS ------------------------*/		
	public static function insertRaid($segnalazione)
	{
		if (!self::isDbConnected()) {
            return false;
        }
		
		try {			
            self::initializeRaidsDb();	
            $sql='
                INSERT INTO `' . TB_RAIDS . '`
                (`raidlevel`, `zona`, `oraschiusa`, `user_id`, `photo_id`, `message_id`, `channel_id`, `fixed_text`, `pokemon`, ';
				
			if(isset($segnalazione['gym_id']))
				$sql .= '`team`,`gym_id`,';
			
			$sql .='`t1`, `t2`, `t3`, `t1member`, `t2member`, `t3member`)
                VALUES
                (:raidlevel, :zona, :oraschiusa, :user_id, :photo_id, :message_id, :channel_id, :fixed_text, :pokemon,';
				
			if(isset($segnalazione['gym_id']))
				$sql .= ':team, :gym_id, ';	
			
			
    		$sql .=':t1, :t2, :t3, :t1member, :t2member, :t3member)
				ON DUPLICATE KEY UPDATE
                    `message_id`       = VALUES(`message_id`),
                    `fixed_text`       = VALUES(`fixed_text`),
                    `pokemon`       = VALUES(`pokemon`),
                    `t1member`       = VALUES(`t1member`),
                    `t2member`       = VALUES(`t2member`),
                    `t3member`       = VALUES(`t3member`)
                 
                    
            ';
			$sth = self::$pdo->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
			
			if(isset($segnalazione['gym_id'])){
				$sth->bindParam(':team', $segnalazione['team'], PDO::PARAM_STR);
				$sth->bindParam(':gym_id', $segnalazione['gym_id'], PDO::PARAM_STR);
			}
            $sth->bindParam(':raidlevel', $segnalazione['raidlevel'], PDO::PARAM_STR);
            $sth->bindParam(':zona', $segnalazione['zona'], PDO::PARAM_STR);
            $sth->bindParam(':oraschiusa', $segnalazione['oraschiusa'], PDO::PARAM_STR);
            $sth->bindParam(':user_id', $segnalazione['user_id'], PDO::PARAM_STR);
            $sth->bindParam(':photo_id', $segnalazione['photo_id'], PDO::PARAM_STR);
            $sth->bindParam(':message_id', $segnalazione['message_id'], PDO::PARAM_STR);
            $sth->bindParam(':channel_id', $segnalazione['chat_id'], PDO::PARAM_STR);
            $sth->bindParam(':fixed_text', $segnalazione['fixed_text'], PDO::PARAM_STR);
            $sth->bindParam(':pokemon', $segnalazione['pokemon'], PDO::PARAM_STR);
            $sth->bindParam(':t1', $segnalazione['t1'], PDO::PARAM_STR);
            $sth->bindParam(':t2', $segnalazione['t2'], PDO::PARAM_STR);
            $sth->bindParam(':t3', $segnalazione['t3'], PDO::PARAM_STR);
            $sth->bindParam(':t1member', $segnalazione['t1member'], PDO::PARAM_STR);
            $sth->bindParam(':t2member', $segnalazione['t2member'], PDO::PARAM_STR);
            $sth->bindParam(':t3member', $segnalazione['t3member'], PDO::PARAM_STR);
			
            return $sth->execute();
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}
	
	public static function getGymByName($gym_name)
    {
		if (!self::isDbConnected()) {
            return false;
        }
		try {
            self::initializeRaidsDb();
            $sth = self::$pdo->prepare('SELECT *
							FROM '.TB_GYM.' g
							WHERE g.`gym_name` like "%'.$gym_name.'%" 
				;');

            
            // $sth->bindParam(':gym_name', $gym_name, \PDO::PARAM_STR);
           
            
			$sth->execute();

			return $sth->fetchAll(PDO::FETCH_ASSOC);
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}
	public static function getGymById($gym_id)
    {
		if (!self::isDbConnected()) {
            return false;
        }
		try {
            self::initializeRaidsDb();
            $sth = self::$pdo->prepare('SELECT *
							FROM '.TB_GYM.' g
							WHERE g.`gym_id` = :gym_id 
				;');

            
            $sth->bindParam(':gym_id', $gym_id, \PDO::PARAM_STR);
           
            
			$sth->execute();

			return $sth->fetch(PDO::FETCH_ASSOC);
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}
	
	public static function getRaid($message_id,$channel_id)
    {
        if (!self::isDbConnected()) {
            return false;
        }
		try {
            self::initializeRaidsDb();
            $sth = self::$pdo->prepare('SELECT *
							FROM '.TB_RAIDS.' r
							WHERE r.`message_id` = :message_id AND 
								  r.`channel_id` = :channel_id 
				;');

            
            $sth->bindParam(':message_id', $message_id, \PDO::PARAM_STR);
            $sth->bindParam(':channel_id', $channel_id, \PDO::PARAM_STR);
            
			$sth->execute();

			return $sth->fetch(PDO::FETCH_ASSOC);
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}		
	public static function getRaidByGym($gym_id)
    {
        if (!self::isDbConnected()) {
            return false;
        }
		try {
            self::initializeRaidsDb();
            $sth = self::$pdo->prepare('SELECT *  
							FROM '.TB_RAIDS.' r
							WHERE r.`gym_id` = :gym_id  
							ORDER BY `id` DESC LIMIT 1
				;');

            
            $sth->bindParam(':gym_id', $gym_id, \PDO::PARAM_STR);
            
            
			$sth->execute();

			return $sth->fetch(PDO::FETCH_ASSOC);
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}	
/*-------------------- NESTS - NIDI ------------------------*/	
	public static function getNests()
    {
        if (!self::isDbConnected()) {
            return false;
        }
		try {
            self::initializeRaidsDb();
            $sth = self::$pdo->prepare('SELECT `nest_id`,`zona`,`pokemon`,`lat`,`lon`,`address`,`enabled`,`date_add`
							FROM '.TB_NESTS.' n
							;
							');

            $sth->execute();

			return $sth->fetchAll(PDO::FETCH_ASSOC);
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}	

	public static function getNest($nest_id)
    {
        if (!self::isDbConnected()) {
            return false;
        }
		try {
            self::initializeRaidsDb();
            $sth = self::$pdo->prepare('SELECT `nest_id`,`zona`,`pokemon`,`lat`,`lon`,`address`,`enabled`,`date_add`
							FROM '.TB_NESTS.' n
							WHERE n.`nest_id` = :nest_id;');

            
            $sth->bindParam(':nest_id', $nest_id, \PDO::PARAM_STR);
            
			$sth->execute();

			return $sth->fetch(PDO::FETCH_ASSOC);
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}
	public static function getNestLastUpdate()
	{
		if (!self::isDbConnected()) {
            return false;
        }
		try {
            self::initializeRaidsDb();
            $sth = self::$pdo->prepare('SELECT DATE_FORMAT(MAX(`date_add`),"%d/%m/%Y") AS data FROM '.TB_NESTS.' n WHERE `enabled` = 1');
			
			$sth->execute();
			
			$temp = $sth->fetch();
			
			$startDate = date_create_from_format('d/m/Y', '03/04/2019');
			$nestDate  = date_create_from_format('d/m/Y', $temp[0]);
			
			// while($startDate->diff($nestDate)->days >= 1) 
			while($startDate<($nestDate) )
				date_add($startDate, date_interval_create_from_date_string('2 week'));
			date_sub($startDate, date_interval_create_from_date_string('2 week'));
			date_add($startDate, date_interval_create_from_date_string('1 day'));
			// return $startDate->diff($nestDate)->days;
			return $startDate->format("d/m/Y");
			
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}
	public static function updateNestStatus($nest_id,$status)
	{
		if (!self::isDbConnected()) {
            return false;
        }
		try {
            self::initializeRaidsDb();
            $sth = self::$pdo->prepare('UPDATE '.TB_NESTS.' SET `enabled` = :status WHERE `nest_id` = :nest_id');

			$sth->bindParam(':status', $status, \PDO::PARAM_INT);
            $sth->bindParam(':nest_id', $nest_id, \PDO::PARAM_INT);

            

			return $sth->execute();
			
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}
	
	public static function disableNestsStatus()
	{
		if (!self::isDbConnected()) {
            return false;
        }
		try {
            self::initializeRaidsDb();
            $sth = self::$pdo->prepare('UPDATE '.TB_NESTS.' SET `enabled` = 0 WHERE 1');

			return $sth->execute();
			
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}
		
	public static function updNestPokemon($nest_id, $pokemon)
	{
		if (!self::isDbConnected()) {
            return false;
        }
		try {
            $sth = self::$pdo->prepare('UPDATE '.TB_NESTS.' SET `pokemon` = :pokemon WHERE `nest_id` = :nest_id');

			$sth->bindParam(':pokemon', $pokemon, \PDO::PARAM_STR);
            $sth->bindParam(':nest_id', $nest_id, \PDO::PARAM_INT);

            

			return $sth->execute();
			
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}
		
	public static function addNest($zona, $pokemon, $lat, $lon, $address)
    {
        if (!self::isDbConnected()) {
            return false;
        }
		try {
            self::initializeRaidsDb();
            $sth = self::$pdo->prepare('
                INSERT INTO `' . TB_NESTS . '`
                (`zona`,`pokemon`,`lat`,`lon`,`address`)
				VALUES (:zona, :pokemon, :lat, :lon, :address)
                ON DUPLICATE KEY UPDATE
                    `zona`       = VALUES(`zona`),
                    `pokemon`       = VALUES(`pokemon`),
                    `lat`       = VALUES(`lat`),
                    `lon`       = VALUES(`lon`),
                    `address`       = VALUES(`address`)
                    
            ');
            
            $sth->bindParam(':zona', $zona, \PDO::PARAM_STR);
            $sth->bindParam(':pokemon', $pokemon, \PDO::PARAM_STR);
            $sth->bindParam(':lat', $lat, \PDO::PARAM_STR);
            $sth->bindParam(':lon', $lon, \PDO::PARAM_STR);
            $sth->bindParam(':address', $address, \PDO::PARAM_STR);

            

			return $sth->execute();
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}

	public static function delNest($nest_id)
	{
		if (!self::isDbConnected()) {
            return false;
        }
		
		try {
            self::initializeRaidsDb();
            $sth = self::$pdo->prepare('UPDATE '.TB_NESTS.' SET `enabled`=0 where `nest_id` = :nest_id;');

            $sth->bindParam(':nest_id', $nest_id, \PDO::PARAM_STR);

            return $sth->execute();
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}
	/*-------------------- CHANNELS - CANALI ------------------------*/	
	public static function addChannel($user_id, $channel_id, $enabled = 1, $pulsanti = 2,$minuti = 30)
    {
        if (!self::isDbConnected()) {
            return false;
        }
		try {
            self::initializeRaidsDb();
            $sth = self::$pdo->prepare('
                INSERT INTO `' . TB_CHANNELS . '`
                (`user_id`,`channel_id`,`pulsanti`,`minuti`,`enabled`)
				VALUES (:user_id, :channel_id, :pulsanti, :minuti, :enabled)
                ON DUPLICATE KEY UPDATE
                    `pulsanti`       = VALUES(`pulsanti`),
                    `minuti`       	 = VALUES(`minuti`),
                    `enabled`      	 = VALUES(`enabled`)
                    
            ');
            
            $sth->bindParam(':user_id', $user_id, \PDO::PARAM_STR);
            $sth->bindParam(':channel_id', $channel_id, \PDO::PARAM_STR);
            $sth->bindParam(':pulsanti', $pulsanti, \PDO::PARAM_STR);            
            $sth->bindParam(':minuti', $minuti, \PDO::PARAM_STR);            
            $sth->bindParam(':enabled', $enabled, \PDO::PARAM_STR);            
			
			
			return $sth->execute();
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}	
	public static function deleteChannel($user_id,$channel_id)
    {
        if (!self::isDbConnected()) {
            return false;
        }
		try {
            self::initializeRaidsDb();
            $sth = self::$pdo->prepare('DELETE FROM '.TB_CHANNELS.'
							WHERE `user_id` = :user_id
							AND `channel_id` = :channel_id;
							');

			$sth->bindParam(':user_id', $user_id, \PDO::PARAM_STR);            
			$sth->bindParam(':channel_id', $channel_id, \PDO::PARAM_STR);            
			
			
			
			return $sth->execute();
       
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}	
	
	public static function getChannels($user_id,$enabled = 1)
    {
        if (!self::isDbConnected()) {
            return false;
        }
		try {
            self::initializeRaidsDb();
            $sth = self::$pdo->prepare('SELECT `user_id`,`channel_id`,`title`,`pulsanti`,`minuti`
							FROM '.TB_CHANNELS.' ch left join 
							'.TB_CHAT.' c on ch.channel_id = c.id
							WHERE `user_id` = :user_id
							AND ch.enabled = :enabled;
							');

			$sth->bindParam(':user_id', $user_id, \PDO::PARAM_STR);            
			$sth->bindParam(':enabled', $enabled, \PDO::PARAM_STR);            
			
			$sth->execute();
			
			return $sth->fetchAll(PDO::FETCH_ASSOC);
       
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}	

	public static function getChannelParameter($user_id, $channel_id)
    {
        if (!self::isDbConnected()) {
            return false;
        }
		try {
            self::initializeRaidsDb();
            $sth = self::$pdo->prepare('SELECT `pulsanti`,`minuti`
							FROM '.TB_CHANNELS.' ch 
							WHERE `user_id` = :user_id AND `channel_id` = :channel_id;
							');

			$sth->bindParam(':user_id', $user_id, \PDO::PARAM_STR);            
			$sth->bindParam(':channel_id', $channel_id, \PDO::PARAM_STR);            
			
            $sth->execute();
			
            $temp = $sth->fetch();
			return $temp[0];
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}	

	public static function getStats($user_id,$field = "segnalazioni", $verso = "DESC", $global = false)
	{
		if (!self::isDbConnected()) {
            return false;
        }
		try {
            self::initializeRaidsDb();
			
			$query = "SELECT `id`,`ingamename`, `".$field."`  ";
			
			$query .= ", (
					  SELECT COUNT(".$field.") 
						FROM ".TB_USER_DETAILS." udsub 
						WHERE (udsub.".$field." >= ud.".$field.") AND `team` <> 0 AND `enabled` > 0
						) AS rank ";
			
			$query .= "FROM ".TB_USER_DETAILS." ud ";
			
			$query .= "WHERE `team` <> 0 AND `enabled` > 0 ";	
			
			if(!$global){
				
				$refeer = self::getRefeer($user_id);
				
				$query .= "AND `refeer` = :refeer ";	
			}
			
			$query .= "ORDER BY `" . $field . "` " . $verso . ", `ingamename` ASC ;";
			
            $sth = self::$pdo->prepare($query);

			if(isset($refeer))
				$sth->bindParam(':refeer', $refeer, \PDO::PARAM_INT);
			
            $sth->execute();
			
            return $sth->fetchAll(PDO::FETCH_ASSOC);
			
			
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}
	
	public static function segnalazioniPerTeam($verso = "ASC", $global = false)
	{
		if (!self::isDbConnected()) {
            return false;
        }
		try {
            self::initializeRaidsDb();
			
			$query = "SELECT * FROM ".V_SEGNALAZIONI_PER_TEAM." st ";

            $sth = self::$pdo->prepare($query);
			
            $sth->execute();
			
            return $sth->fetchAll(PDO::FETCH_ASSOC);
			
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}	
	public static function playerPerTeam($verso = "ASC", $global = false)
	{
		if (!self::isDbConnected()) {
            return false;
        }
		try {
			
			$query = "SELECT * FROM ".V_PLAYER_PER_TEAM." pt ";

            $sth = self::$pdo->prepare($query);
			
            $sth->execute();
			
            return $sth->fetchAll(PDO::FETCH_ASSOC);
			
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}
	/*-------------------- GYM - PALESTRE ------------------------*/			
	
	public static function getGymEx($active = 0)
    {
        if (!self::isDbConnected()) {
            return false;
        }
		try {
            self::initializeRaidsDb();
            $sth = self::$pdo->prepare('SELECT `gym_id`,`gym_name`,`gym_ex_active`,`lat`,`lon`
							FROM '.TB_GYM.' g 
							WHERE `gym_ex` = 1 AND `gym_ex_active` >= :active;
							');

			$sth->bindParam(':active', $active, \PDO::PARAM_STR);              
			
			$sth->execute();
			
			return $sth->fetchAll(PDO::FETCH_ASSOC);
       
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}	
	public static function getGymActive($gym_id)
    {
        if (!self::isDbConnected()) {
            return false;
        }
		try {
            self::initializeRaidsDb();
            $sth = self::$pdo->prepare('SELECT `gym_active`
							FROM '.TB_GYM.' g 
							WHERE `gym_id` = :gym_id;
							');

			$sth->bindParam(':gym_id', $gym_id, \PDO::PARAM_STR);              
			
			$sth->execute();
			
            $temp = $sth->fetch();
			return $temp[0];
       
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}
	public static function setGymEx($gym_id,$active)
	{
        if (!self::isDbConnected()) {
            return false;
        }
		try {
            self::initializeRaidsDb();
            $sth = self::$pdo->prepare(
				'UPDATE `' . TB_GYM . '` SET
				`gym_ex_active` = :active
				WHERE `gym_id` = :gym_id;
			');

			$sth->bindParam(':gym_id', $gym_id, \PDO::PARAM_INT);              
			$sth->bindParam(':active', $active, \PDO::PARAM_INT);              
			
			return $sth->execute();
       
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}
	public static function insertGym($raid_data)
	{
		if (!self::isDbConnected()) {
            return false;
        }
		try {
            self::initializeRaidsDb();
            $sth = self::$pdo->prepare('
                INSERT INTO `' . TB_GYM . '`
                (`gym_id`,`gym_name`,`lat`,`lon`)
				VALUES (:gym_id, :gym_name, :lat, :lon)
                ON DUPLICATE KEY UPDATE
                    `raid`       = `raid`+1,
					`updated_at` = NOW();
                    
            ');
            
            $sth->bindParam(':gym_id', $raid_data['internal_id'], \PDO::PARAM_INT);
            $sth->bindParam(':gym_name', $raid_data['name'], \PDO::PARAM_STR);
            $sth->bindParam(':lat', $raid_data['lat'], \PDO::PARAM_STR);
            $sth->bindParam(':lon', $raid_data['lon'], \PDO::PARAM_STR);
		return $sth->execute();
       
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}
	public static function getMaxGymId()
	{
		if (!self::isDbConnected()) {
            return false;
        }
		try {
            self::initializeRaidsDb();
            $sth = self::$pdo->prepare('SELECT max(`gym_id`)
							FROM '.TB_GYM.' g 
							WHERE 1;
							');

		$sth->execute();
			
		$temp = $sth->fetch();
		return $temp[0];
       
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}
	public static function getGymByLatLon($lat,$lon)
	{
		if (!self::isDbConnected()) {
            return false;
        }
		try {
            self::initializeRaidsDb();
            $sth = self::$pdo->prepare('SELECT * 
							FROM '.TB_GYM.' g 
							WHERE `lat` = :lat AND `lon` = :lon;
							');
							
			$sth->bindParam(':lat', $lat, \PDO::PARAM_STR);
            $sth->bindParam(':lon', $lon, \PDO::PARAM_STR);

		$sth->execute();
			
		return $sth->fetch(PDO::FETCH_ASSOC);
		 
       
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}
	
	public static function insertNewGym($portal_name,$portal_lat,$portal_lon,$portal_original_id,$portal_link)
	{
		if (!self::isDbConnected()) {
            return false;
        }
		try {
            self::initializeRaidsDb();
			
			$max_gym_id = self::getMaxGymId() + 1;
            $sth = self::$pdo->prepare('
                INSERT INTO `' . TB_GYM . '`
                (`gym_id`,`gym_name`,`lat`,`lon`,`original_id`,`image_url`)
				VALUES (:max_gym_id, :gym_name, :lat, :lon, :original_id, :image_link)
                ON DUPLICATE KEY UPDATE
                    `original_id`       =  VALUES(`original_id`),
                    `image_url`         =  VALUES(`image_url`),
					`updated_at` = NOW();
                    
            ');
            
            $sth->bindParam(':max_gym_id', $max_gym_id, \PDO::PARAM_INT);
            $sth->bindParam(':gym_name', $portal_name, \PDO::PARAM_STR);
            $sth->bindParam(':lat', $portal_lat, \PDO::PARAM_STR);
            $sth->bindParam(':lon', $portal_lon, \PDO::PARAM_STR);
            $sth->bindParam(':original_id', $portal_original_id, \PDO::PARAM_STR);
            $sth->bindParam(':image_link', $portal_link, \PDO::PARAM_STR);
			$sth->execute();
			return self::getGymByLatLon($portal_lat,$portal_lon);
       
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}
	
	
	public static function getPokemonName($pokemon_id)
	{
		if (!self::isDbConnected()) {
            return false;
        }
		try {
            self::initializeRaidsDb();
            $sth = self::$pdo->prepare('SELECT `name`
							FROM '.TB_POKEMON.' p 
							WHERE `pokemon_id` = :pokemon_id;
							');
            
            $sth->bindParam(':pokemon_id', $pokemon_id, \PDO::PARAM_INT);
            
		$sth->execute();
			
		$temp = $sth->fetch();
		return $temp[0];
       
        } catch (\Exception $e) {
            throw new TelegramException($e->getMessage());
        }
	}
/*-------------------- RAIDEX - GESTIONE ------------------------*/	

	public static function insertRaidEx($raid_data)
	{
		if (!self::isDbConnected()) {
			return false;
		}
		try {
            self::initializeRaidsDb();
			$sth = self::$pdo->prepare('
				INSERT INTO `' . TB_RAIDEX . '`
				(`gym_id`,`datetime`,`chat_link`)
				VALUES (:gym_id, :datetime, :chat_link)
				ON DUPLICATE KEY UPDATE
					`datetime`       = VALUES(`datetime`),
					`chat_link` 	 = VALUES(`chat_link`);
					
			');
			
			$sth->bindParam(':gym_id', $raid_data['gym_id'], \PDO::PARAM_STR);
			$sth->bindParam(':datetime', $raid_data['datetime'], \PDO::PARAM_STR);
			$sth->bindParam(':chat_link', $raid_data['chat_link'], \PDO::PARAM_STR);

			$sth->execute();
			
			return  DB::$pdo->lastInsertId();
			
		} catch (\Exception $e) {
			throw new TelegramException($e->getMessage());
		}
	}
	public static function getRaidEx($raid_id)
	{
		if (!self::isDbConnected()) {
			return false;
		}
		try {
            self::initializeRaidsDb();
			$sth = self::$pdo->prepare('select * 
							from `' . TB_RAIDEX . '` re 
							 join `' . TB_GYM . '` g on re.gym_id = g.gym_id  
							where re.id = :raid_id;
							');
			
			$sth->bindParam(':raid_id', $raid_id, \PDO::PARAM_INT);
			
		$sth->execute();
					
		$temp = $sth->fetch(PDO::FETCH_ASSOC);
		return $temp;
	   
		} catch (\Exception $e) {
			throw new TelegramException($e->getMessage());
		}
	}
	public static function getActiveRaidEx()
	{
		if (!self::isDbConnected()) {
			return false;
		}
		try {
            self::initializeRaidsDb();
			$sth = self::$pdo->prepare('select * 
							from `' . TB_RAIDEX . '` re 
							 join `' . TB_GYM . '` g on re.gym_id = g.gym_id  
							where re.datetime >= now()
							order by re.datetime;
							');
			
			
			
		$sth->execute();
			
		$temp = $sth->fetchAll(PDO::FETCH_ASSOC);
		return $temp;
	   
		} catch (\Exception $e) {
			throw new TelegramException($e->getMessage());
		}
	}
	public static function insertRaidExUser($raid_id, $user_id, $got_pass = 1, $available = 1, $user_friend_id = null)
	{
		if (!self::isDbConnected()) {
			return false;
		}
		try {
            self::initializeRaidsDb();
			$sth = self::$pdo->prepare('
				INSERT INTO `' . TB_RAIDEX_USER . '`
				(`raidex_id`,`user_id`,`got_pass`,`available`,`user_friend_id`)
				VALUES (:raidex_id, :user_id, :got_pass, :available, :user_friend_id)
				ON DUPLICATE KEY UPDATE
					`got_pass` 	 = VALUES(`got_pass`),
					`available` 	 = VALUES(`available`),
					`user_friend_id` 	 = VALUES(`user_friend_id`);
					
			');
			
			$sth->bindParam(':raidex_id', $raid_id, \PDO::PARAM_INT);
			$sth->bindParam(':user_id', $user_id, \PDO::PARAM_INT);
			$sth->bindParam(':got_pass', $got_pass, \PDO::PARAM_INT);
			$sth->bindParam(':available', $available, \PDO::PARAM_INT);
			$sth->bindParam(':user_friend_id', $user_friend_id, \PDO::PARAM_INT);
			
			return $sth->execute();
			
		} catch (\Exception $e) {
			throw new TelegramException($e->getMessage());
		}
	}
	
	public static function getRaidExUsers($raid_id)
	{
		if (!self::isDbConnected()) {
			return false;
		}
		try {
            self::initializeRaidsDb();
			$sth = self::$pdo->prepare('select ru.* 
							from `' . TB_RAIDEX_USER . '` ru  
							where ru.raidex_id  = :raidex_id and 
							(got_pass = 1 or got_pass = 0 and user_friend_id is not null and available = 0);
							');
			
			$sth->bindParam(':raidex_id', $raid_id, \PDO::PARAM_INT);
			
		$sth->execute();
			
		$temp = $sth->fetchAll(PDO::FETCH_ASSOC);
		return $temp;
	   
		} catch (\Exception $e) {
			throw new TelegramException($e->getMessage());
		}
	}
	
	public static function getRaidExUsersFilter($raid_id, $got_pass = null, $available = null, $user_friend_id = null)
	{
		if (!self::isDbConnected()) {
			return false;
		}
		try {
            self::initializeRaidsDb();
			$query = 'select ru.* 
							from `' . TB_RAIDEX_USER . '` ru  
							where ru.raidex_id  = :raidex_id ';
			
			if($got_pass !== null)
				$query .= ' and got_pass = :got_pass ';
			if($available !== null)
				$query .= ' and available = :available ';	
			if($user_friend_id === null)
				$query .= ' and user_friend_id is null';
			else
				$query .= ' and user_friend_id = :user_friend_id';
			
			$query .= ';';
			
			$sth = self::$pdo->prepare($query);
			
			$sth->bindParam(':raidex_id', $raid_id, \PDO::PARAM_INT);
			if($got_pass !== null)
				$sth->bindParam(':got_pass', $got_pass, \PDO::PARAM_INT);
			if($available !== null)
				$sth->bindParam(':available', $available, \PDO::PARAM_INT);
			if($user_friend_id !== null)
				$sth->bindParam(':user_friend_id', $user_friend_id, \PDO::PARAM_INT);
			
		$sth->execute();
			
		return $sth->fetchAll(PDO::FETCH_ASSOC);
		
	   
		} catch (\Exception $e) {
			throw new TelegramException($e->getMessage());
		}
	}
	public static function getRaidExUsersDuplicate($raid_id, $user_id)
	{
		if (!self::isDbConnected()) {
			return false;
		}
		try {
            self::initializeRaidsDb();
			$query = 'select ru.* 
							from `' . TB_RAIDEX_USER . '` ru  
							where ru.raidex_id  = :raidex_id
							and user_id = :user_id
							and available = 1
							and user_friend_id is not null;';

			$sth = self::$pdo->prepare($query);
			
			$sth->bindParam(':raidex_id', $raid_id, \PDO::PARAM_INT);
			$sth->bindParam(':user_id', $user_id, \PDO::PARAM_INT);
			
		$sth->execute();
			
		$temp = $sth->fetchAll(PDO::FETCH_ASSOC);
		return $temp;
	   
		} catch (\Exception $e) {
			throw new TelegramException($e->getMessage());
		}
	}
	
	public static function getRaidExUser($raid_id, $user_id)
	{
		if (!self::isDbConnected()) {
			return false;
		}
		try {
            self::initializeRaidsDb();
			
			$sth = self::$pdo->prepare('select ru.* 
							from `' . TB_RAIDEX_USER . '` ru 
							where ru.raidex_id  = :raidex_id and ru.user_id = :user_id;
			');
			
			$sth->bindParam(':raidex_id', $raid_id, \PDO::PARAM_INT);
			$sth->bindParam(':user_id', $user_id, \PDO::PARAM_INT);

			$sth->execute();
			
			$temp = $sth->fetch(PDO::FETCH_ASSOC);
			return $temp;
			
		} catch (\Exception $e) {
			throw new TelegramException($e->getMessage());
		}		
	}
		
	public static function getRaidExStat($raid_id)
	{
		if (!self::isDbConnected()) {
			return false;
		}
		try {
            self::initializeRaidsDb();
			$sth = self::$pdo->prepare('select 
								count(user_id) as player, 
								(select count(available) from `' . TB_RAIDEX_USER . '` ru 
									where available = 1 and got_pass = 1 and user_friend_id is null and ru.raidex_id  = :raidex_id) as invitidisponibili, 
								(select count(got_pass) from `' . TB_RAIDEX_USER . '` ru 
									where available = 1 and got_pass = 0 and user_friend_id is null and ru.raidex_id  = :raidex_id) as attesainvito
								from  `' . TB_RAIDEX_USER . '` ru
								where  (ru.raidex_id  = :raidex_id) and
								(got_pass = 1 or got_pass = 0 and user_friend_id is not null and available = 0);
					
			');
			
			$sth->bindParam(':raidex_id', $raid_id, \PDO::PARAM_INT);
			
			
			$sth->execute();
			
			$temp = $sth->fetch(PDO::FETCH_ASSOC);
			return $temp;
			
		} catch (\Exception $e) {
			throw new TelegramException($e->getMessage());
		}		
	}
	
}
	
